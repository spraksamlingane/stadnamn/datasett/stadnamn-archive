import json
import time 
from stadnamn.elasticsearch import es_upload, save_elastic_json
import sys


with open("core-datasets/VOCAB/data/coordinate_types.json", "r", encoding="utf-8") as coordinates_file:
    coordinate_types = json.load(coordinates_file)


props = {
    "uuid": {
        "type": "keyword"
    },
}


listified = [{"uuid": key, **value} for key, value in coordinate_types.items()]
output_json = save_elastic_json("vocab", listified, schema="vocab", props=props)


es_upload(output_json, "vocab")
