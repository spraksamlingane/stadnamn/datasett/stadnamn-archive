import json
from rdflib import Graph, URIRef, Literal, Namespace

with open("lfs-data/vocab/sources/coordinateType_bindings.json", "r", encoding="utf-8") as infile:
    data = json.load(infile)

g = Graph()

# Define namespaces
SKOS = Namespace("http://www.w3.org/2008/05/skos#")
DCTERMS = Namespace("http://purl.org/dc/terms/")
VCARD = Namespace("http://www.w3.org/2006/vcard/ns#")

# Bind namespaces to prefixes
g.bind("skos", SKOS)
g.bind("dcterms", DCTERMS)
g.bind("vcard", VCARD)

for result in data["results"]["bindings"]:
    s = URIRef(result["s"]["value"]) if result["s"]["type"] == "uri" else Literal(result["s"]["value"])
    p = URIRef(result["p"]["value"]) if result["p"]["type"] == "uri" else Literal(result["p"]["value"])
    o = URIRef(result["o"]["value"]) if result["o"]["type"] == "uri" else Literal(result["o"]["value"])
    g.add((s, p, o))

# Define context
context = {
    "skos": "http://www.w3.org/2008/05/skos#",
    "dcterms": "http://purl.org/dc/terms/",
    "vcard": "http://www.w3.org/2006/vcard/ns#"
}

jsonld_data = json.loads(g.serialize(format='json-ld', context=context, indent=4, encoding='utf-8').decode('utf-8'))

json_data = {}


for item in jsonld_data["@graph"]:
    if item["@type"] == "skos:Concept":
        json_data[item["@id"].split("/")[-1]] = {
            "label": item["skos:prefLabel"],
            "legacy_id": item["dcterms:identifier"],
            "definition": item["skos:definition"],

        
        }
    



with open('lfs-data/vocab/edit/coordinates.json', 'w', encoding='utf8') as f:
    json.dump(json_data, f, indent=2, ensure_ascii=False)


# Create a map of legacy coordinate type ids and labels to the uuids
vocab_map = {}

for key, value in json_data.items():
    assert value["legacy_id"] not in vocab_map, f"Duplicate legacy id: {value['legacy_id']}"
    assert value["label"] not in vocab_map, f"Duplicate label: {value['label']}"
    vocab_map[value["legacy_id"].lower()] = key
    vocab_map[value["label"].lower()] = key

    
""" do not overwrite!
with open('shared/manual_mappings/coordinate_type_map.json', 'w', encoding='utf8') as f:
    json.dump(vocab_map, f, indent=2, ensure_ascii=False)

"""

