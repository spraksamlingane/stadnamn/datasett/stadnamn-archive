
import requests
import csv
import json



# Query wikidata for all counties and municipalities in Norway
# Load municipalities, counties and three special cases (villages)
query = """
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>

SELECT DISTINCT ?entity ?entityLabel ?county ?countyLabel
WHERE {
  
    ?entity wdt:P31 ?type ;
            wdt:P131 ?county .
    VALUES ?type { wd:Q755707
                }
    ?COUNTY wdt:P31 ?countyType .
    VALUES ?countyType { wd:Q192299 }
  

  SERVICE wikibase:label { bd:serviceParam wikibase:language "nb". }
}
"""

url = "https://query.wikidata.org/sparql"
r = requests.get(url, params = {'format': 'json', 'query': query})
data = r.json()
wd_adm = {}
wd_debug = {}
for x in data["results"]["bindings"]:
    #print(x["entityLabel"]["value"], x["entity"]["value"], x["county"]["value"])
    municipality = x["entityLabel"]["value"].replace(" kommune", "")
    county = x["countyLabel"]["value"]

    # Override error in wikidata
    if county == "Vestfold og Telemark":
        continue

    if municipality == "Oslo":
        county = "Oslo"






    if county not in wd_adm:
        wd_adm[county] = {}
    else:
        # Ignore duplicates - labels were correct in the first occurence
        if municipality in wd_adm[county]:
            continue


    wd_adm[county][municipality] = x["entity"]["value"].replace("http://www.wikidata.org/entity/", "")

    
    

gnidu_map = {}
duplicates = {}
with open("lfs-data/shared/adm/GNIDu_nåtidig inndeling.txt", "r", encoding="utf8") as f:
    reader = csv.DictReader(f, delimiter=";", quotechar='"')
    for row in reader:
        fylke = row["\ufeffFylke"]
        kommune = row["Kommune"].strip()
        if kommune == "Aker":
            kommune = "Oslo"
            fylke = "Oslo"
        if kommune == "Bodin":
            kommune = "Bodø"

        

        if row["GNIDu"] in gnidu_map:
            
            print(f"{row['GNIDu']} {fylke} {kommune}")
            print(f"Duplicate {row['GNIDu']} {gnidu_map[row['GNIDu']]['adm1']} {gnidu_map[row['GNIDu']]['adm2']}\n")

        if kommune == "Oslo":
            gnidu_map[row["GNIDu"]] = {"adm1": "Oslo", "adm1wd": wd_adm["Oslo"]["Oslo"]}
        else:
            gnidu_map[row["GNIDu"]] = {"adm1": fylke, "adm2": kommune, "adm2wd": wd_adm[fylke][kommune]}
                                   


with open("lfs-data/shared/mappings/gnidu2wikidata.json", "w", encoding="utf8") as f:
    json.dump(gnidu_map, f, indent=2, ensure_ascii=False)

print(f"Found {len(gnidu_map)} GNIDu entries, {duplicates} duplicates")




