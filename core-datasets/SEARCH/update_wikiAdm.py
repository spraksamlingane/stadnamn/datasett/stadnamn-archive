import requests
import json
# Mapping from historical adm to current day adm
# Uncomment when no longer banned
"""with open("core-datasets/SEARCH/adm_query.rq", "r", encoding="utf8") as f:
    wiki_query = f.read()

headers = {
    "User-Agent": "Stadnamnportalen/1.0 (https://stadnamnportalen.uib.no; henrik.askjer@uib.no)",  # Replace with your app's name and version
    "Retry-After": "5" 
}

rq = requests.get("https://query.wikidata.org/sparql", params = {'format': 'json', 'query': wiki_query}, headers = headers)
if not rq.ok:
    print(rq.text)
    exit()
    wikiAdmBindings = rq.json()["results"]["bindings"]
"""

with open("lfs-data/processed/SEARCH/adm/knr_map.json", "r", encoding="utf8") as f:
    knr_map = json.load(f)
    # Add counties
    for key in list(knr_map.keys()):
        if key[:2] not in knr_map:
            try:
                knr_map[key[:2]] = {
                    "adm1": knr_map[key]["adm1"],
                    "wikiAdm": knr_map[key]["wikiAdm"]
                }
            except KeyError:
                print(key, knr_map[key])
                exit()

with open("lfs-data/processed/SEARCH/adm/wikidata_adm_query.json", "r", encoding="utf8") as f:
    wikiAdmBindings = json.load(f)["results"]["bindings"]
    
wiki_mapping = {}


for item in wikiAdmBindings:
    stripped = { key: value["value"].replace("http://www.wikidata.org/entity/", "").replace(" kommune", "").replace(" herred", "").replace(" herad", "") for key, value in item.items()}
    
    # Use labels from kartverket if available
    if "number" in stripped and stripped["number"] in knr_map:
        stripped["adm2Label"] = knr_map[stripped["number"]]["adm2"]

    # Special case for Oslo
    if stripped.get("adm2") == "Q5245991":
        stripped["adm1"] = stripped.pop("adm2")
        stripped["adm1Label"] = stripped.pop("adm2Label")
        
        if "adm3" in stripped:
            stripped["adm2"] = stripped.pop("adm3")
            stripped["adm2Label"] = stripped.pop("adm3Label")

    wikiAdm = stripped.get("adm3", stripped.get("adm2", stripped["adm1"]))
    wiki_mapping[wikiAdm] = subitem = wiki_mapping.get(wikiAdm, {})
    for x in ["adm3", "adm2", "adm1"]:
        if x not in stripped:
            continue
        subitem[x] = list(set(subitem.get(x, []) + [stripped[x + "Label"]]))
    assert "adm1" in subitem, subitem
    if len(subitem.get("adm1")) != 1 and "adm3" in subitem:
       subitem["adm2"] = [x + f" ({' eller '.join(subitem['adm2'])})" for x in subitem.pop("adm3")]

with open("lfs-data/processed/SEARCH/adm/wiki_mapping.json", "w", encoding="utf8") as f:
    json.dump(wiki_mapping, f, indent=2, ensure_ascii=False)