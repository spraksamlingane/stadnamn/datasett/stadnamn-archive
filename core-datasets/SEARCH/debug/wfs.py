import geopandas as gpd
import requests
from io import BytesIO
from shapely.geometry import Point
import json
import time
from tqdm import tqdm

start_time = time.time()

from stadnamn.elasticsearch import es_upload, save_elastic_json

def fetch_wikidata_ids(municipalities):
    query = """
    SELECT ?knr ?id ?idLabel WHERE {
  ?id wdt:P2504 ?knr;
    wdt:P31 wd:Q755707.
  VALUES ?knr { """ + " ".join([f'"{x}"' for x in municipalities]) + """ }
    SERVICE wikibase:label { bd:serviceParam wikibase:language "nb". }
    }
    """
    url = "https://query.wikidata.org/sparql"
    response = requests.get(url, params={'query': query, 'format': 'json'})
    if response.ok:
        data = response.json()
        return {x["knr"]["value"]: x["id"]["value"].replace("http://www.wikidata.org/entity/", "") for x in data["results"]["bindings"]}
    else:
        return response.status_code

# Get dataframe from cached file if it exists
try:
    gdf = gpd.read_file(".tmp/kommuner.gpkg")
    knr_map = json.load(open(".tmp/knr_map.json", "r"))
except:
    adm_data = requests.get("https://api.kartverket.no/kommuneinfo/v1/fylkerkommuner").json()
    knr_map = {}
    for county in adm_data:
        county_name = county["fylkesnavn"]
        for municipality in county["kommuner"]:
            knr_map[municipality["kommunenummer"]] = {
                "adm1": county_name,
                "adm2": municipality["kommunenavn"]
            }

    wikiIds = fetch_wikidata_ids(knr_map.keys())
    for knr, wikidata_id in wikiIds.items():
        knr_map[knr]["adm2wd"] = wikidata_id

    # URL and parameters for the WFS request
    url = "https://wfs.geonorge.no/skwms1/wfs.administrative_enheter"
    params = {
        'service': 'WFS',
        'version': '2.0.0',
        'request': 'GetFeature',
        'typename': 'app:Kommune',
        'outputFormat': 'application/gml+xml; version=3.2'
    }

    # Make the GET request
    response = requests.get(url, params=params)

    # Check if the request was successful
    if response.ok:
        # Convert the bytes response to a file-like object
        gml_data = BytesIO(response.content)
        
        # Read the GML data into a GeoDataFrame
        gdf = gpd.read_file(gml_data)
        
    else:
        print("Failed to fetch data")
    
    # Cache the files to disk
    gdf.to_file(".tmp/kommuner.gpkg", driver="GPKG")
    json.dump(knr_map, open(".tmp/knr_map.json", "w"))


gdf_sindex = gdf.sindex

def find_multipolygon_for_coordinate(coordinate, gdf):
    # Convert the coordinate into a Point object
    point = Point(coordinate[0], coordinate[1])  # Note: Point(longitude, latitude)
    
    # Step 2: Use the spatial index to query polygons that might contain the point
    possible_matches_index = list(gdf_sindex.query(point))
    possible_matches = gdf.iloc[possible_matches_index]
    
    # Step 3: Check which of these candidate polygons actually contains the point
    precise_matches = possible_matches[possible_matches.contains(point)]
    
    if not precise_matches.empty:
        return precise_matches.iloc[0]  # Return the first matching row
    else:
        return None





with open("lfs-data/elastic/search_elastic.json", "r", encoding="utf8") as f:
    data = json.load(f)

print("Find polygons", f"{time.time() - start_time:.2f}s")


seen = {}

for source in tqdm(data["data"]):
    if "location" in source:
        if (source["location"]["coordinates"][0], source["location"]["coordinates"][1]) not in seen:
            kommune = find_multipolygon_for_coordinate(source["location"]["coordinates"], gdf)
            if kommune is not None:
                # Extend source by using the knr map
                knr = str(kommune["kommunenummer"])
                seen[(source["location"]["coordinates"][0], source["location"]["coordinates"][1])] = knr
                

teller = 0
with open(".tmp/coordinate2knr.txt", "w", encoding="utf8") as f:
    # csv with coordinates and knr
    teller += 1
    for source in data["data"]:
        if "location" in source:
            f.write(f"{source['location']['coordinates'][0]}\t{source['location']['coordinates'][1]}\t{source.get('adm2')}\n")
print(teller)



"""print("Save example output", f"{time.time() - start_time:.2f}s")
with open(".tmp/search_elastic.json", "w", encoding="utf8") as f:
    json.dump(data["data"], f, ensure_ascii=False, indent=2)"""




data["timestamp"] = int(time.time())

#print("Upload", f"{time.time() - start_time:.2f}s")
#es_upload(data, "search")

#print("Done", f"{time.time() - start_time:.2f}s")
