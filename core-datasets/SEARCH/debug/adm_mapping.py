import json
from collections import Counter

with open("lfs-data/elastic/search_elastic.json", "r", encoding="utf8") as f:
    data = json.load(f)

with open(".tmp/knr_map.json", "r", encoding="utf8") as f:
    knr_map = {}
    for v in json.load(f).values():
        if "adm2" in v:
            knr_map[(v["adm1"], v["adm2"])] = v


fylke_mapping = {

}

adm1 = Counter()
adm2 = Counter()

for source in data["data"]:
    source_uuid = source["uuid"]
    if "location" not in source:
        if "adm2" not in source:
            continue
        if (source["adm1"], source["adm2"]) in knr_map:
            print("Exists:", source["adm1"], source["adm2"])
            continue
        adm1[source["adm1"]] += 1
        adm2[source["adm2"]] += 1

print(adm1.most_common())
print()
print(adm2.most_common())

