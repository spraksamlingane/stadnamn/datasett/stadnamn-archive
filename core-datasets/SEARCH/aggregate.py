from stadnamn import MAIN_DATASETS
from collections import defaultdict
import json
import uuid
from stadnamn.elasticsearch import es_upload, save_elastic_json

sosi_priorty = {key: num for num, key in enumerate(reversed(["fylke", "kommune", "sogn", "navnegard", "gard", "bruk"]))}
dataset_priorty = {key: num for num, key in enumerate(reversed(MAIN_DATASETS))}    


def coordinate_has_priority(agg, dataset, sosi):
    if "coordinateDataset" not in agg or dataset_priorty[agg["coordinateDataset"]] < dataset_priorty[dataset]:
        return True
    
    elif dataset == agg["coordinateDataset"]:
        if "coordinateSosi" in agg and sosi_priorty.get(agg["coordinateSosi"], -1) < sosi_priorty.get(sosi, -1) :
            return True
        
    

def label_has_priority(agg, dataset, sosi):
    if "labelDataset" not in agg or dataset_priorty[agg["labelDataset"]] < dataset_priorty[dataset]:
        return True
    elif dataset == agg["labelDataset"]:
        if "labelSosi" in agg and sosi_priorty.get(agg["labelSosi"][0], -1) < sosi_priorty.get(sosi, -1):
            return True    



SEED_NAMESPACE = "http://data.toponym.ub.uib.no/SNID_DATASET"
def generate_snid_uuid(snid):
    return str(uuid.uuid3(uuid.NAMESPACE_URL, f"{SEED_NAMESPACE}_{snid}"))


aggregated = defaultdict(lambda: {
    "children": [],
    "altLabels": set(),
})

with open("lfs-data/processed/SEARCH/adm/knr_map.json", "r", encoding="utf8") as f:
    knr_map = json.load(f)
    # Add counties
    for key in list(knr_map.keys()):
        if key[:2] not in knr_map and "wikiAdm" in knr_map[key]:
            try:
                knr_map[key[:2]] = {
                    "adm1": knr_map[key]["adm1"],
                    "wikiAdm": knr_map[key]["wikiAdm"]
                }
            except KeyError:
                print(key, knr_map[key])
                exit()
            

with open('lfs-data/processed/SEARCH/adm/uuid2knr.txt', 'r', encoding='utf8') as f:
    uuid2knr = {}
    for line in f:
        row_uuid, row_knr = line.strip().split(";")
        uuid2knr[row_uuid] = row_knr



with open("lfs-data/processed/SEARCH/adm/wiki_mapping.json", "r", encoding="utf8") as f:
    wiki_mapping = json.load(f)



for num, dataset in enumerate(MAIN_DATASETS):

    assert dataset in dataset_priorty

    print(dataset)
    with open(f"lfs-data/elastic/{dataset}_elastic.json", "r", encoding="utf8") as f:
        data = json.load(f)
    
    teller = 0
    for source in data["data"]:
        source_uuid = source["uuid"]
        teller += 1
        if teller > 10000:
            #break
            pass
        
        
        if "snid" in source:
            instance_uuid = generate_snid_uuid(source["snid"])
            agg = aggregated[instance_uuid]
            agg["snid"] = source["snid"]
        else:
            instance_uuid = str(uuid.uuid3(uuid.NAMESPACE_URL, f"{SEED_NAMESPACE}_{source_uuid}"))
            agg = aggregated[instance_uuid]

            

        if "attestations" in source:
            # Prevent duplicates
            existing_attestations = {(x["label"], x["year"]) for x in agg.get("attestations", [{}]) if x.get('year')}
            # TODO: handle sources
            # TODO: handle attestations without year - make them altlabels?
            new_attestations = [{"label": x["label"], "year": x.get("year")} for x in source["attestations"] if x.get('year') and (x["label"], x["year"]) not in existing_attestations]
            agg["attestations"] = agg.get("attestations", []) + source["attestations"]


        if "year" in source:
            existing_attestations = {(x["label"], x["year"]) for x in agg.get("attestations", [{}]) if x.get('year')}
            if (source["label"], source["year"]) not in existing_attestations:
                agg["attestations"] = agg.get("attestations", []) + [{"label": source["label"], "year": source["year"]}]

            for altLabel in source.get("altLabels", []):
                if (altLabel, source["year"]) not in existing_attestations:
                    agg["attestations"] = agg.get("attestations", []) + [{"label": altLabel, "year": source["year"]}]



        if "altLabels" in source:
            agg["altLabels"] = agg.get("altLabels", set()) | set(source["altLabels"])

        source_gnidu = source.get("gnidu")
        if type(source_gnidu) == list:
                gnidu = source_gnidu[0]
        else:
            gnidu = source_gnidu
            
        if instance_uuid in uuid2knr:
            agg.update(knr_map[uuid2knr[instance_uuid]])
        else:           
            wikiAdm = source.get("wikiAdm")

            

            if wikiAdm and "adm1" not in agg:
                if type(wikiAdm) == list:
                    for adm in wikiAdm:
                        for key, value in wiki_mapping.get(adm, {}).items():
                            agg[key] = agg.get(key, []) + [value]
                        agg["wikiAdm"] = agg.get("wikiAdm", []) + [adm]



                elif wikiAdm in wiki_mapping:
                    agg.update(wiki_mapping[wikiAdm])
                            


        if "image" in source:
            if "image" in agg and agg["image"] and agg["image"].get("manifest") != source["image"]["manifest"]:
                agg["image"] = False
            else:
                agg["image"] = source["image"]

        if "audio" in source:
            if "audio" in agg and agg["audio"] and agg["audio"].get("file") != source["audio"]["file"]:
                agg["audio"] = False
            else:
                agg["audio"] = source["audio"]
        
        if "link" in source:
            if "link" in agg and agg["link"] and agg["link"] != source["link"]:
                agg["link"] = False
            else:
                agg["link"] = source["link"]


        if source_gnidu:
            if type(source_gnidu) == list:
                agg["gnidu"] = agg.get("gnidu", set()) |  set(source_gnidu)
            else:
                agg["gnidu"] = agg.get("gnidu", set()) | {source_gnidu}



    
            
        
        agg["children"].append(source_uuid)

        agg["datasets"] = agg.get("datasets", set()) | {dataset}

        if "sosi" in source:
            agg["sosi"] = agg.get("sosi", set()) | {source.get("sosi")}


        # Add location if not already added or if the source has higher priority
        if "location" in source and coordinate_has_priority(agg, dataset, data.get("sosi")):
            agg["location"] = source["location"]
            if "coordinateType" in source:
                agg["coordinateType"] = source["coordinateType"]
            agg["coordinateDataset"] = dataset
            if "sosi" in source:
                agg["coordinateSosi"] = source["sosi"]

            

        # Add label if not already added or if the source has higher priority
        if label_has_priority(agg, dataset, data.get("sosi")):
            
            agg["label"] = source["label"]
            if agg["label"] in agg["altLabels"]:
                agg["altLabels"].remove(source["label"])

            agg["labelDataset"] = dataset
            if "sosi" in source:
                agg["labelSosi"] = source["sosi"]

        if "label" in agg and agg["label"] != source["label"]:
            agg["altLabels"].add(source["label"])


            
props = { 
        "uuid": {
            "type": "keyword"
        },
        "location": {
            "type": "geo_point"
        },
        "ranking": {
            "type": "integer"
        }
}

with open("lfs-data/enriched/SEARCH/adm/fylke_mapping.json", "r", encoding="utf8") as f:
    fylke_mapping = json.load(f)

with open("lfs-data/enriched/SEARCH/adm/kommune_mapping.json", "r", encoding="utf8") as f:
    kommune_mapping = json.load(f)


for key, value in aggregated.items():
    # Convert sets to lists. Datasets and sosi should be ordered by priority
    aggregated[key]["datasets"] = list(value["datasets"])
    aggregated[key]["datasets"].sort(key=lambda x: -dataset_priorty.get(x, -1))

    if "sosi" in value:
        aggregated[key]["sosi"] = list(value["sosi"])
        aggregated[key]["sosi"].sort(key=lambda x: -sosi_priorty.get(x, -1))

    if "altLabels" in value:
        aggregated[key]["altLabels"] = list(value["altLabels"])
        aggregated[key]["altLabels"].sort()

    if "gnidu" in value:
        aggregated[key]["gnidu"] = list(value["gnidu"])


    # Prevent duplicate nesting of Oslo from spilling into the aggregated dataset
    # Oslo was a borough of Christiania until 1925
    if aggregated.get("adm2") == "Oslo" and aggregated.get("adm1") == "Oslo":
        if "adm3" in aggregated:
            aggregated["adm2"] = aggregated["adm3"]
            del aggregated["adm3"]
        else:
            del aggregated["adm2"]
    
    




    # Purge media key for docs with more than one item
    for media in ["image", "audio", "link"]:
        if media in value and value[media] == False:
            del value[media]

    # Add ranking:
    # 1. SNID with multiple results 
    # 2 Non-SNID with multiple results 
    # 3. others
    
    if len(value["children"]) > 1:
        if "snid" in value:
            aggregated[key]["ranking"] = 1
        else: 
            aggregated[key]["ranking"] = 2
    else:
        aggregated[key]["ranking"] = 3



    
    




json_data = [{"uuid": key, **value} for key, value in aggregated.items()]

output_json = save_elastic_json("search", json_data, schema="snid", dev = True, props=props)

es_upload(output_json, "search")
            
        



