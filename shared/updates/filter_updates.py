"""

usage:
filter_updates.py <dataset> <update_file> <original_file>



"""

import sys
import csv
import json

updated_rows = {}

# Open each file and check whether any rows in the update file are different from the original file

original_rows = {}
dataset = sys.argv[1]
update_filename = sys.argv[2]
original_filename = sys.argv[3]

with open(original_filename, "r", encoding="utf-8") as original_file:
    original_reader = csv.reader(original_file, delimiter=';', quotechar='"')
    original_headings = next(original_reader)
    for row in original_reader:
        cells = dict(zip(original_headings, row))
        original_rows[cells["uuid"]] = cells


with open(update_filename, "r", encoding="utf-8") as update_file:
    update_reader = csv.reader(update_file, delimiter=';', quotechar='"')
    update_headings = next(update_reader)

    seen_field = set()

    for row in update_reader:
        cells = dict(zip(update_headings, row))
        assert cells["uuid"] in original_rows, f"uuid {cells['uuid']} not found in original file"

        if cells != original_rows[cells["uuid"]]:
            updated_rows[cells["uuid"]] = {k: v for k, v in zip(update_headings, row) if cells[k] != original_rows[cells["uuid"]][k]}
            # Show what values are different, and compare with the original values
            for key, value in cells.items():
                different = ""
                if cells[key] != original_rows[cells["uuid"]][key]:
                    if key not in seen_field:
                        print(f"Field {key} has changed")
                        seen_field.add(key)
                    

with open(f"{update_filename.rsplit('.', 1)[0]}.json", "w", encoding="utf-8") as update_file:
    json.dump(updated_rows, update_file, indent=4, ensure_ascii=False)


print("Oppdaterte rader:", len(updated_rows))
print(len(original_rows))
        




        

    