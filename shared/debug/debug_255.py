import os
import json

def parse_children(data, dataset, key=None, uri=None):
    # If string with length 255: return
    # If list: parse each element
    # If dict: parse each value


    if isinstance(data, str) and len(data) == 255:
        return uri, key, data
    
    if isinstance(data, list):
        for item in data:
            truncated_field = parse_children(item, dataset, key, uri)
            if truncated_field:
                return truncated_field
        
    if isinstance(data, dict):
        for subkey, value in data.items():
            
            truncated_field = parse_children(value, dataset, subkey, uri or f"https://stadnamnportalen.uib.no/view/{dataset}/doc/{subkey}")
            if truncated_field:
                return truncated_field



for f in os.listdir("lfs-data/elastic"):
    
    with open(f"lfs-data/elastic/{f}", "r", encoding="utf8") as inn:
        data = json.load(inn)["data"]
        truncated_field = parse_children(data, f.split("_")[0])
        if truncated_field:
            print(f)
            print("\n".join(truncated_field))
            print()
            