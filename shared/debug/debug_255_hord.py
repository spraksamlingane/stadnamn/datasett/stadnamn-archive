import json
from collections import Counter

truncated_fields = Counter()

with open(f"lfs-data/elastic/hord_elastic.json", "r", encoding="utf8") as inn:
    data = json.load(inn)["data"]
    for key, fields in data.items():
        for field, value in fields["rawData"].items():
            if isinstance(value, str) and len(value) == 255:
                truncated_fields[field] += 1
        


with open("debug_hord_255.json", "w", encoding="utf8") as ut:
    json.dump(truncated_fields, ut, indent=2, ensure_ascii=False)




            