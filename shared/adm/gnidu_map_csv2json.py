import csv

with open("lfs-data/shared/adm/GNIDu_nåtidig inndeling.txt", "r", encoding='utf8') as f:
    reader = csv.reader(f, delimiter=";", quotechar='"')
    header = next(reader)

    for row in reader:
        data = dict(zip(header, row))
        print(data)
        break