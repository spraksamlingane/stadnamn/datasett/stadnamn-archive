
import requests
import json
import uuid
import json
import time 
from stadnamn.elasticsearch import es_upload
import csv

# TODO: Move to SEARCH dataset?

# Load municipalities, counties and three special cases (villages)
query = """
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>

SELECT ?entity ?entityLabel ?county
WHERE {
  {
    ?entity wdt:P31 ?type ;
            wdt:P131 ?county .
    VALUES ?type { wd:Q755707
                  wd:Q18663579
                  wd:Q83640434
                  wd:Q192299
                }
  }
  UNION
  {
    ?entity wdt:P31 ?type ;
            wdt:P131 ?county .
    VALUES ?entity { 
      wd:Q7665821
      wd:Q4586097
      wd:Q2176381
    }
  }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "no,nb". }
}

"""

# For now adm3 is not needed. 
# Indcludes amt
query = """
PREFIX wd: <http://www.wikidata.org/entity/>
PREFIX wdt: <http://www.wikidata.org/prop/direct/>

SELECT ?entity ?entityLabel ?type ?parent ?current
WHERE {
  ?entity wdt:P31 ?type .
  OPTIONAL {
    ?entity wdt:P131 ?parent .
  }
  OPTIONAL {
    ?entity wdt:P3842 ?current .
   }
  VALUES ?type { wd:Q755707
                wd:Q18663579
                wd:Q83640434
                wd:Q192299
                wd:Q399445
              }
  SERVICE wikibase:label { bd:serviceParam wikibase:language "no,nb". }
}
"""


          
  



# Get data from query.wikidata.org sparql endpoint

url = "https://query.wikidata.org/sparql"
r = requests.get(url, params = {'format': 'json', 'query': query})
data = r.json()

"""
UUID_NAMESPACE = "http://data.toponym.ub.uib.no/graph/VOCAB_WIKIDATA/"
def create_uuid(wikidata_id):
    return str(uuid.uuid3(uuid.NAMESPACE_URL, UUID_NAMESPACE + wikidata_id))"""

json_data = {}
for item in data['results']['bindings']:
    wikidata_id = item['entity']['value'].split("/")[-1]
    new_item = {
        "label": item['entityLabel']['value'],
    }
    if item["type"] in ['wd:Q755707', 'wd:Q18663579']:
        new_item["type"] = "adm2"
      
    elif item["type"] in ['wd:Q83640434', 'wd:Q192299', 'wd:Q399445']:
        new_item["type"] = "adm1"

      
    if "parent" in item:
        new_item["parent"] = item['parent']['value'].split("/")[-1]

    if "current" in item:
        new_item["current"] = item['current']['value'].split("/")[-1]

    json_data[wikidata_id] = new_item
       


# Add mapping for knr and knr_suffix
knru_mapping = {}
with open("lfs-data/shared/adm/20240506_Knr_med_suffiks.csv", "r", encoding="utf-8") as infile:
    reader = csv.reader(infile, delimiter="\t")
    headings = next(reader)
    for row in reader:
        cells = dict(zip(headings, row))
        wikidata_id = cells.get("wikidata")
        if wikidata_id and wikidata_id in json_data:             
            knru_mapping[cells["KNR"] + cells["knr_suffix"]] = {
                "id": wikidata_id,
                "label": json_data[wikidata_id]["label"]
            }



# The municipalities that are missing in wikidata are currently not used by any dataset
# adm3 is not needed for now
"""with open("lfs-data/adm/adm3.json", "r", encoding="utf-8") as infile:
    adm3 = json.load(infile)
    for knru, data in adm3.items():
        parent_uuid = create_uuid(data["adm2"])
        knru_mapping[knru] = {
            "adm3": data["adm3"],
            "adm2": data["adm2"],
            "adm1": json_data[parent_uuid]["parent"],

        }"""
    


with open("lfs-data/shared/mappings/knru2wikidata.json", "w", encoding="utf-8") as outfile:
    json.dump(knru_mapping, outfile, indent=2, ensure_ascii=False)
    


with open('lfs-data/shared/adm/wikidata_backup.json', 'w', encoding='utf8') as f:
    json.dump(json_data, f, indent=2, ensure_ascii=False)



