import requests, json
archive = {}

data = requests.get('https://www.arkivportalen.no/api/entity/no-SPR_arkiv000000011449').json()

def add_entities(identifier):
    data = requests.get('https://www.arkivportalen.no/api/entity/' + identifier).json()
    print(data["name"])
    archive[identifier] = data
    children = requests.get(f'https://www.arkivportalen.no/api/entity/{identifier}/children?compact=true&from=0&size=10000').json()
    #print(children["hits"])
    for hit in children["hits"]["hits"]:
        add_entities(hit["_id"])


add_entities('no-SPR_arkiv000000011449')

with open("lfs-data/shared/arkivportalen.json", "w", encoding="utf-8") as out:
    json.dump(archive, out)


