import uuid
import json

with open("lfs-data/shared/arkivportalen.json", encoding="utf-8") as inn:
    archive = json.load(inn)

# Uuid functions
def collection_uuid(identifier):
    seed = "http://data.toponym.ub.uib.no/bustadnamn/collections/" + identifier
    uid = str(uuid.uuid3(uuid.NAMESPACE_URL, seed))
    return uid, seed

metadata = {}

for key, value in archive.items():

    # Skip unit types that are handled in each dataset
    if value["unitType"]["name"] in ("Mappe", "Dokument", "Register"):
        continue

    urn = value["urn"]
    uid, seed = collection_uuid(urn)
    

    #if parent_uuid == "d4b24e9f-b933-37bd-9877-76bed4c51888":
    #    print(urn, value)
    

    item_metadata = {
        "urn": urn,
        "seed": seed,
        "identifier": value["identifier"],
        "unitType": value["unitType"]["name"],
        "path": value["path"],
        "label": value["name"],
        "link": "https://www.arkivportalen.no/entity/" + urn,
    }

    
    
    if value.get("contents"):
        item_metadata["summary"] = value["contents"]


    if value.get("parentId"):
        item_metadata["parent"] = parent_uuid = collection_uuid(value["parentId"])[0]
        metadata[parent_uuid] = parent = metadata.get(parent_uuid, {})
        parent["items"] = items = parent.get("items", [])
        items.append(uid)

        if value["parentId"] == "no_SPR_arkiv000000036229":
            print(parent)


    metadata[uid] = metadata.get(uid, {}) | item_metadata
    
    

with open("lfs-data/shared/arkivportalen_iiif_metadata.json", "w", encoding="utf-8") as out:
    out.write(json.dumps(metadata, indent=4, ensure_ascii=False))