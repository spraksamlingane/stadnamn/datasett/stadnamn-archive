import subprocess
import time
from stadnamn import DATASETS

with open('shared/reindex/force-reindex.txt', 'w') as f:
   f.write(str(int(time.time())))

for dataset in DATASETS:
    subprocess.run([".venv/Scripts/python.exe", "lib/stadnamn/elasticsearch/__init__.py", dataset, "local"])