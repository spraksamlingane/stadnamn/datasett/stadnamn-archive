import sys
import os
if sys.version_info[0:2] != (3, 11):
    raise Exception('Requires python 3.11')

# Add lib to pythonpath by creating .pth file in .venv
venv_site_packages = os.path.join(sys.prefix, 'Lib', 'site-packages')
lib_parent_path = os.path.abspath(os.path.dirname('lib'))

with open(os.path.join(venv_site_packages, 'stadnamn-lib.pth'), 'w') as f:
    f.write(lib_parent_path + '\n')

# Create folder for temporary output files
os.mkdir('.tmp')