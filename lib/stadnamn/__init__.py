MAIN_DATASETS = [
    'ssr2016',
    "m1886",
    "m1838", 
    "mu1950",
    "rygh",
    "nbas",
    "bsn", 
    "hord", 
    "leks", 
    "skul", 
    "ostf",
    "sof",
    "tot"
]

SECONDARY_DATASETS = [
    "leks_g"
]

DATASETS = MAIN_DATASETS + SECONDARY_DATASETS
