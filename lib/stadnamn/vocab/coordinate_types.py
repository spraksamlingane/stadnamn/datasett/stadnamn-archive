import json

with open("shared/manual_mappings/coordinate_type_map.json", "r", encoding="utf8") as f:
    coordinate_type_map = json.load(f)

def get_coordinate_type(koordinattype):
    return coordinate_type_map[koordinattype.lower()]