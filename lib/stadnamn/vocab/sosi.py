SOSI_GARD = "gard"
SOSI_BRUK = "bruk"
SOSI_KOMMUNE = "kommune"
SOSI_FYLKE = "fylke"
SOSI_SOKN = "sokn"
SOSI_NAMNEGARD = "navnegard"
SOSI_GAMMEL = "gammelBosettingsplass"


def get_sosi():
    import requests
    return requests.get("https://register.geonorge.no/api/sosi-kodelister/stedsnavn/navneobjekttype.json").json()

def get_sosi_codes():
    response = get_sosi()
    return {x["codevalue"] for x in  response["containeditems"]}


SOSI_CODES = get_sosi_codes()

if __name__ == "__main__":
    import json
    response = get_sosi()
    with open("lfs-data/vocab/sosi_backup.json", "w", encoding="utf8") as f:
        json.dump(get_sosi(), f, ensure_ascii=False, indent=2)
