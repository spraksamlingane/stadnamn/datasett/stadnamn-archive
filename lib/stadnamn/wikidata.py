

import json
def load_mapping():
    with open("lfs-data/shared/mappings/knru2wikidata.json", "r", encoding="utf-8") as infile:
        return json.load(infile)
    
def load_data():
    with open("lfs-data/elastic/adm_elastic.json", "r", encoding="utf-8") as infile:
        return json.load(infile)
    
ADM_MAPPING = load_mapping()