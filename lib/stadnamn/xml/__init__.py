from collections import defaultdict

def element2dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(element2dict, children):
            for k, v in dc.items():
                if v:  # Skip keys with None or empty string values
                    dd[k].append(v)
        d = {t.tag: {k:v[0] if len(v)==1 else v for k, v in dd.items()}}
                    
    if t.attrib:
        d[t.tag].update(('@' + k, v) for k, v in t.attrib.items() if v)  # Skip attributes with None or empty string values
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
              d[t.tag]['#text'] = text
        else:
            if text:  # Only assign text if it's not None or empty
                d[t.tag] = text
    return {k: v for k, v in d.items() if v}  # Skip keys with None or empty string values at the top level