from elasticsearch import Elasticsearch
import os
from elasticsearch.helpers import bulk
from elasticsearch.helpers import BulkIndexError
import sys
import json
import yaml
from jsonschema import validate
import time

ENDPOINT_TEST = os.environ.get("SN_ELASTIC_ENDPOINT_TEST")
ENDPOINT = os.environ.get("SN_ELASTIC_ENDPOINT")


def create_index(data, dataset, env, endpoint, token, dynamic=True):
    print(f"Environment: {env}")
    print(f"Endpoint: {endpoint}")
    timestamp = data["timestamp"]
    with open('shared/reindex/force-reindex.txt', 'r') as f:
        global_timestamp = int(f.read())
    if global_timestamp > timestamp:
        timestamp = global_timestamp

    es = Elasticsearch(endpoint, api_key=token).options(request_timeout=900)

    # Check if the index already exists
    new_index = f"search-stadnamn-{dataset}-{timestamp}"
    if es.indices.exists(index=new_index):
        print(f"Index {new_index} already exists, skipping upload")

    else:
        es.indices.create(index=new_index, 
                          body={
            "mappings": {
                "dynamic": dynamic,
                "dynamic_templates": [
                {
                    "strings": {
                        "match_mapping_type": "string",
                        "mapping": {
                            "type": "text",
                            "fields": {
                                "keyword": {
                                    "type": "keyword",
                                    "ignore_above": 4096
                                }
                            }
                        }
                    }
                }
            ],
                "properties": data["properties"]
            }
        })
        
        
        def gendata():
            seen = set()
            for doc in data["data"]:
                assert doc["uuid"] not in seen, f"Duplicate UUID: {doc['uuid']}"
                seen.add(doc["uuid"])
                yield {
                    "_index": new_index,
                    "_source": doc
                }


        try:
            print("Populating index...")
            bulk(es, gendata())
        except BulkIndexError as e:
            for error in e.errors:
                print(error)

            print("BulkIndexError, DELETING INDEX")
            es.indices.delete(index=new_index)
            sys.exit(1)

    # Assert that the number of documents in the index is the same as the number of documents in the data
    
    es.indices.refresh(index=new_index)
    index_doc_count = es.count(index=new_index)["count"]
    if index_doc_count != len(data["data"]):
        print(f"Number of documents in index does not match number of documents in data: {es.count(index=new_index)['count']} != {len(data['data'])}")
        # Delete index if it has no aliases
        if env != "local":
            if not es.indices.get(index=new_index)[new_index]['aliases']:
                print("DELETING INDEX")
                es.indices.delete(index=new_index)
            sys.exit(1)

    print("Number of documents in index:", index_doc_count)
    
    

    
    new_alias = f"search-stadnamn-{env}-{dataset}"
    old_index = list(es.options(ignore_status=[404]).indices.get_alias(name=new_alias).keys())[0]
    
    if old_index == new_index:
        print("Index is already aliased, skipping alias update")
        return
    
    print("Updating aliases...")
    
    
    actions = [{"add": {"index": new_index, "alias": new_alias}}]
    
    if old_index != "error":
        actions.append({"remove": {"index": old_index, "alias": new_alias}})
    
    es.indices.update_aliases(body={
        "actions": actions
    })

    
    # Delete old index if it isn't prod_index, dev_index or local_index
    if old_index != 'error' and not (es.indices.get(index=old_index)[old_index]['aliases']):
        print("Deleting", old_index)
        es.indices.delete(index=old_index)


def validate_schema(data, schemaName):
    with open(f"schemas/{schemaName}_schema.yml", "r") as f:
        schema = yaml.safe_load(f)

    #with open("lfs-data/vocab/sosi_backup.json", "r", encoding='utf8') as f:
    #    sosi = json.load(f)
    validate(data, schema)



def save_elastic_json(dataset, json_data, rawDataProps = {}, dev = False, schema = "dataset", props={}):
    print("Validating...")
    # Check for duplicates
    assert len(json_data) == len({doc["uuid"] for doc in json_data}), "Duplicate UUIDs in dataset"

    # Add mappings for common fields
    shared_fields = {key for doc in json_data for key in doc.keys()}
    properties = props
    if schema == "dataset":
        with open("schemas/dataset_elastic_props.yml", "r") as f:
            default_props = yaml.safe_load(f)
            for key in default_props:
                if key in shared_fields:
                    properties[key] = default_props[key]
        
        if rawDataProps:
            if "rawData" not in properties:
                properties["rawData"] = {"properties": {}}
            properties["rawData"]["properties"] = rawDataProps
    

    # Sort json_data by uuid so that documents are distributed randomly in the map view
    json_data.sort(key=lambda x: x["uuid"])


    output_json = {"timestamp": int(time.time()), "properties": properties, "data": json_data}

    if dev:
        try:
            validate_schema(json_data, schema)
        except Exception as e:
            print("Error:", e)

    else:
        validate_schema(json_data, schema)

        # If json file already exists: Ensure that no uuids have been removed in json_data
        # Deprecated uuids must have a redirect in the new dataset
        if os.path.exists('lfs-data/elastic/' + dataset + '_elastic.json'):
            with open('lfs-data/elastic/' + dataset + '_elastic.json', 'r', encoding='utf8') as f:
                old_json = json.load(f)
                new_uuids = {doc["uuid"] for doc in json_data}
                if isinstance(old_json["data"], dict): # Legacy
                    for key in old_json["data"]:
                        assert key in new_uuids, f"UUID {key} has been removed  (LEGACY)"
                else:
                    old_uuids = {doc["uuid"] for doc in old_json["data"]}
                    for uuid in old_uuids:
                        assert uuid in new_uuids, f"UUID {uuid} has been removed"

    print("Saving json...")
    with open('lfs-data/elastic/' + dataset + '_elastic.json', 'w', encoding='utf8') as f:
        json.dump(output_json, f, indent=2, ensure_ascii=False)

    return output_json




def es_upload(data, dataset, env = 'local', dynamic=True):
    if env == 'local':
        if not dynamic:
            es = Elasticsearch(os.environ.get("STADNAMN_ENDPOINT_TEST"), api_key= os.environ["STADNAMN_EDIT_TOKEN"]).options(request_timeout=900)
            mapping = es.indices.get_mapping(index=f"search-stadnamn-local-{dataset}")
            data["properties"] = mapping.popitem()[1]["mappings"]["properties"]
        create_index(data, dataset, env, os.environ.get("STADNAMN_ENDPOINT_TEST"), os.environ["STADNAMN_EDIT_TOKEN"], dynamic) 
    else:
        create_index(data, dataset, env, os.environ.get("ELASTIC_ENDPOINT_TEST"), os.environ["ELASTIC_TOKEN_TEST"], dynamic=True) 

        # Reuse mapping from test index
        es = Elasticsearch(os.environ.get("ELASTIC_ENDPOINT_TEST"), api_key= os.environ["ELASTIC_TOKEN_TEST"]).options(request_timeout=900)
        mapping = es.indices.get_mapping(index=f"search-stadnamn-{dataset}-{data['timestamp']}")
        data["properties"] = mapping[f"search-stadnamn-{dataset}-{data['timestamp']}"]["mappings"]["properties"]

        create_index(data, dataset, env, os.environ.get("ELASTIC_ENDPOINT"), os.environ["ELASTIC_TOKEN"], dynamic=False)




# Main

if __name__ == "__main__":
    dataset = sys.argv[1]
    env = sys.argv[2]
    with open(f'lfs-data/elastic/{dataset}_elastic.json', 'r', encoding='utf8') as f:
        output_json = json.load(f)
    es_upload(output_json, dataset, env)



    




