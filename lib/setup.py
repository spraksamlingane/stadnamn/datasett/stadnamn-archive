from setuptools import setup, find_packages

setup(
    name='stadnamn-lib',
    version='0.2',
    packages=find_packages(),
    install_requires=[
        'elasticsearch==8.12.0',
        'requests==2.31.0',
        'jsonschema==4.22.0',
        'PyYAML==6.0.1',
    ],
)