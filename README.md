# stadnamn-archive
Git Large File Storage (LFS) and conversion of base data to JSON for Elasticsearch.

Aggregated or overarching datasets can be found in [core-datasets](core-datasets).
Scripts for heterogeneous datasets are located under [datasets](datasets). The final output is in [lfs-data/elastic](lfs-data/elastic). All datasets are nested under their dataset code.

The scripts in [datasets](datasets) are meant to be run locally. Running the main scripts, following the pattern `*_elastic.json` will create elasticsearch indexes with the alias pattern `stadnamn-local-*`. This enables rapid deployment of data while developing the frontend locally. The output data is also saved in [lfs-data/elastic](lfs-data/elastic), and when committed these files will be deployed automatically with the alias patterns `stadnamn-dev-*` and `stadnamn-prod-*` in the dev and main branches respectively. Vercel dev and prod environments in the frontend repository are set up to use these aliases. Dev indices are deployed to both search.ub.uib.no and search.testdu.uib.no. When deploying to prod, the prod alias is added to existing indices in both clusters, so that the test cluster serves as fallback.

`upload_datasets.py` runs the conversion and upload scripts for all datasets. If you only need to upload all datasets from the json-files in [lfs-data/elastic](lfs-data/elastic), you can run [shared/reindex/force-reindex.py](shared/reindex/force-reindex.py). This will also force the cicd jobs to run without any changes in the json files.


## Installation
1. Clone the repository.
2. Extract the zip files you might need in [lfs-data/raw](lfs-data/raw). The files should be extracted without creating a new folder. TODO: Automate the extraction of all zip files.
3. Create a Python environment with Python 3.11 and name it `venv` (it should be placed in `.venv`).
4. Activate the environment and run `local_setup.py` to install `stadnamn-lib`.
5. Run `pip install` with `requirements.txt`. Some datasets also have their own `requirements.txt`.

## Data Refinement
The original data is located in [lfs-data/raw](lfs-data/raw). Only zip files can be committed here. When downloading the repository, these need to be extracted for the scripts to use them. Data enriched by language collections can be found under [lfs-data/enriched](lfs-data/enriched), while standardized batch updates are in [lfs-data/updates](lfs-data/updates). Data processed by the scripts in this repository are in [lfs-data/processed](lfs-data/processed). Files in `updates` and `processed` should never be created or edited manually. Folder containing minor patches are placed in the same folder as the update script (e.g datasets/LEKS/patches).

## Dataset Codes
Add new codes to `lib/stadnamn/__init__.py` when the dataset is fully integrated and can be included in the aggregation.

### Overarching Datasets:
- SEARCH: General search across the datasets
- VOCAB: Coordinate types, etc.

### Sources:
- BSN: Bustadnamn
- HORD: Hordanamn
- M1838: Matrikkelen 1838
- M1886: Matrikkelen 1886
- MU1950: Matrikkelutkastet av 1950
- RYGH: Oluf Rygh's Norske gaardnavne
- NBAS: The National Place Names Database
- NBAS_K: The National Place Names Database, map collection
- NSNB: Legacy UUID-seed for NBAS
- SKUL: Skulebarnsoppskriftene
- FT1900: Census 1900
- FT1910: Census 1910
- GN2019: Geonames 2019
- SSR2016: Central Place Names Register, Karverket 2016
- SSR: Regularly updated data from the Central Place Names Register
- OSM2019: Open Street Map 2019
- LEKS: Norwegian Place Names Encyclopedia
- LEKS_G: Norwegian Place Names Encyclopedia, base forms
- SOF: Place Names Sogn og Fjordane
- ETYM: The Etymology Register

## Schema
The overarching dataset is aggregated from the other JSON files in [lfs-data/elastic](lfs-data/elastic). Therefore, it is important that they have a common structure. At the same time, there needs to be flexibility for heterogeneous data in new datasets. Heterogeneous data should therefore be placed under "rawData" for the original data, and under "tmp" if it's data added by language collections that are not yet integrated into the common structure. Data not nested under these keys must be defined in [schemas/dataset_schema.yml](schemas/dataset_schema.yml). See the comments in the file for more information about each field.
