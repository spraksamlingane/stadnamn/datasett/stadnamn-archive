
import subprocess
import sys
from stadnamn import DATASETS
omit = []
#omit = ["BSN", "HORD", "LEKS", "LEKS_G", "M1838", "M1886", "MU1950"]
scripts = [f"datasets/{dataset.upper()}/{dataset.lower()}_elastic.py" for dataset in DATASETS if dataset not in omit]

processes = []

for script in scripts:
    process = subprocess.Popen([sys.executable, script], stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    processes.append((script, process))

# Wait for all processes to finish and track successes and failures
successful_scripts = []
failed_scripts = []

for script, process in processes:
    stdout, stderr = process.communicate()
    dataset_code = script.split('/')[1]  # Extract dataset code from script path
    print(f"{dataset_code} STDOUT:\n{stdout.decode()}")
    print(f"{dataset_code} STDERR:\n{stderr.decode()}")
    return_code = process.returncode
    if return_code == 0:
        successful_scripts.append(script)
    else:
        failed_scripts.append(script)

# Print successful scripts
print("The following scripts were successful:")
for script in successful_scripts:
    print(script)

# Print failed scripts
print("The following scripts failed:")
for script in failed_scripts:
    print(script)

print("Creating aggregated index...")
subprocess.run([sys.executable, "core-datasets/SEARCH/aggregate.py", "local"])

print("Adding vocabulary dataset...")
subprocess.run([sys.executable, "core-datasets/VOCAB/vocabulary.py", "local"])