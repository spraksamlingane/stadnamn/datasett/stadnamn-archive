import csv
from stadnamn.elasticsearch import es_upload, save_elastic_json
from stadnamn.vocab.sosi import *
from stadnamn.vocab.coordinate_types import get_coordinate_type
from stadnamn.wikidata import ADM_MAPPING
import re


json_data = []
json_map = {}


"""missing_coord = open("m1886_fix_coordinates.csv", "w", encoding="utf8", newline="")
missing_coord_writer = csv.writer(missing_coord, delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL)
missing_coord_writer.writerow(["uuid", "label", "knr", "gnidu", "midu", "x", "y"])"""


with open('lfs-data/processed/M1886/m1886_updated.csv', 'r', encoding="utf8") as f:
    reader = csv.reader(f, delimiter=';', quotechar='"')
    header = [x.lower() for x in next(reader)]
    print(header)

    for tmp, row in enumerate(reader):
        cells = dict(zip(header, row))
        


        if cells["contains_gard"] == "True":
            gard_data = {
                "uuid": cells["gard_uuid"],
                "label": cells["gardsnamn"],
                "snid": cells["gardsnamn_snid"],
                "adm1": cells["fylke"],
                "adm2": cells["kommune"],
                "year": cells["år"],
                "wikiAdm": ADM_MAPPING[cells["knru"]]["id"],
                "sosi": SOSI_GARD,
                "link": cells["lenke_til_digital_matrikkel"],
                "rawData": cells,
                "cadastre": [
                    {"gnr": int(cells["gnr"])},
                ]
            }

            assert "" not in gard_data.values(), print(gard_data)
            if cells.get("gnidu"):
                gard_data["gnidu"] = cells["gnidu"]

            if cells.get("x"):
                gard_data["coordinateType"] = get_coordinate_type(cells["koordinattype"])
                gard_data["location"] = {
                                    "type": "Point",
                                    "coordinates": [float(cells["x"]), float(cells["y"])]
                }
                json_data.append(gard_data)

        # assert " til " in cells.get("bnr") or re.match(r"^\d+$", cells.get("bnr")), print(cells)

        bruk_data = {
            "uuid": cells["bruk_uuid"],
            "label": cells["bruksnamn"],
            "snid": cells["bruksnamn_snid"],
            "adm1": cells["fylke"],
            "adm2": cells["kommune"],
            "wikiAdm": ADM_MAPPING[cells["knru"]]["id"],
            "sosi": SOSI_BRUK,
            "within": cells["gard_uuid"],
            "link": cells["lenke_til_digital_matrikkel"],
            "rawData": cells,
            "cadastre": []
        }

        if " til " in cells.get("bnr"):
            brn_range = cells["bnr"].split(" til ")
            bruk_data["cadastre"].append({"gnr": int(cells["gnr"]), "bnr": [n for n in range(int(brn_range[0]), int(brn_range[1]) + 1)]})

        else:
            bruk_data["cadastre"].append({"gnr": int(cells["gnr"]), "bnr": int(cells["bnr"])})

        assert "" not in bruk_data.values(), print(bruk_data)

        if cells.get("midu"):
            bruk_data["midu"] = cells["midu"]

        if cells.get("gnidu"):
            bruk_data["gnidu"] = cells["gnidu"]


        #if (cells["x"] or cells["y"]) and float(cells["y"]) < 56:
        #    missing_coord_writer.writerow([bruk_data["uuid"], bruk_data["label"], cells["knr"], cells["gnidu"], cells["midu"], cells["x"], cells["y"]])
                                          

        
        if cells.get("x"):
            bruk_data["coordinateType"] = get_coordinate_type(cells["koordinattype"])
            bruk_data["location"] = {
                "type": "Point",
                "coordinates": [float(cells["x"]), float(cells["y"])]
            }
        
        json_data.append(bruk_data)
            

#missing_coord.close()

output_json = save_elastic_json("m1886", json_data)
es_upload(output_json, 'm1886')



        

        


        


