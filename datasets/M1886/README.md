# Matrikkelen 1886

Dataene i m1886_complete.csv er hentet fra det arkiverte stadnamn-data repoet, og var vasket med dette scriptet:
https://git.app.uib.no/spraksamlingane/stadnamn/datasett/stadnamn-data/-/blob/main/data-scripts/refine_m1886.py?ref_type=heads 

Master-fil for senere rettelser er m1886_complete_uuid.csv, som er beriket med uuid for gard og bruk.