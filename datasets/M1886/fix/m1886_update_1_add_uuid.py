"""
Prevent duplicate IDs in Elasticsearch by adding UUIDs to each row in the CSV file.
These uuids should be preserved in later changes to the data.
"""
import csv
import uuid
seen = {}
nr_duplicates = 0
SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/M1886"

def get_uuid(columns, values):
    # Cancatenate if key is a tuple
    if isinstance(columns, tuple):
        columns = "_".join(columns)
        values = "_".join(values)
    seed = f"{SEED_NAMESPACE}_{columns}_{values}"

    return str(uuid.uuid3(uuid.NAMESPACE_URL, seed))

with open("lfs-data/processed/M1886/m1886_complete.csv", "r", encoding="latin-1") as f:
    reader = csv.reader(f, delimiter='\t')
    headings = next(reader)
    # Add uuid to each row and save to new file
    with open("lfs-data/processed/M1886/m1886_complete_uuid.csv", "w", encoding="utf-8", newline="") as f2:
        
        
        writer = csv.writer(f2, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerow(headings + ["gard_uuid", "bruk_uuid"])
        for num, row in enumerate(reader):
            cells = dict(zip(headings, row))
            gard_uuid = get_uuid(("KNR", "GNR", "SNID"), (cells["KNR"], cells["GNR"], cells["Gardsnamn_SNID"])) 
            bruk_uuid = get_uuid("row_m1886_complete.csv", str(num))
            writer.writerow(row + [gard_uuid, bruk_uuid])
