import csv
import uuid
import re
import csv
import uuid
import json
from stadnamn.elasticsearch import es_upload, save_elastic_json

json_data = []

with open("lfs-data/enriched/SSR2016/2016_kommuner.txt" , "r", encoding="utf8") as f:
    knr_map = {}
    fnr_map = {}
    reader = csv.DictReader(f, delimiter=";", quotechar='"', quoting=csv.QUOTE_ALL)
    for row in reader:
        knr_map[row["ENH_KOMM"]] = row["Kommune"]
        fnr_map[row["ENH_KOMM"][:2]] = row["\ufeffFylke"]

with open("lfs-data/processed/SSR2016/ssr2016_sosi.json", "r", encoding="utf8") as f:
    ssr2016_sosi = json.load(f)


field_mappings = {
    "SNSKRSTAT": {
        "V": "Vedtak",
        "G": "Godkjent",
        "S": "Samlevedtak",
        "A": "Avslått",
        "K": "Vedtak påklaget",
        "F": "Foreslått",
        "H": "Historisk",
        "P": "Privat",
        "U": "Uvurdert",
        "I": "Internasjonalt område"
    },
    "SNSPRAAK": {
        "EN": "Engelsk",
        "FI": "Kvensk",
        "NO": "Norsk",
        "SL": "Lulesamisk",
        "SN": "Nordsamisk",
        "SS": "Sørsamisk",
        "SV": "Svensk"
    }
}


seen_ids = set()

fix_null_island_vesterli = {
    "350225": ["361140", "8.87185", "58.5964"],
    "407540": ["223970", "7.60223", "58.35034"],
    "407580": ["100981", "7.60856", "58.3505"],
    "442403": ["951842", "5.90989", "58.47878"],
}


with open("lfs-data/enriched/SSR2016/20240903_SK2016_UTF8/20240903_SK2016_UTF8.txt" , "r", encoding="utf8") as f:
    reader = csv.DictReader(f, delimiter=";", quotechar='"', quoting=csv.QUOTE_ALL)
    for row in reader:
        #print(row.keys())
        #exit()

        if row["ID"] in seen_ids:
            row["X"], row["Y"], row["Stedsnavn_lokalId"] = fix_null_island_vesterli[row["ID"]]
            assert row["ID"] not in seen_ids, "Duplicate ID: " + row["ID"]
        seen_ids.add(row["ID"])
        
        instance_data = {
            "uuid": str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/SSR2016/" + row["ID"])),
            "label": row["ENH_SNAVN"],
            "altLabels": [row["FOR_SNAVN"]],
            "adm1": fnr_map[row["ENH_KOMM"][:2]],
            "adm2": knr_map[row["ENH_KOMM"]],
            "sosi": ssr2016_sosi[row["NAVNTYPE"]]["sosi"],
            "misc": {
                "status": field_mappings["SNSKRSTAT"][row["SNSKRSTAT"]],
                "language": field_mappings["SNSPRAAK"][row["SNSPRAAK"]],
            },
            "rawData": row,

        }

        if "origLabel" in ssr2016_sosi[row["NAVNTYPE"]]:
            instance_data["misc"]["origSosi"] = ssr2016_sosi[row["NAVNTYPE"]]["origLabel"]


        if instance_data["adm2"] == "Oslo":
            instance_data["adm1"] = "Oslo"
            del instance_data["adm2"]

        if row.get("SNID") and row.get("FOR_SNAVN") == row.get("ENH_SNAVN"):
            instance_data["snid"] = row["SNID"]
        
        if row.get("GNIDu"):
            instance_data["gnidu"] = row["GNIDu"]

        


        if row.get("X"):
            instance_data["coordinateType"] = "b7be7b6b-accb-365c-9907-a3febf807a6b"
            instance_data["location"] = {
                "type": "Point",
                "coordinates": [float(row["X"]), float(row["Y"])]
            }

        json_data.append(instance_data)




output_json = save_elastic_json("ssr2016", json_data)
es_upload(output_json, "ssr2016", "local")
