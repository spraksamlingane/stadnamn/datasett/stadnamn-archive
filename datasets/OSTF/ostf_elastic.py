import csv
import uuid
from stadnamn.elasticsearch import es_upload, save_elastic_json
from stadnamn.vocab.coordinate_types import get_coordinate_type
import json

json_data = []
seen = set()

SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/OSTF/"


with open('lfs-data/processed/OSTF/adm/knr2wikidata.json', 'r', encoding="utf8") as f:
    knr2wikidata = json.load(f)


with open('lfs-data/enriched/OSTF/20240430_OSTF registerbind_7.txt', 'r', encoding="utf8") as f:
    reader = csv.reader(f, delimiter=';', quotechar='"')
    header = [x for x in next(reader)]
    print(header)
    seen_ids = set()
    for row in reader:
        cells = dict(zip(header, row))

        assert cells["ID"] not in seen_ids, cells["ID"]
        seen_ids.add(cells["ID"])
        

        data = {
            "uuid": str(uuid.uuid3(uuid.NAMESPACE_URL, SEED_NAMESPACE + "_" + cells["ID"])),
            "label": cells["Oppslagsform/skriftform"],
            "adm1": cells["Fylke"],
            "adm2": cells["Herred for lokalitet"],
            "snid": cells["SNID"],
            "rawData": cells,
            "wikiAdm": knr2wikidata.get(cells["KNR"])["id"],
            
        }

        for x in range(1, 12):
            number = str(x).zfill(2)
            if cells.get(f"Lenke til side_{number}"):
                data["links"] = data.get("links", [])
                data["links"].append(cells[f"Lenke til side_{number}"].strip("#"))


        for x in range(1, 7):
            number = str(x).zfill(2)
            if cells.get(f"GNIDu_{number}"):
                data["gnidu"] = data.get("gnidu", [])
                data["gnidu"].append(cells[f"GNIDu_{number}"])
            
        
        if cells.get("MIDu"):
            data["midu"] = cells["MIDu"]


        if cells.get("X"):
            coordinates = [float(cells["X"]), float(cells["Y"])]
            data["coordinateType"] = get_coordinate_type(cells["koordinattype"])
            if coordinates[0] < 180 and coordinates[1] < 90:
                data["location"] = {
                    "type": "Point",
                    "coordinates": coordinates
                }
            else:
                print("Invalid coordinates", cells["ID"], coordinates)


        data["rawData"] = cells
        json_data.append(data)


output_json = save_elastic_json("ostf", json_data)
es_upload(output_json, "ostf")

        


        






        

        


        


