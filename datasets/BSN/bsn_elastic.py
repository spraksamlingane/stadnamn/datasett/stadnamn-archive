from stadnamn.elasticsearch import es_upload, save_elastic_json
import json
import re



"""
VIDERE PROSESSERING
1. Legg til cadastre
2. Erstatt rawData.supplemented


TODO:
1. Legg til sosi (feil i supplerte data - underbruk finner ikke lenger som navneobjekttype

"""

def potentialList(value):
    return value if type(value) == list else [value]


with open(".tmp/bsn_data.json", encoding="utf-8") as f:
    json_data = json.load(f)


longest = 0
teller = 0

with open("lfs-data/processed/BSN/adm/adm2wikidata.json", "r", encoding='utf8') as f:
    adm2wikidata = json.load(f)


for entry in json_data:


    # Fix adm errors
    if entry["adm1"] == "Akerhus":
        entry["adm1"] = "Akershus"
    if entry["adm2"] == "Eresfj. Vistd.":
        entry["adm2"] = "Eresfjord og Vistdal"
    if entry["adm1"] == "Buskerud" and entry["adm2"] == "Hovin":
        entry["adm1"] = "Telemark"

    # Add wikidata identifiers
    entry["wikiAdm"] = adm2wikidata[entry["adm1"]][entry["adm2"]]

    supplemented = entry.get("rawData").get("supplemented", {})
    original = entry.get("rawData").get("original", {})
    
    if original:
        stnavn = original["stnavn"]


        # Add cadastre

        steder = potentialList(stnavn.get("sted"))
        if len([x for x in steder if x.get("gårdsnr")]) > 0:

            cadastre = []

            for sted in steder:
                garder = potentialList(sted.get("gårdsnr"))
                bruk = potentialList(sted.get("bruksnr"))
                # Include only those containing a number
                garder = [x for x in garder if x and re.search(r"\d", x)]
                garder = [re.sub(r"\((.*?)\)", "", x) for x in garder] # Remove text in parentheses
                garder = [re.sub(r"\D$", "", x ) for x in garder] # Remove non digits
                garder = [x for x in garder if x]
                bruk = [x for x in bruk if x and re.search(r"\d", x) and "?" not in x and "/" not in x and "eller" not in x]
                bruk = [re.sub(r"\((.*?)\)", "", x) for x in bruk] # remove parentheses
                # Remove trailing dot and whitespace
                bruk = [re.sub(r"\s*\.\s*$", "", x) for x in bruk]
                bruk = [re.sub(r"!", "", x) for x in bruk]

                bruk = [x for x in bruk if x]


                if len(garder) > 1 or not bruk or "-" in garder[0]:
                    for gnr in garder:
                        if "?" in gnr or "eller" in gnr:
                            continue

                        if "-" in gnr:
                            if gnr == "88-135": # Ignore a sokn with a big gnr range
                                continue
                            fix = {
                                "69-68": (68, 69),
                                "136-38:": (136, 138),
                            }
                            start, end = fix.get(gnr, map(int, gnr.split("-")))
                            
                            range_list = list(range(start, end + 1))
                            cadastre.append({"gnr": range_list})

                        else:
                            cadastre.append({"gnr": int(gnr)})
                elif garder and bruk and "?" not in garder[0]:

                    if len(bruk) == 1:
                        cadastre.append({"gnr": int(garder[0]), "bnr": int(bruk[0])})
                    else: # Multiple bruk
                        cadastre.append({"gnr": int(garder[0]), "bnr": [int(x) for x in bruk]})

                                
            if cadastre:
                entry["cadastre"] = cadastre

    if supplemented and not potentialList(original.get("stnavn", {}).get("sted", {}))[0].get("gårdsnr"):
        assert "cadastre" not in entry, entry
        if supplemented["gnr"]:
            # Remove preceding '
            gnr = supplemented["gnr"].replace("'", "")

            if supplemented["bnr"]:
                entry["cadastre"] = [{"gnr": int(gnr), "bnr": int(supplemented["bnr"])}]
            else:
                entry["cadastre"] = [{"gnr": int(gnr)}]
                



    # Set rawData to original. Remove if original is not present
    if original:
        entry["rawData"] = original
    else:
        assert supplemented
        del entry["rawData"]
    
    if supplemented:
        entry["tmp"] = supplemented



output_json = save_elastic_json('bsn', json_data)


print("Uploading to ElasticSearch...")
es_upload(output_json, 'bsn')

