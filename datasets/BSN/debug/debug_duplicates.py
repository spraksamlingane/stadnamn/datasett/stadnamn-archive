import json
with open("lfs-data/elastic/bsn_elastic.json", "r", encoding="utf8") as f:
    data = json.load(f)


seen = {}

count = 0
different_coordinates = 0

for item in data["data"]:
    if item["uuid"] in seen:
        
        if seen[item["uuid"]].get("location") and item.get("location") and (seen[item["uuid"]]["location"]["coordinates"][0] != item["location"]["coordinates"][0] or seen[item["uuid"]]["location"]["coordinates"][1] != item["location"]["coordinates"][1]):
            different_coordinates += 1
            print("Duplicate", "A", item, "\n\nB", seen[item["uuid"]])
            print("\n")

        count += 1

    seen[item["uuid"]] = item
    
    

print(count)
print(different_coordinates)