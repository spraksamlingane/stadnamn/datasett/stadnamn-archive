import xml.etree.ElementTree as ET
tree = ET.parse('lfs-data/raw/BSN/BSN_original.xml')
root = tree.getroot()

nested_properties = set()
all_properties = set()

def parse(node, path=""):
    # add children to nested properties if the same tag occurs more than once
    seen = set()
    for child in node:
        # skip if the child is empty
        new_path = path + "." + child.tag
        all_properties.add(new_path)
        if len(child) == 0 and (child.text is None or child.text.strip() == ""):
            continue

        if child.tag in seen and len(child) > 0:
            nested_properties.add(new_path)
        seen.add(child.tag)
        
        parse(child, new_path)
        



# list 10 first children of <steder>
counter = 0
for stnavn in root:
    #if counter > 3:
    #    break
    counter += 1
    parse(stnavn, stnavn.tag)
    
        

print("Nested properties:")
for prop in sorted(nested_properties):
    print(prop)
print()
print("Non nested properties:")
for prop in sorted(all_properties - nested_properties):
    print(prop)
