import json
import csv

with open('lfs-data/elastic/bsn_elastic.json', 'r', encoding='utf8') as infile:
    data = json.load(infile)

with open('lfs-data/updates/BSN/20240528_fix_coordinates.json', 'r', encoding='utf8') as checkfile:
    fixed = json.load(checkfile)

with open("20240528b_bsn_fix_coordinates.txt", "w", encoding="utf-8") as export:
    writer = csv.writer(export, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL, lineterminator='\n')
    writer.writerow(["uuid", "x", "y", "label", "knr", "gnidu", "midu", "koordinattype"])

    for uid, v in data["data"].items():
        coordinates =  v.get("location", {}).get("coordinates")
        
        if coordinates and coordinates[1] < 56:
            writer.writerow([uid, coordinates[0], coordinates[1], v["label"], v["rawData"]["supplemented"]["knr"], v["rawData"]["supplemented"]["gnidu"], v["rawData"]["supplemented"]["midu"], v["rawData"]["supplemented"]["koordinattype"]])
            assert uid not in fixed, f"Already fixed {uid}"