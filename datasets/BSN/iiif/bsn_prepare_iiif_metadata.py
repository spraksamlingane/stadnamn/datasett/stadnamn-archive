import uuid
import xml.etree.ElementTree as ET
import requests
import re
import json
import csv
from collections import Counter
tree = ET.parse('lfs-data/raw/BSN/BSN_original.xml')
root = tree.getroot()


# Load all image paths
with open("datasets/BSN/iiif/images.json") as inn:
    bildestier = set(json.load(inn))
print("Number of images", len(bildestier))


# Uuid functions
def collection_uuid(identifier):
    seed = "http://data.toponym.ub.uib.no/bustadnamn/collections/" + identifier
    uid = str(uuid.uuid3(uuid.NAMESPACE_URL, seed))
    return uid, seed

def metadata_uuid(*args):
    seed = "http://data.toponym.ub.uib.no/bustadnamn/manifests/" + "_".join(args)
    uid =  str(uuid.uuid3(uuid.NAMESPACE_URL, seed))
    return uid, seed

def image_uuid(imagepath):
    seed = "http://data.toponym.ub.uib.no/bustadnamn/images/" + imagepath
    uid = str(uuid.uuid3(uuid.NAMESPACE_URL, seed))
    return uid, seed



debug = {} # Add image paths here for debugging



processed_images = set() # Check that all images have metadata or don't need metadata
image_counter = Counter()
metadata = {
    "collections": {},
    "groups": {},
    "scans": {}
}

BUSTADNAVN, BUSTADNAVN_SEED = collection_uuid("bsn")

OPPSLAGSORD, OPPSLORD_SEED = collection_uuid("bsn/oppsl")
OPPRINNELIG, OPPRINNELIG_SEED = collection_uuid("bsn/oppslagsord/oppr")
SUPPLERT, SUPPLERT_SEED = collection_uuid("bsn/oppsl/supp")

SKANNINGER, SKANNINGER_SEED = collection_uuid("bsn/skanninger")
TROMS, TROMS_SEED = collection_uuid("bsn/troms")
MANGLET, MANGLET_SEED = collection_uuid("bsn/manglet")


metadata["collections"][BUSTADNAVN] = {
    "seed": BUSTADNAVN_SEED,
    "label": {"nb": "Bustadnavn", "nn": "Bustadnamn", "en": "Farm Name Register"}
}

metadata["collections"][OPPSLAGSORD] = {
    "seed": SKANNINGER_SEED,
    "label": {"nb": "Oppslagsord", "nn": "Oppslagsord", "en": "Keywords"},
    "parent": BUSTADNAVN,
}

metadata["collections"][OPPRINNELIG] = {
    "seed": OPPRINNELIG_SEED,
    "label": {"nb": "Opprinnelige metadata", "nn": "Opprinnelige metadata", "en": "Original metadata"},
    "parent": OPPSLAGSORD,
}

metadata["collections"][SUPPLERT] = {
    "seed": SUPPLERT_SEED,
    "label": {"nb": "Supplerte metadata", "nn": "Supplerte metadata", "en": "Supplementary metadata"},
    "parent": OPPSLAGSORD,
}
metadata["collections"][MANGLET] = {
    "seed": MANGLET_SEED,
    "label": "Diverse 2023",
    "parent": SUPPLERT,
}

metadata["collections"][TROMS] = {
    "seed": TROMS_SEED,
    "label": "Troms 2023",
    "parent": SUPPLERT,
}

metadata["collections"][SKANNINGER] = {
    "seed": SKANNINGER_SEED,
    "label": {"nb": "Skanninger", "nn": "Skanningar", "en": "Scans"},
    "parent": BUSTADNAVN,
}




# Get folder structure for the foldersHent mappestruktur for kilder
with open("datasets/BSN/iiif/fylkemap.json") as inn:
    fylkemap = json.load(inn)



# Add fylke as collections
for kilde, fylke in fylkemap.items():
    fylke_id, fylke_seed = collection_uuid("fylke_" + fylke)
    kilde_id, kilde_seed = collection_uuid(kilde)
    if fylke_id not in metadata["collections"]:
        metadata["collections"][fylke_id] = {
            "seed": fylke_seed,
            "label": fylke,
            "parent": OPPRINNELIG
        }
    metadata["collections"][kilde_id] = {
        "seed": kilde_seed,
        "label": kilde,
        "parent": fylke_id
    }



# Add metadata for manifests in scan batch collections
for sti in bildestier:
    batch_path = sti[:-13]
    batch_id, batch_seed = collection_uuid(batch_path)
    if batch_id not in metadata["collections"]:
        metadata["collections"][batch_id] = {
            "seed": batch_seed,
            "label": "Batch " + str(int(sti[5:-13])),
            "parent": SKANNINGER
        }
    
    scan_id, scan_seed = metadata_uuid(sti)
    image_id, image_seed = image_uuid(sti)
    metadata["scans"][scan_id] = {
        "seed": scan_seed,
        "label": sti[-12:-4],
        "collection": batch_id,
        "image": image_id,
        "image_seed": image_seed
    }



# Fix broken links between images and original metadata
missing_links = {}
MISSING_LINKS_FILENAME = "20230224_disconnected_images.txt"
with open("lfs-data/enriched/BSN/"+ MISSING_LINKS_FILENAME, encoding="utf-8") as inn:
    for num, line in enumerate(inn):
        cells = [x.strip("\n") for x in line.split(";")]

        if cells[1] != "": # Exclude the ones fixed by peder in 20230217_Manglende metadata_fikset_3.txt
            biletesti = cells[0][-26:]
            
            if len(cells) == 3:
                #print(cells, biletesti)
                id_pair = tuple(cells[1:3])
                if id_pair not in missing_links:
                    missing_links[id_pair] = {"images": set(), "source_rows": {MISSING_LINKS_FILENAME: []}}
                
                missing_links[id_pair]["images"].add(biletesti)
                missing_links[id_pair]["source_rows"][MISSING_LINKS_FILENAME].append(num)
            else:
                processed_images.add(biletesti) # Add images that don't need group metadata
                image_counter[biletesti] += 1
                if biletesti in debug:
                    print(biletesti)
                


# Load supplemented metadata first in case it is needed for debugging when loading original data
supplerte = {}
SUPPLERTE_FILENAME = "20230223_Manglende metadata_fikset_7.txt"
with open('lfs-data/enriched/BSN/' + SUPPLERTE_FILENAME) as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar="\"")
    header = [x.lower() for x in next(reader)]
    print(header)
    for num, row in enumerate(reader):
        supplerte[(row[4], row[5])] = {a: b for a, b in zip(header, row)}
        supplerte[(row[4], row[5])]["source_row"] = num



# Load original data
error_skip = {
    "cd01/00000076/00076306.001"   
}
def as_number(string):
    found = re.search(r'\d+', string)
    if found:
        return int(found.group())
    else:
        return 0


for num, row in enumerate(root):
    bilete = row.attrib["bilete"]   
    idnr = row.attrib["idnr"]
    oppslag = row.find("oppslag").find("oppslord").text or ""
    oppslag_id, oppslag_seed = metadata_uuid(idnr, oppslag)
    
    kjelde = row.attrib["kjelde"]
    
    if bilete or (idnr, oppslag) in supplerte or (idnr, oppslag) in missing_links:
        if oppslag_id in metadata["groups"]:
            metadata["groups"][oppslag_id]["source_rows"]["BSN_original.xml"].append(num)
        else:

            oppslag_list = []
            for o in row.findall("oppslag"):
                oppslord = o.find("oppslord").text
                if oppslord:
                    oppslag_list.append(oppslord)
                    
                    
            entry = {
                "seed": oppslag_seed,
                "label": (idnr + " " + oppslag).strip(),
                "scans": [],
                "id_stamp": idnr,
                "collection": collection_uuid(kjelde)[0],
                "source_rows": {"BSN_original.xml": [num]}
            }
            if oppslag_list:
                entry["keywords"] = oppslag_list
            
            sted = row.find("sted") # Only use first sted for kommune and fylke.
            gnrset = set()
            bnrset = set()
            if sted is not None:
                entry["municipality"] = sted.find("herred").text
                entry["county"] = sted.find("fylke").text
                
                # Add bnr and gnr if applicable
                for sted in row.findall("sted"):
                    for gnr in sted.findall("gårdsnr"):
                        if gnr.text and bool(re.search(r'\d', gnr.text)):
                            gnrset.add(gnr.text)
                    
                    for bnr in sted.findall("bruksnr"):
                        if bnr.text and bool(re.search(r'\d', bnr.text)):
                            if "/" in bnr.text: # Fix bug in the original dataset
                                for x in bnr.text.split("/"):
                                    bnrset.add(x)
                            else:
                                bnrset.add(bnr.text)

                
                if gnrset:
                    entry["gnr"] = sorted(list(gnrset), key = as_number)

                if bnrset and len(gnrset) == 1: # Skip bnr if no gnr
                    entry["bnr"] = sorted(list(bnrset), key = as_number)   
            
            for loktype in row.findall("loktype"):
                loktype_label = loktype.find("type").text
                entry["types"] = entry.get(loktype_label, []) + [loktype_label]
                
            komm = row.find("komm")
            if komm is not None:
                entry["comment"] = komm.text
                
            oppskr = row.find("oppskr")
            if oppskr is not None:
                entry["collector"] = oppskr.find("os_navn").text
                aar = oppskr.find("år")
                if aar is not None and aar.text !=  "9999":
                    entry["year"] = aar.text
            
            metadata["groups"][oppslag_id] = entry
            
            
            """if (idnr, oppslag) in supplerte: # Activate test if supplerte has ben modified
                supp = supplerte[(idnr, oppslag)]
                
                print(idnr, oppslag)
                sfylke = supp["fylke"].lower()
                sherad = supp["herad"].lower()
                sgnr = supp["gnr"].lower()
                sbnr = supp["bnr"].lower()
                suppsign = (sfylke, sherad, sgnr)
                efylke = entry.get("county").lower()
                eherad = entry.get("municipality").lower()
                egnr = {x.lower() for x in entry.get("gnr", [])}
                ebnr = {x.lower() for x in sorted(list(bnrset))}
                if not (sfylke == efylke and sherad == eherad and (sgnr in egnr or  not (sgnr and egnr)) and (sbnr in ebnr or  not (sbnr and ebnr))):
                    print("SUPP", sfylke, sherad, sgnr, sbnr)
                    print("ORIG", efylke, eherad, egnr, ebnr)
                    print(supp["lenke"])
                print()"""


    if bilete and bilete not in error_skip:
        scan_id = metadata_uuid(bilete)[0]
        assert scan_id not in metadata["groups"][oppslag_id]["scans"]
        metadata["groups"][oppslag_id]["scans"].append(scan_id)
        processed_images.add(bilete)
        image_counter[bilete] += 1
        if bilete in debug:
            print(bilete, oppslag_seed)



# Add missing images
for key, value in missing_links.items():
    oppslag_id = metadata_uuid(key[0], key[1])[0]
    for scan in value["images"]:
        processed_images.add(scan)
        image_counter[scan] += 1
        if scan in debug:
            print(scan)
        scan_id = metadata_uuid(scan)[0]
        group = metadata["groups"][oppslag_id]
        if scan_id not in group["scans"]:
            group["scans"].append(scan_id)
            group["source_rows"][MISSING_LINKS_FILENAME] = value["source_rows"][MISSING_LINKS_FILENAME]   #group["source_rows"].get(MISSING_LINKS_FILENAME, [])  + [num]
    

# Add metadata supplemented by Språksamlingene
inkluder_unummerert = {
    "cd02/00000143/00143306.001",
    "cd01/00000026/00026417.001"
}
for key, value in supplerte.items():
    scan = value["biletesti"]
    if value["stempel"] != "---ikke seddel---":
        idnr = value["stempel"].strip()
        oppslag = value["stadnamn"].strip()
        manglet_id = value["id"]
        assert idnr or oppslag
        assert ".tif" not in scan and ".001" in scan
        
        group_id, group_seed = metadata_uuid(idnr, oppslag)
        scan_id= metadata_uuid(scan)[0]
        
        if group_id in metadata["groups"]:
            if scan_id not in metadata["groups"][group_id]["scans"]:
                processed_images.add(scan)
                image_counter[scan] += 1
                metadata["groups"][group_id]["scans"].append(scan_id)
                if scan in debug: print("both places", scan)
                
                
        elif value["status"] != "unummerert" or scan in inkluder_unummerert: #not (value["status"] == "unummerert" and (scan in processed_images or scan == "cd03/00000184/00184324.001"): # Skip Løene, as it appears in Troms
            # ADD COLLECTIONS
            assert value["herad"] and value["fylke"]
            processed_images.add(scan)
            image_counter[scan] += 1
            if scan in debug:
                print("new", scan, group_seed, value["status"])
            # Use different UUID in case of collisions with the original metadata
            group_id, group_seed = metadata_uuid("MANGLET", manglet_id, idnr, oppslag)

                
            group_metadata = {
                "seed": group_seed,
                "label": (idnr + " " + oppslag).strip(),
                "keywords": [oppslag],
                "scans": [scan_id],
                "id_stamp": idnr,
                "county": value["fylke"],
                "municipality": value["herad"],
                "bnr": value["bnr"],
                "gnr": value["gnr"],
                "collection": MANGLET,
                "source_rows": {SUPPLERTE_FILENAME: [value["source_row"]]}
            }
            
            metadata["groups"][group_id] = group_metadata
    
    else:
        processed_images.add(scan)
        image_counter[scan] += 1
        if scan in debug:
            print("ikke seddel", scan, value["status"])
            

# Supplerte metadata fra Troms
supplerte_troms = {}
TROMS_FILENAME = "20230224_Bustadnamn Tromsø_Final_3.txt"
with open('lfs-data/enriched/BSN/20230224_Bustadnamn Tromsø_Final_3.txt') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar="\"")
    header = [x.lower() for x in next(reader)]
    print(header)
    for num, row in enumerate(reader):
        
        
        row[0] = row[0].split(",")[0] # Remove decimals from ID
        row[1] = row[1].split(",")[0] # Remove decimals from Stempel
        supplerte_troms[row[17]] = {a: b for a, b in zip(header, row)}
        supplerte_troms[row[17]]["source_row"] = num
        
        
# Rows from fixed not yet added - 
for key, value in supplerte_troms.items(): 
    idnr = value["stempel"]
    if idnr != "": # Håndter desimaler i troms-filen
        idnr = str(int(value["stempel"]))
    troms_id = str(int(value["id"]))
    oppslag = value["stadnamn"].strip()
    assert idnr or oppslag
    scan = value["biletesti"]
    assert ".tif" not in scan and ".001" in scan

    group_id, group_seed = metadata_uuid("TROMS", troms_id, idnr, oppslag)
    scan_id = metadata_uuid(scan)[0]


    # ADD COLLECTIONS
    assert value["kommune"] and value["fylke"]

    group_metadata = {
        "seed": group_seed,
        "label": (idnr + " " + oppslag).strip(),
        "keywords": [oppslag],
        "scans": [scan_id],
        "id_stamp": idnr,
        "collection": TROMS,
        "county": value["fylke"],
        "municipality": value["kommune"],
        "gnr": value["gnr"],
        "bnr": value["bnr"],
        "collector": value["innsamler"],
        "year": value["innsamlingsår"],
        "source_rows": {TROMS_FILENAME: [num]}
    }

    metadata["groups"][group_id] = group_metadata

    processed_images.add(scan)   
    image_counter[scan] += 1
    if scan in debug:
        print(scan)


# Valitation
print("Not processed:", (len(bildestier) - len(processed_images)))
print([x for x in bildestier if x not in processed_images])
assert len(processed_images) / len(bildestier) == 1
print("Processed more than once:\n",{x[0] for x in image_counter.most_common() if x[1] > 1})

with open("lfs-data/processed/BSN/manifest_basis.json", "w") as outfile:
    json.dump(metadata, outfile)