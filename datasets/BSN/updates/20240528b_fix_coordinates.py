
import csv
import json
# This script prepares a json that is injected into the json file that will be uploaded to ElasticSearch
updated_rows = {}
with open("lfs-data/updates/BSN/20240528c_bsn_fix_coordinates.txt", "r", encoding="utf-8") as update_file:
        update_reader = csv.reader(update_file, delimiter=';', quotechar='"')
        update_headings = next(update_reader)
        for row in update_reader:
            cells = dict(zip(update_headings, row))
            updated_rows[cells["uuid"]] = {
                 "X": cells["x"],
                "Y": cells["y"],
                "Koordinattype": cells["koordinattype"]
            }

with open("lfs-data/updates/BSN/20240528c_fix_coordinates.json", "w", encoding="utf-8") as update_file:
    json.dump(updated_rows, update_file, indent=4, ensure_ascii=False)

            