# Kombinasjoner av idnr og oppslagsord som mangler i de berikede dataene. I flere av tilfellene gjelder det underbruk som er ført
# opp på samme seddel som den overordnede posten, selv om de også har egen seddel.
from  lib.xml import element2dict
import uuid
import xml.etree.ElementTree as ET
import csv

LFS = "lfs-data/"

def metadata_uuid(*args):
    seed = "http://data.toponym.ub.uib.no/bustadnamn/manifests/" + "_".join(args)
    uid =  str(uuid.uuid3(uuid.NAMESPACE_URL, seed))
    return uid, seed

tree = ET.parse(LFS+'raw/BSN/BSN_original.xml')
root = tree.getroot()

local_instance_uuid = lambda *args, fragment="": str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/BSN/" +  "_".join(list(args)))) + fragment

enriched = set()

# Check that none of the combinations of idnr and oppslag are in the supplemented data
with open(LFS + "enriched/BSN/20230223_Manglende metadata_fikset_7.txt", encoding='latin-1') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
    header = [x.lower() for x in next(reader)]
    for num, row in enumerate(reader):
        if "ikke seddel" not in row[5]:
            enriched.add((row[4], row[5]))

with open(LFS + "enriched/BSN/20230224_Bustadnamn Tromsø_Final_3.txt", encoding='latin-1') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
    header = [x.lower() for x in next(reader)]
    for num, row in enumerate(reader):
        enriched.add((row[1], row[2]))

with open(LFS + "enriched/BSN/20230223_Bustadnamn_MASTER_2.txt", encoding='utf8') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    header = [x.lower() for x in next(reader)]
    for num, row in enumerate(reader):
        enriched.add((row[2], row[3]))




# Place names in BSN_orig not found in enriched data
missing = [["uuid", "biletesti", "lenke", "stempel", "stadnamn", "fylke", "herad", "gnr", "bnr"]]
for stnavn in root:
    data = element2dict(stnavn)
    oppsl = None
    idnr = data["stnavn"].get("@idnr")
    if type(data["stnavn"].get("oppslag")) == list:
        oppsl = data["stnavn"]["oppslag"][0].get("oppslord", "")
    elif data["stnavn"].get("oppslag"):
        oppsl = data["stnavn"]["oppslag"].get("oppslord", "")
    
    if oppsl:
        if (idnr, oppsl) not in enriched:
            item_uuid = local_instance_uuid("ID", idnr, oppsl)
            manifest_uuid, _ = metadata_uuid(idnr, oppsl)
            if type(data["stnavn"]["sted"]) == list:
                fylke = data["stnavn"]["sted"][0]["fylke"]
                kommune = data["stnavn"]["sted"][0]["herred"]
                gnr = data["stnavn"]["sted"][0].get("gårdsnr", "")
                bnr = data["stnavn"]["sted"][0].get("bruksnr", "")
            else:
                fylke = data["stnavn"]["sted"]["fylke"]
                kommune = data["stnavn"]["sted"]["herred"]
                gnr = data["stnavn"]["sted"].get("gårdsnr", "")
                bnr = data["stnavn"]["sted"].get("bruksnr", "")
            
            if type(bnr) == list:
                bnr = ", ".join(bnr)
            if type(gnr) == list:
                gnr = ", ".join(gnr)


            missing.append([
                item_uuid, 
                data["stnavn"].get("@bilete", ""),
                f"https://iiif.test.ubbe.no/viewer/viewer.html?manifest=https://iiif.test.ubbe.no/iiif/manifest/{manifest_uuid}.json" if data["stnavn"].get("@bilete") else "",
                data["stnavn"]["@idnr"],
                oppsl,
                fylke,
                kommune,
                gnr,
                bnr
                
            ])


with open("datasets/BSN/updates/output/20240212_not_enriched.txt", "w", encoding="utf8") as f:
    for line in missing:
        f.write(";".join([f"\"{item}\"" for item in line]) + "\n")