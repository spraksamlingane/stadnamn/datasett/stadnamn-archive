
import csv

missing = []
with open("lfs-data/enriched/BSN/20230223_Manglende metadata_fikset_7.txt", encoding='latin-1') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
    header = [x.lower() for x in next(reader)]
    for row in reader:
        if "ikke seddel" not in row[5] and (not row[15] or row[15] == row[16]):
            missing.append(row)


with open("datasets/BSN/updates/output/Manglende metadata_fikset_7_fikse_koordinat.txt", "w", newline="", encoding="utf-8") as csvfile:
    writer = csv.writer(csvfile, delimiter=";", quotechar='"')
    writer.writerow(header)
    writer.writerows(missing)
            