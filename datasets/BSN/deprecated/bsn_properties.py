# Extracts properties from xml that serve as the basis of the dataset specific ontology
import xml.etree.ElementTree as ET
RAW = "lfs-data/raw/"
tree = ET.parse(RAW+'BSN/BSN_original.xml')
root = tree.getroot()
object_properties = {}
all_properties = {}
attribute_properties = {}

def parse(node, path=""):
    # add attributes as data props
    for attribute in node.attrib:
        attribute_path = path + "/" + attribute
        assert attribute_properties.get(attribute, attribute_path) == attribute_path
        attribute_properties[attribute] = attribute_path

    for child in node:
        assert all_properties.get(node.tag, path) == path, (node.tag, path, all_properties[node.tag]) # Detect duplicate prop names
        object_properties[node.tag] = path
        
        childpath = path + "/" + child.tag
        assert all_properties.get(child.tag, childpath) == childpath, (child.tag, childpath, all_properties[child.tag]) # Detect duplicate prop names
        all_properties[child.tag] = childpath

        parse(child, childpath)


for child in root:
    parse(child)


data_properties = {key: value for key, value in all_properties.items() if key not in object_properties}

with open("datasets/BSN/metadata/data_properties.txt", "w", encoding="utf-8") as outfile:
    for key in data_properties:
        outfile.write(key + "\n")
    outfile.write("attributeProperty\n")
    for key in attribute_properties:
        outfile.write("\t" + key + "\n")


with open("datasets/BSN/metadata/object_properties.txt", "w", encoding="utf-8") as outfile:
    for key in object_properties:
        outfile.write(key + "\n")
