from rdflib import Namespace, RDF, RDFS, Graph, Literal, XSD, BNode, DCTERMS, URIRef, DCAT, SKOS, OWL, DCMITYPE, PROV
import uuid
import xml.etree.ElementTree as ET
import sys
import json
import time

start = time.time()

TEST = len(sys.argv) == 1
TEST_SAMPLE = {"04EID27.BSN", "1arema01.bsn", "Diverse 2023", "Troms 2023", "1kraak01.bsn"}


# Load ontology
SPRAK = Namespace("https://data.spraksamlingane.no/id/")
IIIF_MANIFEST = Namespace("https://iiif.spraksamlingane.no/iiif/manifest/")
IIIF_PREZI = Namespace("http://iiif.io/api/presentation/3#")
ontology = Graph()
ontology.parse("datasets/BSN/bsn_ontology.ttl")
graph = Graph()

NAMESPACES = {x[0]: x[1] for x in ontology.namespaces()}
BSN = Namespace(NAMESPACES[""])

graph.bind("dct", DCTERMS)
graph.bind("dcat", DCAT)
graph.bind("skos", SKOS)
graph.bind("spr", SPRAK)
graph.bind("prov", PROV)
graph.bind("iiifm", IIIF_MANIFEST)
graph.bind("iifp", IIIF_PREZI)
graph.bind("", BSN)

CLASS = {str(result[0][len(BSN):]): result[0] for result in ontology.query("SELECT ?class WHERE { ?class a owl:Class }")}
OBJPROP = {str(result[0][len(BSN):]): result[0] for result in ontology.query("SELECT ?prop WHERE { ?prop rdfs:subPropertyOf :rawObjectProperty }")}
DATAPROP = {str(result[0][len(BSN):]): result[0] for result in ontology.query("SELECT ?prop WHERE { ?prop rdfs:subPropertyOf+ :rawDataProperty }")} 
ADDED_PROP = {str(result[0][len(BSN):]): result[0] for result in ontology.query("SELECT ?prop WHERE { ?prop rdfs:subPropertyOf ?propertyClass . FILTER NOT EXISTS { ?prop rdfs:subPropertyOf :rawDataProperty } FILTER NOT EXISTS { ?prop rdfs:subPropertyOf :rawObjectProperty }}")}

local_instance_uri = lambda *args, fragment="": SPRAK[str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/BSN/" +  "_".join(list(args)))) + fragment]

# Helper functions

def has_content(node):
    for x in node:
        if x.text:
            return True
        if has_content(x):
            return True
    return False

def assert_one_to_one(row, node, tag):
    assert len(node.findall(tag)) == 1, print(len(row.findall(tag)), row.attrib["idnr"])

def add_data_props(parent_uri, iterable):
    for item in iterable:
        if item.tag in DATAPROP and item.text:
            graph.add((parent_uri, DATAPROP[item.tag], Literal(item.text)))
            
            #TODO:Language tag?

def add_bnode_obj_prop(uri, row, tag):
    for node in row.findall(tag):
        if has_content(node):
            node_uri = BNode()
            graph.add((uri, OBJPROP[tag], node_uri))
            add_data_props(node_uri, node)



# Get metadata for keywords that have images
with open("lfs-data/processed/BSN/manifest_basis.json") as infile:
    manifest_data = json.load(infile)


# Get metadata from manifests file
print("Add manifest data")

for identifier, data in manifest_data["groups"].items():
    if TEST and manifest_data["collections"][data["collection"]]["label"] not in TEST_SAMPLE:
        continue
    manifest_uri = IIIF_MANIFEST[identifier]
    graph.add((manifest_uri, RDF.type, IIIF_PREZI.Manifest))
    collection_uri = IIIF_MANIFEST[data["collection"]]
    graph.add((collection_uri, RDF.type, IIIF_PREZI.Collection))
    graph.add((collection_uri, DCTERMS.identifier, Literal(data["collection"])))

    id_stamp = data.get("id_stamp", "")
    keyword = data.get("keywords", [""])[0]
    row_uri = local_instance_uri("ID", id_stamp, keyword) # Our master data
    graph.add((row_uri, DCTERMS.type, DCMITYPE.Text))
    graph.add((row_uri, RDF.type, CLASS["Source"]))

    # Add source
    source_uri = BNode()  # The primary source, both image and metadata
    graph.add((source_uri, RDF.type, CLASS["OriginalResource"]))
    graph.add((source_uri, DCTERMS.type, DCMITYPE.Text))
    graph.add((row_uri, DCTERMS.source, source_uri))


    # Add manifest
    graph.add((row_uri, DCTERMS.isReferencedBy, manifest_uri))
    graph.add((manifest_uri, DCTERMS.identifier, Literal(identifier)))

    if "keywords" in data:
        graph.add((row_uri, RDFS.label, Literal(data["keywords"][0])))

    # Add collection
    graph.add((manifest_uri, DCTERMS.isPartOf, collection_uri))
    graph.add((collection_uri, RDFS.label, Literal(manifest_data["collections"][data["collection"]]["label"])))

    # Add extra metadata if it was supplemented by Språksamlingane
    #if "BSN_original.xml" not in data["source_rows"]:

    if data.get("collector"):
        graph.add((source_uri, DCTERMS.creator, Literal(data["collector"])))
    if data.get("year"):
        graph.add((source_uri, DCTERMS.date, Literal(data["year"])))

    for x in ["gnr", "bnr", "county", "municipality"]:
        if data.get(x):
            text = data[x] if isinstance(data[x], str) else ", ".join(data[x])
            graph.add((row_uri, ADDED_PROP[x], Literal(text)))
        
                
    # Add references to original files, mostly for documentation and debugging purposes
    for key, values in data["source_rows"].items():
        data_rows = BNode()
        graph.add((row_uri, PROV.wasDerivedFrom, data_rows))
        graph.add((data_rows, ADDED_PROP["sourceFilename"], Literal(key)))    
        for page in values:
            graph.add((data_rows, ADDED_PROP["itemNumber"], Literal(page, datatype=XSD.integer)))
            

# cleanup
del manifest_data

# Add original data
print("load xml")

RAW = "lfs-data/raw/"
tree = ET.parse(RAW+'BSN/BSN_original.xml')
root = tree.getroot()

print("Convert xml to rdf")
for num, row in enumerate(root):
    kjelde = row.attrib["kjelde"]
    if TEST and kjelde not in TEST_SAMPLE:
        continue

    id_stamp = row.attrib["idnr"]
    keyword = row.find("oppslag").find("oppslord").text or ""
    image = row.attrib["bilete"]

    row_uri = local_instance_uri("ID", id_stamp, keyword) # Spraksamlingene minimal enrichment
    stnavn = local_instance_uri("ID-original", id_stamp, keyword) # Minimally encriched original data


    # Test: intercept instances with image but no manifest
    if (image and (row_uri, None, None) not in graph):
        print("NOT IN GRAPH:",image, "ROW:", row_uri, keyword, id_stamp)

    # Add data for rows without manifest
    if not image and (row_uri, DCTERMS.source, None) not in graph:

        if keyword:
            graph.add((row_uri, RDFS.label, Literal(keyword)))


        graph.add((row_uri, DCTERMS.type, DCMITYPE.Text))

        # Add source
        source_uri = BNode() # The primary source, both image and metadata
        graph.add((source_uri, DCTERMS.type, DCMITYPE.Text))
        graph.add((source_uri, RDF.type, CLASS["OriginalResource"]))
        graph.add((row_uri, DCTERMS.source, source_uri))

        data_rows = BNode()
        graph.add((row_uri, PROV.wasDerivedFrom, data_rows))
        graph.add((data_rows, ADDED_PROP["sourceFilename"], Literal("BSN_original.xml")))   
        graph.add((data_rows, ADDED_PROP["itemNumber"], Literal(num, datatype=XSD.integer)))


    if (stnavn, None, None) not in graph:
        graph.add((row_uri, DCTERMS.hasPart, stnavn))
        graph.add((stnavn, RDF.type, CLASS["Stnavn"]))


        # Add data properties
        add_data_props(stnavn, row)

        # Add blank node properties
        add_bnode_obj_prop(stnavn, row, 'oppslag')
        add_bnode_obj_prop(stnavn, row, 'gmlsform')
        add_bnode_obj_prop(stnavn, row, 'parform')
        add_bnode_obj_prop(stnavn, row, 'undbruk')
        add_bnode_obj_prop(stnavn, row, 'litt')
        add_bnode_obj_prop(stnavn, row, 'inform')
        add_bnode_obj_prop(stnavn, row, 'oppskr')
        add_bnode_obj_prop(stnavn, row, 'loktype')
        add_bnode_obj_prop(stnavn, row, 'sted')

    # Add attributes
    if id_stamp:
        graph.add((stnavn, DATAPROP["idnr"], Literal(id_stamp)))
    if image:
        graph.add((stnavn, DATAPROP["bilete"], Literal(image)))
    if image:
        graph.add((stnavn, DATAPROP["bilete"], Literal(image)))
    if kjelde:
        graph.add((stnavn, DATAPROP["kjelde"], Literal(kjelde)))


del tree
del root
print("serializing")
graph.serialize("datasets/BSN/bsn_sample.ttl" if TEST else "lfs-data/rdf/BSN/" + sys.argv[1], format="turtle") #TODO: handle output format

print(time.time() - start)