# Identify duplicates rows where only the attributes differ
# In order to preserve the structure of the original dataset, xml2rdf no longer ignores duplicates.
import xml.etree.ElementTree as ET
tree = ET.parse('lfs-data/raw/BSN/BSN_original.xml')
root = tree.getroot()

def identical_nodes(a, b):
    if a.text != b.text or a.tag != b.tag:
        return False
    
    for a_child, b_child in zip(a, b):
        if not identical_nodes(a_child, b_child):
            return False
    return True
    

def no_duplicates(row, potential_duplicates):
    for other in potential_duplicates:
        identical = identical_nodes(row, other)
        if identical:
            return False
    return True


processed = {}
processed_num = {}
duplicates = set()
with open("datasets/BSN/metadata/duplicates.txt", "w") as outfile:
    for num, row in enumerate(root):
        if row.attrib["idnr"] not in processed or no_duplicates(row, processed[row.attrib["idnr"]]):
            processed[row.attrib["idnr"]] = processed.get(row.attrib["idnr"], [row])
            processed_num[row.attrib["idnr"]] = processed_num.get(row.attrib["idnr"], num)
        else:
            outfile.write(str(num)+";"+str(processed_num[row.attrib["idnr"]])+"\n")