import xml.etree.ElementTree as ET
import csv
import json
from stadnamn.elasticsearch import es_upload, save_elastic_json
import uuid
from stadnamn.xml import element2dict
from stadnamn.vocab.coordinate_types import get_coordinate_type
import os


def metadata_uuid(*args):
    seed = "http://data.toponym.ub.uib.no/bustadnamn/manifests/" + "_".join(args)
    uid =  str(uuid.uuid3(uuid.NAMESPACE_URL, seed))
    return uid, seed

seen = set()
def local_instance_uuid(*args, fragment=""):
    new_uuid = str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/BSN/" +  "_".join(list(args)))) + fragment
    #assert new_uuid not in seen, args
    seen.add(new_uuid)
    return new_uuid


tree = ET.parse('lfs-data/raw/BSN/BSN_original.xml')
root = tree.getroot()

print("Loading enriched data...")
enriched = {}
id_pairs = set()
with open("lfs-data/enriched/BSN/20230223_Bustadnamn_MASTER_2.txt", encoding='utf8') as csvfile:
    reader = csv.reader(csvfile, delimiter=';')
    header = [x.lower() for x in next(reader)]
    for num, row in enumerate(reader):
        enriched[(row[2], row[3])] = {a: b for a, b in zip(header, row)}
        id_pairs.add((row[2], row[3]))


# Load data enriched in 2024
enriched_2024 = {}
print("Loading enriched data from 2024...")
with open("lfs-data/enriched/BSN/20240219_Manglende metadata_fikset.txt", encoding='utf8') as csvfile:
    reader = csv.reader(csvfile, delimiter='\t')
    header = [x.lower() for x in next(reader)]
    print(header)
    for num, row in enumerate(reader):
        enriched_2024[(row[4], row[5])] = {a: b for a, b in zip(header, row)}
        id_pairs.add((row[4], row[5]))


        


# Load supplemented metadata added by språksamlingane
print("Loading supplemented data...")
supplemented = {}
supplemented_troms = {}
with open("lfs-data/enriched/BSN/20230223_Manglende metadata_fikset_7.txt", encoding='latin-1') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
    header = [x.lower() for x in next(reader)]
    for num, row in enumerate(reader):
        if "ikke seddel" not in row[5]:
            supplemented[(row[4], row[5])] = {a: b for a, b in zip(header, row)}
            id_pairs.add((row[4], row[5]))

print("Loading supplemented data from Troms...")
with open("lfs-data/enriched/BSN/20230224_Bustadnamn Tromsø_Final_3.txt", encoding='latin-1') as csvfile:
    reader = csv.reader(csvfile, delimiter=';', quotechar='"')
    header = [x.lower() for x in next(reader)]
    for num, row in enumerate(reader):
        supplemented_troms[(row[1], row[2])] = {a: b for a, b in zip(header, row)}
        id_pairs.add((row[1], row[2]))

    

# Unused code. Might be useful when creating SNID-dataset, in order to find names without SNID.
"""# Place names in BSN_orig not found in enriched data
print("Add names not found in enriched data...")
missing = [["uuid", "biletesti", "lenke", "stempel", "stadnamn", "fylke", "herad"]]
for stnavn in root:
    data = element2dict(stnavn)
    oppsl = None
    idnr = data["stnavn"].get("@idnr")
    if type(data["stnavn"].get("oppslag")) == list:
        oppsl = data["stnavn"]["oppslag"][0].get("oppslord", "")
    elif data["stnavn"].get("oppslag"):
        oppsl = data["stnavn"]["oppslag"].get("oppslord", "")
    
    if oppsl:
        if (idnr, oppsl) not in id_pairs:
            item_uuid = local_instance_uuid("ID", idnr, oppsl)
            seen.add(item_uuid)
            manifest_uuid = metadata_uuid(idnr, oppsl)
            if type(data["stnavn"]["sted"]) == list:
                fylke = data["stnavn"]["sted"][0]["fylke"]
                kommune = data["stnavn"]["sted"][0]["herred"]
            else:
                fylke = data["stnavn"]["sted"]["fylke"]
                kommune = data["stnavn"]["sted"]["herred"]

            missing.append([
                item_uuid, 
                data["stnavn"].get("@bilete", ""),
                f"https://iiif.test.ubbe.no/viewer/viewer.html?manifest=https://iiif.test.ubbe.no/iiif/manifest/{manifest_uuid}.json",
                data["stnavn"]["@idnr"],
                oppsl,
                fylke,
                kommune
                
            ])

print(len(missing), "missing names found in BSN_orig")
print(missing)"""




json_data = []

json_data_map = {}

with open("lfs-data/processed/BSN/manifest_basis.json", "r", encoding="utf8") as f:
    image_info = json.load(f)

print("Add manifest data...")
for stnavn in root:
    data = element2dict(stnavn)
    IDNR = data["stnavn"]["@idnr"]
    source = data["stnavn"]["@kjelde"]
    if type(data["stnavn"].get("oppslag")) == list:
        OPPSL = data["stnavn"]["oppslag"][0].get("oppslord", "")
    elif data["stnavn"].get("oppslag"):
        OPPSL = data["stnavn"]["oppslag"].get("oppslord", "")
    else:
       OPPSL = ""

    if (IDNR, OPPSL) in json_data_map: # Ignore duplicates - the bilete attr is the only difference
        bilete = data["stnavn"].get("@bilete")
        if bilete:
            existing_bilete =  json_data_map[(IDNR, OPPSL)]["rawData"]["original"]["stnavn"].get("@bilete")
            if type(existing_bilete) == list and bilete not in existing_bilete:
                existing_bilete.append(bilete)
            if type(existing_bilete) == str and existing_bilete != bilete:
                json_data_map[(IDNR, OPPSL)]["rawData"]["original"]["stnavn"]["@bilete"] = [existing_bilete, bilete]
        continue


    label = OPPSL.strip("\"")

    item_uuid = local_instance_uuid("ID", IDNR, OPPSL)

    
    attestations = []

    if "gmlsform" in data["stnavn"]:
        form_list = data["stnavn"]["gmlsform"] if type(data["stnavn"]["gmlsform"]) == list else [data["stnavn"]["gmlsform"]]
        for old_form in form_list:
            if type(old_form) == str:
                print(old_form)
            attestation = {
                "label": old_form["navnform"],
            }
            if "source" in old_form:
                attestation["source"] = old_form["source"]

            if "gf_fraår" in old_form:
                attestation["year"] = old_form["gf_fraår"]
            
            attestations.append(attestation)
    
    altLabels = []
    if "parform" in data["stnavn"]:
        form_list = data["stnavn"]["parform"] if type(data["stnavn"]["parform"]) == list else [data["stnavn"]["parform"]]
        for parform in form_list:
            if "pf_navn" in parform:
                altLabels.append(parform["pf_navn"])
    


    row = {
        "label": label.strip("\"? *"),
        "uuid": item_uuid,
        "rawData": {
            "original": data
        }
    }

    if altLabels:
        row["altLabels"] = altLabels

    if attestations:
        row["attestations"] = attestations

    
            


    if (IDNR, OPPSL) in enriched:
        row["rawData"]["supplemented"] = enriched[(IDNR, OPPSL)]
        row["snid"] = enriched[(IDNR, OPPSL)]["snid"]
        row["gnidu"] = enriched[(IDNR, OPPSL)]["gnidu"]
        if "midu" in enriched[(IDNR, OPPSL)]:
            row["midu"] = enriched[(IDNR, OPPSL)]["midu"]

    if (IDNR, OPPSL) in enriched_2024:
        row["rawData"]["supplemented"] = enriched_2024[(IDNR, OPPSL)]
        row["snid"] = enriched_2024[(IDNR, OPPSL)]["snid"]
        row["gnidu"] = enriched[(IDNR, OPPSL)]["gnidu"]
        if "midu" in enriched[(IDNR, OPPSL)]:
            row["midu"] = enriched[(IDNR, OPPSL)]["midu"]

    if (IDNR, OPPSL) in enriched and enriched[(IDNR, OPPSL)]["x"]:
        row["coordinateType"] = get_coordinate_type(enriched[(IDNR, OPPSL)]["koordinattype"])
        row["location"] = {
                "type": "Point",
                "coordinates": [float(enriched[(IDNR, OPPSL)]["x"]), float(enriched[(IDNR, OPPSL)]["y"])]
            }
    elif (IDNR, OPPSL) in enriched_2024 and enriched_2024[(IDNR, OPPSL)]["x"]:
        row["coordinateType"] = get_coordinate_type(enriched_2024[(IDNR, OPPSL)]["koordinattype"])
        row["location"] = {
                "type": "Point",
                "coordinates": [float(enriched_2024[(IDNR, OPPSL)]["x"]), float(enriched_2024[(IDNR, OPPSL)]["y"])]
            }
        
    if data["stnavn"].get("@bilete"):
        manifest_uuid, _ = metadata_uuid(IDNR, OPPSL)
        assert manifest_uuid in image_info["groups"], f"Manifest {manifest_uuid} for {IDNR}, {OPPSL} not found in image_info"
        row["image"] = {
            "manifest": manifest_uuid,
        }
    
    json_data_map[(IDNR, OPPSL)] = row
    assert item_uuid not in json_data, f"Duplicate uuid: {item_uuid}"
    assert type(item_uuid) == str, item_uuid
    json_data.append(row)


# Create uniform adm hierarchy by finding the most frequent municipality and county for each source
print("Create uniform adm hierarchy...")
from collections import Counter
kommune_counters = {}
fylke_counters = {}
for row in json_data:
    kjelde = row["rawData"]["original"]["stnavn"]["@kjelde"]
    steder = row["rawData"]["original"]["stnavn"]["sted"]
    if type(steder) != list:
        steder = [steder]
    for sted in steder:
        kommune = sted["herred"]
        if not kommune_counters.get(kjelde):
            kommune_counters[kjelde] = Counter()

        kommune_counters[kjelde][kommune] += 1

        fylke = sted["fylke"]

        if fylke == "Vest- Agder":
            fylke = "Vest-Agder"


        if not fylke_counters.get(kjelde):
            fylke_counters[kjelde] = Counter()
        fylke_counters[kjelde][fylke] += 1


kommune_mapping = {key: val.most_common() for key, val in kommune_counters.items()}
def replace_archaic(kommune):
    if kommune == "Nittedalen":
        return "Nittedal"
    if kommune == "Hurdalen":
        return "Hurdal"
    return kommune

source2adm = {key: [replace_archaic(kommune_mapping[key][0][0]), val.most_common()[0][0]] for key, val in fylke_counters.items()}
for num, value in enumerate(json_data):
    json_data[num]["adm2"], json_data[num]["adm1"] = source2adm[value["rawData"]["original"]["stnavn"]["@kjelde"]]
    


print("Add troms data to json...")
for value in supplemented_troms.values():
    # Clean data
    idnr = value["stempel"]
    if idnr != "": # Håndter desimaler i troms-filen
        idnr = value["stempel"].split(",")[0]
    troms_id = value["id"].split(",")[0]
    oppslag = value["stadnamn"].strip()
    assert idnr or oppslag
    scan = value["biletesti"]
    assert ".tif" not in scan and ".001" in scan

    iif_group_id, _ = metadata_uuid("TROMS", troms_id, idnr, oppslag)

    instance_uuid = local_instance_uuid("ID", idnr, oppslag)

    data = {
        "image": {
            "manifest": iif_group_id,
        },
        "label": value["stadnamn"].strip("\"? * "),
        "uuid": instance_uuid,
        "adm1": value["fylke"],
        "adm2": value["kommune"],
        "snid": value["snid"],
        "rawData": {
            "supplemented": value
        }
    }
    if value["x"]:
        data["coordinateType"] = get_coordinate_type(value["koordinat_type"])
        data["location"] = {
            "type": "Point",
            "coordinates": [float(value["x"]), float(value["y"])]
        }

    assert instance_uuid not in json_data, f"Duplicate uuid: {instance_uuid}"
    json_data.append(data)

    
inkluder_unummerert = {
    "cd02/00000143/00143306.001",
    "cd01/00000026/00026417.001"
}

print("Add supplemented data to json...")

"""missing_coord = open("bsn_fix_coordinates.csv", "w", encoding="utf8")
missing_coord_writer = csv.writer(missing_coord, delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL)
missing_coord_writer.writerow(["uuid", "label", "knr", "gnidu", "midu", "x", "y"])"""

for value in supplemented.values():
    # Clean data
    idnr = value["stempel"].strip()
    if idnr == "--ikke seddel--" or (value["status"] == "unummerert" and value["biletesti"] not in inkluder_unummerert): # Skip unnumbered (duplicates) and invalid data
        continue
    oppslag = value["stadnamn"].strip()

    


    manglet_id = value["id"]
    assert idnr or oppslag
    assert ".tif" not in scan and ".001" in scan

    iiif_group_id, _ = metadata_uuid(idnr, oppslag)
    if iiif_group_id not in image_info["groups"]:
        iiif_group_id = metadata_uuid("MANGLET", manglet_id, idnr, oppslag)[0]
        assert iiif_group_id in image_info["groups"], f"Manifest {iiif_group_id} for {idnr}, {oppslag} not found in image_info"

    if (idnr, oppslag) in json_data_map:
        if not json_data_map[(idnr, oppslag)].get("image"):
            json_data_map[(idnr, oppslag)]["image"] = {
                "manifest": iiif_group_id
            }

        
        if not json_data_map[(idnr, oppslag)].get("location"):
            print("add missing coordinates")
            if value["x"]:
                json_data_map[(idnr, oppslag)]["coordinateType"] = value["koordinattype"]
                json_data_map[(idnr, oppslag)]["location"] = {
                    "type": "Point",
                    "coordinates": [float(value["x"]), float(value["y"])]
                }
        
        continue

    instance_uuid = local_instance_uuid("ID", idnr, oppslag)
    data = {
        "image": {
            "manifest": iiif_group_id,
        },
        
        "label": value["stadnamn"].strip("\"? *"),
        "uuid": instance_uuid,
        "adm1": value["fylke"],
        "adm2": value["herad"],
        "snid": value["snid"],
        "rawData": {
            "supplemented": value
        }
    }

    if value.get("gnidu"):
        data["gnidu"] = value["gnidu"]
    
    if value.get("midu"):
        data["midu"] = value["midu"]



    assert value["x"] != "0" and value["y"]!= "0", f"Missing coordinates for {idnr}, {oppslag}"
    """if (value["x"] or value["y"]) and float(value["y"]) < 56:
        missing_coord_writer.writerow([data["uuid"], data["label"], value["knr"], value["gnidu"], value["midu"], value["x"], value["y"]])"""

    
    if value["x"] and value["y"]:
        data["coordinateType"] = value["koordinattype"]
        data["location"] = {
            "type": "Point",
            "coordinates": [float(value["x"]), float(value["y"])],
        }

    assert instance_uuid not in json_data, f"Duplicate uuid: {instance_uuid}"
    json_data.append(data)

#missing_coord.close()



# Updates
uuid2index = {value["uuid"]: num for num, value in enumerate(json_data)}
for f in sorted(os.listdir('lfs-data/updates/BSN/')):
    if f.endswith('.json'):
        with open(f'lfs-data/updates/BSN/{f}', 'r', encoding='utf8') as update_file:
            updates = json.load(update_file)
            for uid, value in updates.items():
                json_data[uuid2index[uid]]["location"]["coordinates"][0] = float(value["X"])
                json_data[uuid2index[uid]]["location"]["coordinates"][1] = float(value["Y"])
                json_data[uuid2index[uid]]["coordinateType"] = get_coordinate_type(value["Koordinattype"])


with open('.tmp/bsn_data.json', 'w', encoding='utf8') as f:
    json.dump(json_data, f, ensure_ascii=False, indent=2)