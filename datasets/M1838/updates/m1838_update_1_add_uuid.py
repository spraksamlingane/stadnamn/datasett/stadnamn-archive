import csv
import uuid

SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/"
SEED_DATASET = "M1838"
def get_uuid(columns, values):
    # Cancatenate if key is a tuple
    if isinstance(columns, tuple):
        columns = "_".join(columns)
        values = "_".join(values)
    seed = f"{SEED_NAMESPACE}{SEED_DATASET}_{columns}_{values}"

    return str(uuid.uuid3(uuid.NAMESPACE_URL, seed))

with open("lfs-data/processed/M1838/m1838_complete.csv", "r", encoding="latin-1") as f:
    reader = csv.reader(f, delimiter='\t')
    headings = next(reader)
    # Add uuid to each row and save to new file
    with open("lfs-data/processed/M1838/m1838_norsampo_uuid.csv", "w", encoding="utf-8", newline="") as f2:
        
        writer = csv.writer(f2, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerow(headings + ["uuid"])

        for row in reader:
            cells = dict(zip(headings, row))
            if cells["Lokalitetstype"] == 'Bruk':
                writer.writerow(row + [get_uuid("ID", cells["ID"])])          
            elif cells["Lokalitetstype"] == 'Gard':
                writer.writerow(row + [get_uuid("M1838_GardID", cells["M1838_GardID"])])

            


