import csv
from stadnamn.wikidata import ADM_MAPPING
import uuid
import re
from stadnamn.vocab.sosi import *
from stadnamn.vocab.coordinate_types import get_coordinate_type
from stadnamn.elasticsearch import es_upload, save_elastic_json

json_data = []
seen = set()

SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/"
SEED_DATASET = "M1838"
def get_uuid(columns, values):
    # Cancatenate if key is a tuple
    if isinstance(columns, tuple):
        columns = "_".join(columns)
        values = "_".join(values)
    seed = f"{SEED_NAMESPACE}{SEED_DATASET}_{columns}_{values}"

    return str(uuid.uuid3(uuid.NAMESPACE_URL, seed))


"""missing_coord = open("m1838_fix_coordinates.csv", "w", encoding="utf8", newline="")
missing_coord_writer = csv.writer(missing_coord, delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL)
missing_coord_writer.writerow(["uuid", "label", "knr", "gnidu", "midu", "x", "y"])
"""

incorrect_adm2_transcription = {
    "Melhuu": "Melhuus",
}

gard_labels = {}
cadastral_sort_index = []

with open('lfs-data/processed/M1838/m1838_updated.csv', 'r', encoding="utf8") as f:
    reader = csv.reader(f, delimiter=';', quotechar='"')
    header = next(reader)
    print(header)


    for row in reader:
        cells = dict(zip(header, row))
        data = {
            "uuid": cells["uuid"],
            "label": cells["Stadnamn"],
            "adm1": cells["Amt"],
            "adm2": incorrect_adm2_transcription.get(cells["Prestegjeld"], cells["Prestegjeld"]),
            "link": cells["Lenke_til_skannet_matrikkel"],
            "snid": cells["SNID"],
            "year": cells["År"]
        }


        if cells["KNRu"] in ADM_MAPPING:
            data["wikiAdm"] = ADM_MAPPING[cells["KNRu"]]["id"]
        else:
            # Two municipalities are missing from the mapping
            missing_knru = {'1147': 'Q23042303', '1752': 'Q52367652'}.get(cells["KNR"])
            if missing_knru:
                data["wikiAdm"] = missing_knru
            

        if cells.get("MIDu"):
            data["midu"] = cells["MIDu"]
        
        if cells.get("GNIDu"):
            data["gnidu"] = cells["GNIDu"]

        assert cells["Lokalitetstype"].lower() in SOSI_CODES, "Unknown lokalitetstype: " + cells["Lokalitetstype"]

        
        

        if cells["Lokalitetstype"] == 'Bruk':
            data["within"] = get_uuid("M1838_GardID", cells["M1838_GardID"])
            data["sosi"] = SOSI_BRUK
            if cells.get("midu"):
                data["midu"] = cells["midu"]
        

        elif cells["Lokalitetstype"] == 'Gard':
            data["sosi"] = SOSI_GARD
            if cells.get("gnidu"):
                data["gnidu"] = cells["gnidu"]

            gard_labels[data["uuid"]] = data["label"]

        
        #if (cells["x"] or cells["y"]) and float(cells["y"]) < 56:
        #    missing_coord_writer.writerow([data["uuid"], data["label"], cells["knr"], cells["gnidu"], cells["midu"], cells["x"], cells["y"]])

        if cells.get("X"):
            data["coordinateType"] = get_coordinate_type(cells["Koordinat_type"])
            data["location"] = {
                "type": "Point",
                "coordinates": [float(cells["X"]), float(cells["Y"])]
            }
        
        # Omit fields added by Henrik
        data["rawData"] = {
            k: v for k, v in cells.items() if k not in ("unique", "admid", "KNRu", "FNRu")
        }

        # Get first number and first letter from MNR and LNR so that the cadastral information can be sorted numerically
        if cells.get("MNR"):
            # TODO: del inn etter tinglag i stedet for å sortere på lenken
            sorting = {"data": data, "knr": cells["KNR"], "lenke": cells["Lenke_til_skannet_matrikkel"]}
            if not re.match(r"[^\d]*(\d+)([a-z])?", cells["MNR"]):
                print("Invalid MNR:", cells["MNR"])
                
            mnr, mnrLetter = re.match(r"[^\d]*(\d+)([a-z])?", cells["MNR"]).groups()
            
            
            sorting["mnr"] = int(mnr)

            if mnrLetter:
                sorting["mnrLetter"] = mnrLetter

            if cells.get('LNR'):
                # Ignore invalid values
                if cells["LNR"] not in {"Mofjeld", "Høklid (Haukalid)", "¿"}:

                    lnr, lnrLetter = re.match(r"[^\d]*(\d+)([a-z])?", cells["LNR"]).groups()
                    sorting["lnr"] = int(lnr)
                    if lnrLetter:
                        sorting["lnrLetter"] = lnrLetter

            cadastral_sort_index.append(sorting)


        json_data.append(data)


for num, item in enumerate(sorted(cadastral_sort_index, key=lambda x: (x["knr"], x["lenke"], x["mnr"], x.get("mnrLetter", ""), x.get("lnr", 0), x.get("lnrLetter", "")))):
    item["data"]["cadastralIndex"] = num


for data in json_data:
    if data["sosi"] == "bruk":
        data["misc"] = data.get("misc", {})
        data["misc"]["gardLabel"] = gard_labels[data["within"]]


#missing_coord.close()

output_json = save_elastic_json("m1838", json_data, props = {
    "cadastralIndex": {
        "type": "integer"
    }
})
es_upload(output_json, "m1838")

        
