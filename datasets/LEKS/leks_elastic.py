import csv
from stadnamn.elasticsearch import es_upload, save_elastic_json
import time
import json
import uuid
from collections import defaultdict
import re
import yaml


json_data = []

debug = set()

SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/LEKS/"

entires = {}
headword_lookup = defaultdict(list)
headword_adm_lookup = defaultdict(dict)

# Add internal links to grunnord
with open('lfs-data/enriched/LEKS/Grunnord_NSL.txt', 'r', encoding="latin-1") as f:
    reader = csv.DictReader(f, delimiter=';', quotechar='"')

    for row in reader:
        uid = str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/LEKS_G/" + "_" + row["Id"]))
        headword_lookup[row["Grunnord"]].append(uid)


def process_internal_links(pattern, create_replace_pattern, html_content, data):
    internal_links = re.findall(pattern, html_content)
    for groups in internal_links:
        link = groups[0]
        district = groups[1]

        
        
        
        replace_pattern = create_replace_pattern(link, district)
        
        fixed_link = link
        if link not in internal_links:
            fixed_link = link.replace("-", " ").strip().split("(")[0].strip().strip(".").strip("†")


        links = headword_lookup.get(fixed_link, [])


        if  len(links) != 1:
            links = headword_adm_lookup.get(district, {}).get(fixed_link, links)
        
        if len(links) == 1:
            if fixed_link[0].isupper():
                html_content = re.sub(replace_pattern, fr"\1<a href='/view/leks/doc/{links[0]}'>{link}</a>\2", html_content)
            else:
                html_content = re.sub(replace_pattern, fr"\1<a href='/uuid/{links[0]}'>{link}</a>\2", html_content)


        if len(links) > 1 and fixed_link[0].isupper():
            html_content = re.sub(replace_pattern, fr"\1<a href='/view/leks/doc/{data['uuid']}?q={fixed_link}'>{link}</a>\2", html_content)

    return html_content


# Read the file in advance to create a lookup table for internal links
with open('lfs-data/enriched/LEKS/NSL_20240610.txt', 'r', encoding="utf8") as f:
    reader = csv.DictReader(f, delimiter=';', quotechar='"')
    for row in reader:
        uid = str(uuid.uuid3(uuid.NAMESPACE_URL, SEED_NAMESPACE + "_" + row["Id"]))
        entires[uid] = row
        oppslord = row["Oppslagsform"].replace("\"", "").strip().replace("†", "").strip()
        headword_lookup[oppslord].append(uid)
        
        if row.get("Kommune"):
            kommune = row["Kommune"].split()[0]
            headword_adm_lookup[kommune][oppslord] = headword_adm_lookup[kommune].get(oppslord, []) + [uid]
        if row.get("Fylke"):
            fylke = row["Fylke"].split()[0]
            headword_adm_lookup[row["Fylke"]][oppslord] = headword_adm_lookup[kommune].get(oppslord, []) + [uid]

instance_patches = {}
with open("datasets/LEKS/patches/instance_patches.yml", "r", encoding="utf8") as f:
    for patch in yaml.safe_load(f):
        instance_patches[patch["uuid"]] = instance_patches.get(patch["uuid"], []) + [patch]

field_patches = {}

with open("datasets/LEKS/patches/field_patches.yml", "r", encoding="utf8") as f:
    for patch in yaml.safe_load(f):
        field_patches[patch["field"]] = field_patches.get(patch["field"], []) + [patch]


with open("datasets/LEKS/patches/wikiadm_fylke.yml", "r", encoding="utf8") as f:
    fylke_wikiadm = yaml.safe_load(f)


with open("lfs-data/processed/LEKS/adm/adm2wikidata.json", "r", encoding='utf8') as f:
    adm2wikidata = json.load(f)


for uid, cells in entires.items():

    data = {
        "uuid": uid,
        "label": cells["Oppslagsform"].strip("\""),
        "rawData": {k: v for k, v in cells.items() if v.strip()}
    }

    if cells.get("SNID"):
        data["snid"] = cells["SNID"]

    if cells.get("Fylke"):
        data["adm1"] = cells["Fylke"]


        orig_fylker = cells["Fylke"]
        fylker = re.split(r",\s|/", orig_fylker)
        
        kommuner = re.split(r",\s|/", cells['Kommune']) if cells.get("Kommune") else []

        if not kommuner or len(fylker) > 1:
            if len(fylker) == 1:
                data["adm1"] = fylker[0]
                data["wikiAdm"] = fylke_wikiadm[fylker[0].split(" - ")[0]]
            else:
                data["adm1"] = []
                for fylke in fylker:
                    data["adm1"].append(fylke)
                    data["wikiAdm"] = data.get("wikiAdm", []) + [fylke_wikiadm[fylke.split(" - ")[0]]]
                
                
        else:
            data["adm2"] = []
            data["wikiAdm"] = []
            for kommune in kommuner:
                wikidataId = adm2wikidata.get(orig_fylker, {}).get(kommune)
                # Ignore lists, as they are handled manually in instance_patches instead
                if type(wikidataId) == list:
                    continue
                
                if wikidataId:
                    data["wikiAdm"].append(wikidataId)
                data["adm2"].append(kommune)
            if len(data["adm2"]) == 1:
                data["adm2"] = data["adm2"][0]


            

    if cells.get("gnidu"):
        data["gnidu"] = cells["GNIDu"]

    if "Tolking" in cells:
        data["content"] = {}

        html_content = cells["Tolking"]

        pattern = r"→\s<em>(.*?)</em>(?:\s*i\s([\w\-]+))?"
        create_replace_pattern = lambda link, district: rf"(→\s<em>){re.escape(link)}(</em>\s*{'' if not district else f'i {re.escape(district)}'})"
        html_content = process_internal_links(pattern, create_replace_pattern, html_content, data)
        
        pattern = r"→\s([\w\-]+)(?:\s*i\s([\w\-]+))?"
        create_replace_pattern = lambda link, district: rf"(→\s){re.escape(link)}(\s*{'' if not district else f'i {re.escape(district)}'})"
        html_content = process_internal_links(pattern, create_replace_pattern, html_content, data)



        data["content"]["html"] = html_content



    

    if cells.get("Latitude"):
        assert cells.get("Longitude"), print(cells)
        data["location"] = {
            "type": "Point",
            "coordinates": [float(cells["Longitude"]), float(cells["Latitude"])]
        }

    # Fix errors
    for patch in instance_patches.get(uid, []):
        if "update" in patch:
            data.update(patch["update"])
        if "remove" in patch:
            for key in patch["remove"]:
                if key in data:
                    data.pop(key)

    for key in data:
        for patch in field_patches.get(key, []):
            if type(data[key]) == list:
                continue
            data[key] = patch["mapping"].get(data[key], data[key])
    


    json_data.append(data)

output_json = save_elastic_json("leks", json_data)
es_upload(output_json, "leks")

print(debug)

    
    
