import csv
from stadnamn.elasticsearch import es_upload, save_elastic_json
import time
import json
import uuid
import re

json_data = []

debug = set()

SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/LEKS_G/"


with open('lfs-data/enriched/LEKS/Grunnord_NSL.txt', 'r', encoding="latin-1") as f:
    reader = csv.reader(f, delimiter=';', quotechar='"')
    header = [x.lower() for x in next(reader)]
    print(header)

    for row in reader:
        cells = dict(zip(header, row))


        data = {
            "uuid": str(uuid.uuid3(uuid.NAMESPACE_URL, SEED_NAMESPACE + "_" + cells["id"])),
            "label": cells["grunnord"],
            "content": {"html": cells["forklaring"], "text": re.sub(r'</?[^>]*>', '', cells["forklaring"])},
            "rawData": cells
        }


        
        json_data.append(data)


output_json = save_elastic_json("leks_g", json_data)
es_upload(output_json, "leks_g")



        





print(debug)

    
    
