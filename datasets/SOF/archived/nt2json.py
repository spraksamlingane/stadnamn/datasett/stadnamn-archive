import re
import json
json_data = {}

with open("lfs-data/raw/SOF/stedsnavn-sof-data.nt", "r", encoding="utf8") as f:
    for line in f:
        match = re.match(r"^<https://resource.fylkesarkivet.no/placename/(.*)#> <.*#(.+)> \"(.+)\" <http://data.stadnamn.uib.no/stedsnavn/sogn-og-fjordane> \.$", line)
        if match:

            uuid, predicate, obj = match.groups()
            json_data[uuid] = json_data.get(uuid, {})
            assert predicate not in json_data[uuid], f"ERROR: {uuid} {predicate} {obj} - {json_data[uuid][predicate]}"
            json_data[uuid][predicate] = obj
        else:
            print(" NOT MATCHING LINE: ", line)
            break


with open("lfs-data/raw/SOF/stedsnavn-sof-data2.json", "w", encoding="utf8") as f:
    json.dump(json_data, f, indent=2, ensure_ascii=False)


