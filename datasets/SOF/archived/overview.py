import json

with open("lfs-data/raw/SOF/stedsnavn-sof-data.json", "r", encoding="utf8") as f:
    place_names = json.load(f)


# print all keys with the first occuring non empty value for each key
keys = {}
occurences = {}
length = 0

for uuid, value in place_names.items():
    length += 1
    for key, val in value.items():
        if key not in keys and val:
            keys[key] = val
        
        # count occurences that arent empty
        if val:
            if key in occurences:
                occurences[key] += 1
            else:
                occurences[key] = 1
        

# Create a list of tuples (key, value, percentage)
key_percentage_tuples = [(key, val, (occurences[key]/length) * 100) for key, val in keys.items()]

# Sort the list by the percentage in descending order
key_percentage_tuples.sort(key=lambda x: x[2], reverse=True)

# Pretty print keys
for key, val, percentage in key_percentage_tuples:
    print(f"{key}: {val} ({round(percentage)}%)\n")
    