from stadnamn.elasticsearch import es_upload, save_elastic_json
import json
from collections import defaultdict, Counter

with open("lfs-data/raw/SOF/stedsnavn-sof-data.json", "r", encoding="utf8") as f:
    place_names = json.load(f)

with open("lfs-data/processed/SOF/adm/adm2wikidata.json", "r", encoding='utf8') as f:
    adm2wikidata = json.load(f)


output_list = []
errors = 0

admfix = defaultdict(Counter)
# Clean up administative divisions
for values in place_names.values():
    knr = values.get("KommuneNr")
    if not knr or len(knr) != 4:
        continue

    if "Kommune" in values:
        admfix[knr][values["Kommune"]] += 1
    
    if "Fylke" in values:
        fylkenr = knr[:2]
        admfix[fylkenr][values["Fylke"]] += 1

# Most common name for each code
adm_clean = {key: value.most_common(1)[0][0] for key, value in admfix.items()}


for uuid, values in place_names.items():

    if "Normform" not in values and "OppsForm" not in values:
        errors += 1
        continue
    
    # Remove test entry https://stadnamn.fylkesarkivet.no/placename/6eca07e0-c3bc-e811-810c-001dd8b71c74
    if "OppsForm" == "xxx":
        continue

    data = {
        "uuid": uuid,
        "label": values.get("Normform", values.get("OppsForm")),
        "link": "https://stadnamn.fylkesarkivet.no/placename/" +uuid,
        "rawData": values
    }

    if "Kulturkode1" in values:
        kulturkode_info = values["Kulturkode1"].split(" - ")
        data["misc"] = data.get("misc", {}) | {
            "kulturKode": kulturkode_info[0],
            "label": "- ".join(kulturkode_info[1:-1])
        }

    if "KommuneNr" in values and len(values["KommuneNr"]) == 4:
        data["adm2"] = adm_clean[values["KommuneNr"]]
        fylkenr = values["KommuneNr"][:2]
        data["adm1"] = adm_clean[fylkenr]

    else:
        if "Fylke" in values:
            data["adm1"] = values["Fylke"]
        if "Kommune" in values:
            data["adm2"] = values["Kommune"]
    
    assert data["label"], f"ERROR: No label {uuid} {values}"

    if "adm1" in data and "adm2" in data:
        data["wikiAdm"] = adm2wikidata[data["adm1"]][data["adm2"]]
    
    if "Wgs84Epsg4326Latitude" in values:
        assert "Wgs84Epsg4326Longitude" in values
        lat = float(values["Wgs84Epsg4326Latitude"])
        lng = float(values["Wgs84Epsg4326Longitude"])
        
        # Check that coordinates are within mainland Norway
        if lat > 58.1 and lat < 71 and lng > 2 and lng < 32:
            data["location"] = {
                "type": "Point",
                "coordinates": [float(values["Wgs84Epsg4326Longitude"]), float(values["Wgs84Epsg4326Latitude"])]
            }
    

    
    output_list.append(data)


    

outpus_json = save_elastic_json("sof", output_list)
print(f"Errors: {errors}")

es_upload(outpus_json, "sof")


