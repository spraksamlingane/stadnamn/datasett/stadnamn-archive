import json
import csv
# Check if all enhetsnr in the enriched data is in the oracle data

with open("lfs-data/raw/RYGH/oracle/rygh-1717141951747.json", encoding="utf-8") as f:
    oracle_data = json.load(f)["RYGH_ENHETER"]


# The value should not contain enhetnr, since it's the key
oracle_data = {x["ENHETSNR"]: {k: v for k,v in x.items() if k !="ENHETSNR"} for x in oracle_data}

found = 0
total = 0

with open('lfs-data/processed/RYGH/rygh_updated.csv', 'r', encoding="utf8") as f:

    reader = csv.DictReader(f, delimiter=';', quotechar='"')
    
    for row in reader:
        if int(row["Enhetsnummer"]) in oracle_data:
            found += 1
        else:
            print("ENHETSNUMMER:", row["Enhetsnummer"])
            print(row["Digitalisert_tekst"])
            print()
        total += 1

print(f"Missing: {total - found}")
print((found/total) * 100, "%")


