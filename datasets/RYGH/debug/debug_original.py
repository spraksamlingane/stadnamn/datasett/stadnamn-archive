import csv
gard_ids = {}
duplicate_gard_ids = {}
teller = 0
gard_med_bnr = set()
garder_for_gard_med_bnr = set()
with open("lfs-data/raw/RYGH/rygh.csv", "r", encoding="latin-1") as f:
    reader = csv.reader(f, delimiter='\t')
    headings = next(reader)
    for row in reader:
        assert len(row) == 26
        cells = dict(zip(headings, row))
        if cells["Lokalitetstype"] == "Gard":
            if cells["Bnr"]:
                gard_med_bnr.add((cells["Knr"], cells["Gnr"]))
            else:
                garder_for_gard_med_bnr.add((cells["Knr"], cells["Gnr"]))
            gard_id = (cells["Knr"], cells["Gnr"])

            if gard_id in gard_ids:
                duplicate_gard_ids[gard_id] = duplicate_gard_ids.get(gard_id, set()) | {gard_ids[gard_id]["Stadnamn"]}
                duplicate_gard_ids[gard_id].add(gard_ids[gard_id]["Stadnamn"])
                print(cells["Stadnamn"], "|", gard_ids[gard_id]["Stadnamn"])
                print(cells["Knr"], cells["Gnr"])
                print(cells["Lenke_til_ originalside"], "|", gard_ids[gard_id]["Lenke_til_ originalside"])
                print()
                #print(gard_ids[gard_id])
                teller += 1
                # Print values that are different bwteen them
                for key, value in cells.items():
                    if value != gard_ids[gard_id].get(key):
                        # print(f"{key}: {value} != {gard_ids[gard_id].get(key)}")
                        
                        #limit the number of characters printed
                        print(f"{key}: {value[:100]} != {gard_ids[gard_id].get(key)[:100]}")
            
            gard_ids[gard_id] = cells
        # Check if any bruk have gard id that is in gard_ids
        if cells["Lokalitetstype"] == "Bruk":
            gard_id = (cells["Knr"], cells["Gnr"])
            if gard_id in duplicate_gard_ids:
                print("FOUND: ", duplicate_gard_ids[gard_id])
                print("URLs: ", cells["Lenke_til_ originalside"], gard_ids[gard_id]["Lenke_til_ originalside"])
                print(cells["Stadnamn"], "KNR",cells["Knr"], "GNR", cells["Gnr"], "BNR", cells["Bnr"], cells["Enhetsnummer"])
                print()


print(teller)
print("Garder med bnr", len(gard_med_bnr))
print("Garder for gard med bnr", len([x for x in garder_for_gard_med_bnr if x in gard_med_bnr]))





