import csv

with open("lfs-data/processed/RYGH/rygh_toponymi_uuid.csv", "r", encoding="utf-8") as f:
    reader = csv.reader(f, delimiter=';', quotechar='"')
    headings = next(reader)
    print(headings)
    for row in reader:
        assert len(row) == len(headings)

        cells = dict(zip(headings, row))
        if cells["X"] and not cells["koordinattype"]:
            print("\nMISSING")
            print(cells)
            print()







