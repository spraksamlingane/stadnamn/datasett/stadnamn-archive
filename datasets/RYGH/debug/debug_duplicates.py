import json
with open("lfs-data/elastic/rygh_elastic.json", "r", encoding="utf8") as f:
    data = json.load(f)


seen = {}

count = 0
different_coordinates = 0

for item in data["data"]:
    if item["uuid"] in seen:
        
        if seen[item["uuid"]].get("location") and item.get("location") and (seen[item["uuid"]]["location"]["coordinates"][0] != item["location"]["coordinates"][0] or seen[item["uuid"]]["location"]["coordinates"][1] != item["location"]["coordinates"][1]):
            different_coordinates += 1
            
        """else:
            print("Duplicate", "A", item, "\n\nB", seen[item["uuid"]])
            print("\n")"""
        
        different = ""
        for key, value in item.items():
            if type(value) == str and value != seen[item["uuid"]].get(key):
                different += f"{key}: {value} != {seen[item['uuid']].get(key)}\t"
        if different:
            print(item["type"], item["uuid"])
            print(different)
            if item["type"] == 'gard':
                print("Knr", item["rawData"]["knr"], "Gnr", item["rawData"]["gnr"])
            if item["type"] == 'bruk':
                print("Knr", item["rawData"]["knr"], "Gnr", item["rawData"]["gnr"], "Bnr", item["rawData"]["bnr"], "Enhetsnummer", item["rawData"]["enhetsnummer"])

        count += 1
        print()

    seen[item["uuid"]] = item
    
    

print(count)
print(different_coordinates)