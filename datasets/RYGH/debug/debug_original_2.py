import csv
gard_ids = {}
duplicate_gard_ids = {}
teller = 0
gard_med_bnr = {}
har_bruk = set()
with open("lfs-data/raw/RYGH/rygh.csv", "r", encoding="latin-1") as f:
    reader = csv.reader(f, delimiter='\t')
    headings = next(reader)
    for row in reader:
        assert len(row) == 26
        cells = dict(zip(headings, row))
        if cells["Lokalitetstype"] == "Gard":
            if cells["Bnr"]:
                gard_med_bnr[cells["Knr"], cells["Gnr"]] = gard_med_bnr.get((cells["Knr"], cells["Gnr"]), []) + [cells]


            gard_id = (cells["Knr"], cells["Gnr"])
        if cells["Lokalitetstype"] == "Bruk":
            har_bruk.add((cells["Knr"], cells["Gnr"]))



print(len([x for x in gard_med_bnr if x in har_bruk]))






