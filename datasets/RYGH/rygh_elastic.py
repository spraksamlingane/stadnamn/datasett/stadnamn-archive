import csv
import json
from stadnamn.vocab.sosi import *
from stadnamn.vocab.coordinate_types import get_coordinate_type
from stadnamn.elasticsearch import es_upload, save_elastic_json
from stadnamn.wikidata import ADM_MAPPING
import time
import re
from collections import Counter


json_data = []
seen = set()

codes = Counter()


with open("lfs-data/raw/RYGH/oracle/rygh-1717141951747.json", encoding="utf-8") as f:
    oracle_data = json.load(f)["RYGH_ENHETER"]
    oracle_data = {x["ENHETSNR"]: {k: v for k,v in x.items() if k !="ENHETSNR"} for x in oracle_data}

def processPhonetic(match):
    text = match.group(1).replace('£', 'ˉ')
    return "<span class='font-phonetic'>" + text + "</span>"


with open('lfs-data/processed/RYGH/rygh_updated.csv', 'r', encoding="utf8") as f:
    reader = csv.DictReader(f, delimiter=';', quotechar='"')

    for cells in reader:

        data = {
            "uuid": cells["uuid"],
            "label": cells["Stadnamn"],
            "adm1": cells["Amt"],
            "adm2": cells["Herred"],
            "wikiAdm": ADM_MAPPING[cells["KNRu"]]["id"],
            "link": cells["Lenke_til_originalside"],
            "snid": cells["SNID"]
        }
        #if cells["uuid"] != "7464294e-ba0c-3ae0-817c-e50ab7f4f164":
        #    continue

        if cells.get("Gnr"):
            assert cells["Gnr"].isdigit(), "Gnr not digits: " + cells["Gnr"]
            data["cadastre"] = []

            if cells.get("Bnr"):
                if cells["Bnr"].isdigit():
                    data["cadastre"].append({
                        "gnr": int(cells["Gnr"]),
                        "bnr": int(cells["Bnr"])
                    })
                
                else:
                    bnr_range = []
                    if not re.match(r"^\d+-\d+$", cells["Bnr"]):
                        all_bnr = cells["Bnr"].split(" og ")
                    else:
                        start, end = map(int, cells["Bnr"].split("-"))
                        range_list = list(range(start, end + 1))
                        all_bnr = [str(x) for x in range_list]
                    # Assert that all elements in brn_range are digits
                    assert all(map(str.isdigit, all_bnr)), "Bnr not digits: " + cells["Bnr"]

                    #for bnr in all_bnr:
                    data["cadastre"].append({
                        "gnr": int(cells["Gnr"]),
                        "bnr": int(all_bnr[0]) if len(all_bnr) == 1 else [int(x) for x in all_bnr]
                    })
            else:
                data["cadastre"].append({
                    "gnr": int(cells["Gnr"])
                })
                
                


        if cells.get("GNIDu"):
            data["gnidu"] = cells["GNIDu"]
        
        if cells.get("midu"):
            data["midu"] = cells["MIDu"]

        if cells["Lokalitetstype"] == 'Bruk':
            data["sosi"] = SOSI_BRUK
        
        elif cells["Lokalitetstype"] == 'Namnegard':
            data["sosi"] = SOSI_NAMNEGARD

        elif cells["Lokalitetstype"] == 'Gard':
            data["sosi"] = SOSI_GARD

        elif cells["Lokalitetstype"] == 'Sokn':
            data["sosi"] = SOSI_SOKN

        elif cells["Lokalitetstype"] == 'Herad/sokn' or cells["Lokalitetstype"] == 'Herad':
            data["sosi"] = SOSI_KOMMUNE

        elif cells["Lokalitetstype"] in ['Forsv. Lokalitet','Forsv. lokalitet']:
            data["sosi"] = SOSI_GAMMEL
        

        if cells.get("X"):
            assert "koordinattype" in cells, cells
            assert cells["koordinattype"], cells
            data["location"] = {
                "type": "Point",
                "coordinates": [float(cells["X"]), float(cells["Y"])]
            }
        
        # Omit fields added by Henrik
        data["rawData"] = {
            k: v for k, v in cells.items() if k.lower() not in ("unique", "admid", "knru", "fnru")
        }

        # Process text content from oracle database
        if int(cells["Enhetsnummer"]) in oracle_data:

            html = oracle_data[int(cells["Enhetsnummer"])]["TEKST"]
            html = re.sub('-<L>', "", html) # Fix spurious newlines
            html = re.sub('<L>', " ", html)
            html = html.replace("\n", "<br>")
            

            # Fix encoding

            """if "Ã¸" in html or "Ã¥" in html or "Ã¦" in html:
                print("Encoding error in", data["label"])
                html = html.encode('windows-1252').decode('utf-8')
            """

            #print("REPRODUCE", "være".encode('utf-8').decode('cp1252', errors="ignore"))
            #print("REVERSE", "vÃ¦re".encode('cp1252').decode('utf-8', errors="ignore"))


            # Replace custom dokpro encoding
            html = html.replace("&bmaapeno;", "ǫ")
            html = html.replace("&bmAAPENO;", "Ǫ")
            html = html.replace("&bmOE;", "Œ")
            html = html.replace("&bmdvs;", "ↄ:")
            
            

            # Add any other codes to the counter
            for code in re.findall(r'&\w+;', html):
                codes[code] += 1



            # Replace "<UTTALE...</UTTALE>" with a span
            html = re.sub('<UTTALE.*?>(.*?)</UTTALE>', processPhonetic, html)
            html = re.sub('<KUR.*?>(.*?)</KUR>', processPhonetic, html)
            html = re.sub('<AVSN.*?>(.*?)</AVSN>', r'<p>\1</p>', html)
            html = re.sub('<ETY.*?>(.*?)</ETY>', r'<p>\1</p>', html)
            
            # Remove tags that may pollute attestation labels
            
            html = re.sub('<SIDE.*?>', "", html)

            data["attestations"] = data.get("attestations", [])
            # Find attestations (FORM, year nested as the attribute AAR of <LITTREF>
            for match in re.finditer('<FORM.*?>(.*?)</FORM>', html):
                form = match.group(1)
                # Extract the text before the first nested tag
                label_search = re.search('(.*?)<', form)
                if label_search:  # Check if the search found a match
                    label = label_search.group(1).strip()
                else:
                    continue


                littref_search = re.search('<LITTREF.*?AAR="(.*?)"', match.group(0))
                if littref_search:  # Check if the search found a match
                    
                    year = littref_search.group(1)

                    # Remove text in parentheses at the end of the label
                    label = re.sub(r'\(.*?\)$', "", label)

                    # Remove ", Underbrug til...", "med ..."
                    label = label.split(", Underbrug")[0]
                    label = label.split(" med ")[0]

                    
                    
                    if re.match("^[\w\s\-]+$", label):
                        if " og " not in label or label.split(" og ")[0][0] == label.split(" og ")[1][0]:
                            data["attestations"].append({
                                "label": label,
                                "year": year
                            })
                            continue

                    labels = [x for x in re.split(", | og ", label) if x]

                    suffix_pattern="([Ss]øndre|[Øø]stre|[Vv]estre|[Nn]edre|[Øø]f?re|[Mm]ellem|[Ii]ndre|[Yy]ttre)$"
                    # If not any of the labels matches the suffix pattern
                    if not labels or any(re.match(suffix_pattern, x) for x in labels):
                        continue
                    
                    


                    # If all first letters are capitalized and the same letter
                    for sublabel in [x for x in labels if x and x[0].upper() == x[0] and x[0] == labels[0][0]]:
                        if not re.match("^[\w\s\-]+$", sublabel):
                            break
                        data["attestations"].append({
                            "label": sublabel,
                            "year": year
                        })

                    


            if not data["attestations"]:
                del data["attestations"]


                
            # Remove all remaining tags with capital letters
            html = re.sub('</?[A-Z].*?>', "", html)
            data["content"] = {"html" : html}




        json_data.append(data)

print(codes.most_common())

output_json = save_elastic_json("rygh", json_data)
es_upload(output_json, "rygh")
        
