"""

Generic script in /shared/updates/filter_updates.py

This script had to be created because Digitalisert_tekst was clipped in the input csv

"""

import csv
import json
import os

updated_rows = {}

# Open each file and check whether any rows in the update file are different from the original file

original_rows = {}

digitalisert_tekst = {}


with open("lfs-data/processed/RYGH/rygh_toponymi_uuid.csv", "r", encoding="utf-8") as original_file:
    original_reader = csv.reader(original_file, delimiter=';', quotechar='"')
    original_headings = next(original_reader)
    for row in original_reader:
        cells = dict(zip(original_headings, row))
        original_rows[cells["uuid"]] = {k: v for k, v in cells.items() if k != "Digitalisert_tekst"}
        digitalisert_tekst[cells["uuid"]] = cells["Digitalisert_tekst"]


with open("lfs-data/updates/RYGH/20240515_Rygh_complete_uuid_2.txt", "r", encoding="utf-8") as update_file:
        update_reader = csv.reader(update_file, delimiter=';', quotechar='"')
        update_headings = next(update_reader)

        seen_field = set()

        for row in update_reader:
            cells = {k: v for k, v in zip(update_headings, row) if k != "Digitalisert_tekst"}
            assert cells["uuid"] in original_rows, f"uuid {cells['uuid']} not found in original file"

            if cells != original_rows[cells["uuid"]]:
                updated_rows[cells["uuid"]] = {k: v for k, v in zip(update_headings, row) if k != "Digitalisert_tekst" and cells[k] != original_rows[cells["uuid"]][k]}
                



with open("lfs-data/updates/RYGH/20240515_rygh_update.json", "w", encoding="utf-8") as update_file:
    json.dump(updated_rows, update_file, indent=4, ensure_ascii=False)

            

    