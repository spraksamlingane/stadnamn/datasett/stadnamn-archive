# Workflow for å oppdatere RYGH
Kopier rygh_updated.csv og gjør endringer i den.
Kjør et script som kontrollerer at det ikke er gjort uønskede endringer og genererer en json-fil med endringene i lgs-data/updates/RYGH (e. g. slik jeg har gjort med update_1.py)
Kjør update_csv.py for å generere ny rygh_updated.csv basert på originalen fra toponymi og endringene i json-filene.


