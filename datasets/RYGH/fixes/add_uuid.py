import csv
import uuid
seen = {}
nr_duplicates = 0
SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/rygh"
with open("lfs-data/enriched/RYGH/rygh_complete.csv", "r", encoding="latin-1") as f:
    reader = csv.reader(f, delimiter='\t')
    headings = next(reader)
    # Add uuid to each row and save to new file
    with open("lfs-data/processed/RYGH/rygh_toponymi_uuid.csv", "w", encoding="utf-8", newline="") as f2:
        
        
        writer = csv.writer(f2, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerow(headings + ["uuid"])
        for num, row in enumerate(reader):
            cells = dict(zip(headings, row))
            row_uuid = str(uuid.uuid3(uuid.NAMESPACE_URL, SEED_NAMESPACE + "_ROWNUMBER_" + str(num)))
            writer.writerow(row + [row_uuid])

