import csv
from stadnamn.elasticsearch import es_upload, save_elastic_json
import uuid
from stadnamn.vocab.coordinate_types import get_coordinate_type
import json

json_data = []
debug = set()

SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/SKUL/"


with open('lfs-data/processed/SKUL/adm/knr2wikidata.json', 'r', encoding="utf8") as f:
    knr2wikidata = json.load(f)


with open('lfs-data/enriched/SKUL/20240223_Skulebarnsoppskriftene_N_Tr_2.txt', 'r', encoding="latin-1") as f:
    reader = csv.reader(f, delimiter=';', quotechar='"')
    header = [x.lower() for x in next(reader)]
    print(header)

    for row in reader:
        cells = dict(zip(header, row))
        debug.add(cells["arkivportalen_sti"])

        row_uuid = str(uuid.uuid3(uuid.NAMESPACE_URL, SEED_NAMESPACE + "_" + cells["id"]))
        data = {
            "uuid": row_uuid,
            "label": cells["stadnamn"].strip("\""),
            "adm1": "Troms" if cells["knr"][:2] == "19" else "Nordland" if cells["knr"][:2] == "18" else None,
            "adm2": cells["kommune"],
            "wikiAdm": knr2wikidata.get(cells["knr"])["id"],
            "snid": cells["snid"],
            "rawData": cells
        } 

        if cells.get("gnidu"):
            data["gnidu"] = cells["gnidu"]

        if cells.get("midu"):
            data["midu"] = cells["midu"]      

        assert data["adm1"] is not None, print(cells)

        if cells.get("lat"):
            assert cells.get("long"), print(cells)
            if cells["koordinat_type"]:
                if cells.get("koordinat_type") == 'Gardnummercentroide, Språsamlingane':
                    cells["koordinat_type"] = 'Gardnummercentroide, Språksamlingane'

                data["coordinateType"] = get_coordinate_type(cells["koordinat_type"])
            data["location"] = {
                "type": "Point",
                "coordinates": [float(cells["long"]), float(cells["lat"])]
            }
        
        json_data.append(data)


output_json = save_elastic_json("skul", json_data)
es_upload(output_json, "skul", 'local')



    
    
