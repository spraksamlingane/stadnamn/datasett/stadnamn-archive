"""
Prevent duplicate IDs in Elasticsearch by adding UUIDs to each row in the CSV file.
These uuids should be preserved in later changes to the data.
In the conversion to JSON for upload to Elasticsearch, rows where bruksnr is 1 will generate both a bruk and a gard.
"""
import csv
import uuid
seen = {}
nr_duplicates = 0
SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/NBAS"
seen = set()


with open("lfs-data/enriched/NBAS/20221117_Nasjonale_ stedsnavndatabase_MASTER.txt", "r", encoding="latin-1") as f:
    reader = csv.reader(f, delimiter='\t')
    headings = next(reader)
    # Add uuid to each row and save to new file
    with open("lfs-data/processed/NBAS/nbas_uuid.csv", "w", encoding="utf-8", newline="") as f2:
        
        writer = csv.writer(f2, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerow(headings + ["uuid"])
        for num, row in enumerate(reader):
            cells = dict(zip(headings, row))
            row_uuid = str(uuid.uuid3(uuid.NAMESPACE_URL, SEED_NAMESPACE + "20221117_Nasjonale_ stedsnavndatabase_MASTER" + str(num)))
            assert row_uuid not in seen
            seen.add(row_uuid)
            
            writer.writerow(row + [row_uuid])

