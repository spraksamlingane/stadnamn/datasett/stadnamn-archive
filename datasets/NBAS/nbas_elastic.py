import csv
import uuid
import json
from stadnamn.vocab.sosi import *
from stadnamn.vocab.coordinate_types import get_coordinate_type
from stadnamn.elasticsearch import es_upload, save_elastic_json


json_data = []
seen = set()

SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/NBAS/"
with open('datasets/NBAS/iiif/manifest_uuids.txt', 'r') as f:
    manifest_uuids = set([x.strip() for x in f.readlines()])

with open("lfs-data/processed/NBAS/adm/adm2wikidata.json", "r", encoding='utf8') as f:
    adm2wikidata = json.load(f)



def get_manifest_uuid(place_name_id):
    assert type(place_name_id) == int
    seed = "http://data.toponym.ub.uib.no/nsnb/manifest/" + str(place_name_id)
    uid = str(uuid.uuid3(uuid.NAMESPACE_URL, seed))
    return uid, seed


coordinate_data = {}
with open('lfs-data/enriched/NBAS/nbas_koordinater.txt', 'r') as f:
    reader = csv.reader(f, delimiter='\t')
    header = [x.lower() for x in next(reader)]
    print(header)

    for row in reader:
        cells = dict(zip(header, row))
        coordinate_data[cells["oppslag_id"]] = cells
        

with open('lfs-data/processed/NBAS/nbas_uuid.csv', 'r', encoding='utf-8') as f:
    reader = csv.reader(f, delimiter=';', quotechar='"') # TODO: use newer data
    header = [x.lower() for x in next(reader)]
    print(header)
    teller = 0
    for row in reader:
        cells = dict(zip(header, row))

        teller += 1
        if teller > 900:
            pass
        

        if not "uuid" in cells:
            print("WARNING", "uuid not found,", cells) # Investigate why some values are missing
            continue

         # Fix errors in enriched data
        if not cells.get("fylke") and cells.get("oppslagsform") == "Nestevatn":
            cells["fylke"] = "Oppland"
            cells["herred"] = "Vestre Slidre"
        if cells["herred"] in {"Bygland", "Åmli", "Mykland"}:
            cells["fylke"] = "Aust-Agder"
        if cells["herred"] == "Hole":
            cells["fylke"] = "Buskerud"
        if cells["herred"] == "Eneebakk":
            cells["herred"] = "Enebakk"
        
        data = {
            "uuid": cells["uuid"],
            "label": cells["oppslagsform"].strip("\""),
            "adm1": cells["fylke"],
            "adm2": cells["herred"],
            "wikiAdm": adm2wikidata[cells["fylke"]][cells["herred"]],
            "rawData": cells
        }

        # Omit SNIDs created in nbas, as they are likely to be duplicates
        if "NSDb" not in cells["snid"]:
            data["snid"] = cells["snid"]
        else:
            assert cells.get("oppslag_id") not in coordinate_data, cells

        if cells.get("gnidu"):
            data["gnidu"] = cells["gnidu"]
        

        if cells.get("midu"):
            data["midu"] = cells["midu"]

        if cells.get("lokalitetstype_sosi"):
            assert cells["lokalitetstype_sosi"] in SOSI_CODES, "Unknown lokalitetstype: " + cells["lokalitetstype_sosi"]
            data["sosi"] = cells["lokalitetstype_sosi"]



        if cells.get("stadnamn_id"):
            manifest_uuid, seed = get_manifest_uuid(int(cells["stadnamn_id"]))
            if manifest_uuid in manifest_uuids:
                data["image"] = {
                    "manifest": manifest_uuid,
                }


        if cells.get("oppslag_id") and coordinate_data.get(cells["oppslag_id"]) and coordinate_data[cells["oppslag_id"]]["x"]:
            data["coordinateType"] = get_coordinate_type(coordinate_data[cells["oppslag_id"]]["koordinattype"])
            data["location"] = {
                "type": "Point",
                "coordinates": [float(coordinate_data[cells["oppslag_id"]]["x"]), float(coordinate_data[cells["oppslag_id"]]["y"])]
            }
        
        json_data.append(data)

output_json = save_elastic_json("nbas", json_data, dev=False)
es_upload(output_json, "nbas", dynamic=False)