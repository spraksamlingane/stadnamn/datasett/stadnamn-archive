SELECT ROWNUM "SEARCH_RAD#",
       T1.BINDELEDD "Bindeledd", 
       T1.BUNDEN_ARTIKKEL "Bunden_Artikkel", 
       T1.DATIVFORM "Dativform", 
       T1.HOVUDLEDD "Hovudledd",
       T1.MERKNAD "Merknad", 
       T1.NORMERT "Normert", 
       T1.NORMERT_FORM "Normert_Form", 
       T1.OPPSLAG_ID "Oppslag_Id", 
       T1.OPPSLAGSFORM "Oppslagsform", 
       T1.PREPOSISJON "Preposisjon", 
       T1.SPRAAK "Spraak", 
       T1.STADNAMN_ID "Stadnamn_Id", 
       T1.STANDARDFORM "Standardledd", 
       T1.UTMERKINGSLEDD "Utmerkingsledd", 
       T1.UTTALE "Uttale", 
       T1.OPPSLAG_ID "#PRIMARYKEY#"
FROM USD_NAMN_KORTARKIV_OSLO.V_OPPSLAG T1
WHERE rownum < 250000;

