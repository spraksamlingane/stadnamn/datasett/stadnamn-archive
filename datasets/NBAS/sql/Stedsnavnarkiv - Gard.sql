SELECT ROWNUM "SEARCH_RAD#",
       T1.GARD_ID "Gard_Id", 
       T1.GARDNAMN "Gardnamn", 
       T1.GARDNUMMER "Gardnummer", 
       T1.LOKALISERING_ID "Lokalisering_Id", 
       T1.GARD_ID "#PRIMARYKEY#"
FROM USD_NAMN_KORTARKIV_OSLO.V_GARD T1
WHERE rownum < 250000;
