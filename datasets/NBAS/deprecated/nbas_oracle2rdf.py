import json
import glob
from rdflib import Graph, Namespace, RDF, RDFS, Literal, OWL
import urllib.parse

with open('lfs-data/raw/NBAS/DBA_TAB_COLS_202305151336.json', encoding="utf-8") as inn:
    cols = json.load(inn).popitem()[1]
    
with open('lfs-data/raw/NBAS/CONSTRAINT_QUERY_202305151434.json', encoding="utf-8") as inn:
    constraints = json.load(inn).popitem()[1]


constraint_dict = {x["CONSTRAINT_NAME"]: x for x in constraints}


# Skip unconnected tables
# Initially I planned to include the map table and the binary relation that connects it to stadnamn.
# Due to errors in the data I'll instead add the map data as a refined dataset.
connected_tables = set()
traversed = set()
OWNER = "USD_NAMN_KORTARKIV_OSLO"
def traverse(table_name):
    for x in constraint_dict.values():
        if x["R_CONSTRAINT_NAME"]:
            if x["OWNER"] != OWNER:
                continue
            if x["TABLE_NAME"] == table_name and constraint_dict[x["R_CONSTRAINT_NAME"]]["TABLE_NAME"]:
                connected_tables.add(constraint_dict[x["R_CONSTRAINT_NAME"]]["TABLE_NAME"])
            elif constraint_dict[x["R_CONSTRAINT_NAME"]]["TABLE_NAME"] == table_name:
                connected_tables.add(x["TABLE_NAME"])
    traversed.add(table_name)
    for x in list(connected_tables):
        if x not in traversed:
            traverse(x)


traverse("STADNAMN")


primary_keys = {}
foreign_keys = {}
for c in constraints:
    if c["OWNER"] != OWNER:
        continue
    if c["CONSTRAINT_TYPE"] == "P":
        primary_keys[c["TABLE_NAME"]] = primary_keys.get(c["TABLE_NAME"], []) + [c["COLUMN_NAME"]]
    if c["CONSTRAINT_TYPE"] == "R": # Referential integrity = foreign key
        other = constraint_dict[c["R_CONSTRAINT_NAME"]]
        foreign_keys[(c["TABLE_NAME"], c["COLUMN_NAME"])] = (other["TABLE_NAME"], other["COLUMN_NAME"])


classes = {}
props = {}

BASE = Namespace("https://lod.uib.no/id/stadnamnarkiv/nbas/")
ONT = Namespace("https://lod.uib.no/ont/nbas/")

teller = 0


SKIP_TABLES = {"STATUS", "STATUS_HISTORIE", "INNSKRIVAR", "LYD", "HERRED", "FYLKER", "BILETETYPAR", "ARKIV", "KARTREF_TYPAR", "KOPLING_STADNAMN_KART", "KARTFESTING"}
# STATUS, STATUS_HISTORIE and INNSKRIVAR skipped due to limited capacity in stardog essentials plan
# LYD skipped because it doesn't contain any exact references to the audio files, only "lydbandnr"
# Skip FYLKE because label is ID and no other fields present
# Skip Herred because label is ID and other fields are redundant and mostly null
# Skip FYLKER, BILETETYPAR, KART_ARKIV, ARKIV and KART_TYPE because they have only one column
# Map references removed because they are in need of data cleaning.

ontology = Graph()

for table in connected_tables:
    if table in SKIP_TABLES:
        #print("SKIPPING", table)
        continue
    if teller > 4:
        pass
    
    teller += 1
    #if teller < 7:
    #   continue

    graph = Graph()
    graph.bind('nbas', BASE)

    for f in glob.glob("lfs-data/raw/NBAS/" + OWNER + "/" + table + "_[0-9]*.json"):
        #print(owner, f)
        
        print("FILE", f)
        with open(f, encoding='utf-8') as infile:
            
            table, data = json.load(infile).popitem()
            class_uid = table.capitalize()
            graph.bind(table.lower(), BASE + class_uid + "#")


            #print(table_id)
            primary_key_cols = primary_keys.get(table)
            foreign_key_cols = {key[1:]: value for key, value in foreign_keys.items() if key[0] == table and value[0] not in SKIP_TABLES}
            print(foreign_key_cols)

            if data is None:
                continue
            if len(data) == 0:
                continue
            if primary_key_cols is None:
                #print("No primary key:", table)
                continue

            assert len(data[0]) != 1, table
            

            # Binary ralations
            if len(data[0]) == len(primary_key_cols) == 2:
                
                # The column the largest number of unique values will be the domain
                domain_col, range_col = sorted(data[0].keys(), key=lambda x: len({y[x] for y in data}), reverse=True)
                #print("DOMAIN RANGE", domain_col, range_col)

                subj_table, subj_col =  foreign_key_cols[(domain_col,)]
                obj_table, obj_col =  foreign_key_cols[(range_col,)]

                subj_uuid_base =  subj_table.capitalize() + "/"
                obj_uuid_base = subj_table.capitalize() + "/"
                pred = ONT[table.lower()]
                
                for row in data[:10]:
                    subj = BASE[subj_uuid_base + subj_col.lower() + "=" + urllib.parse.quote(str(row[domain_col]))]
                    obj = BASE[obj_uuid_base  + obj_col.lower() + "=" + urllib.parse.quote(str(row[range_col]))]
                    graph.add((subj, pred, obj))
                    #print("ROW", row)
                continue
            
            for row in data[:10]:
                primary_key_vals = [row.get(key) for key in primary_key_cols]
                uid = ";".join(f"{key.lower()}={urllib.parse.quote(str(val))}" for key, val in zip(primary_key_cols, primary_key_vals))
                uri = BASE[class_uid + "/" + uid]
                props = {}
                fk_classes = {}

                for fk, reference in foreign_key_cols.items():
                    fk_vals = [row.get(key) for key in fk]
                    #print("ref", reference)
                    if None in fk_vals:
                        continue

                    fk_classes[fk] = fk_classes.get(fk,  reference[0].capitalize())
                    props[fk] = props.get(fk, class_uid + "#ref-" + ";".join([x.lower() for x in fk]))
                    fk_uid = fk_classes[fk] + "/" + ";".join(f"{key.lower()}={urllib.parse.quote(str(val))}" for key, val in zip(reference[1:], fk_vals))

                    ontology.add((ONT[props[fk]], RDF.type, OWL.ObjectProperty))
                    ontology.add((ONT[props[fk]], RDFS.domain, ONT[class_uid]))
                    ontology.add((ONT[props[fk]], RDFS.range, ONT[fk_classes[fk]]))
                    ontology.add((ONT[fk_classes[fk]], RDF.type, OWL.Class))
                    graph.add((uri, ONT[props[fk]], BASE[fk_uid]))

                for key, value in row.items():
                    if not value:
                        continue

                    if (key,) not in primary_key_cols and (key,) not in foreign_key_cols:                        
                        ontology.add((ONT[class_uid], RDF.type, OWL.Class))
                        props[key] = props.get(key, class_uid + "#" + key.lower())
                        graph.add((uri, ONT[props[key]], Literal(value) ))
                        ontology.add((ONT[props[key]], RDF.type, OWL.DatatypeProperty))



    if graph:
        graph.serialize(format="ttl", destination=f"lfs-data/rdf/NBAS/.tmp/{OWNER}-{table}.ttl")

    #with open(owner + "/" + table + ".json")

ontology.serialize(format="ttl", destination=f"lfs-data/rdf/NBAS/.tmp/nbas_ontology.ttl")

# TODO: legg til registreringsdato fra status