import json
import glob
from rdflib import Graph, Namespace, RDF, RDFS, Literal, OWL, XSD
import urllib.parse

with open('DBA_TAB_COLS_202305151336.json', encoding="utf-8") as inn:
    cols = json.load(inn).popitem()[1]
    
with open('CONSTRAINT_QUERY_202305151434.json', encoding="utf-8") as inn:
    constraints = json.load(inn).popitem()[1]

constraint_dict = {x["CONSTRAINT_NAME"]: x for x in constraints}


# Ignore unconnected tables
connected_tables = set()
traversed = set()
def traverse(owner, table_name):
    for x in constraint_dict.values():
        if x["R_CONSTRAINT_NAME"]:
            if x["TABLE_NAME"] == table_name and constraint_dict[x["R_CONSTRAINT_NAME"]]["TABLE_NAME"]:
                connected_tables.add((x["OWNER"], constraint_dict[x["R_CONSTRAINT_NAME"]]["TABLE_NAME"]))
            elif constraint_dict[x["R_CONSTRAINT_NAME"]]["TABLE_NAME"] == table_name:
                connected_tables.add((x["OWNER"], x["TABLE_NAME"]))
    traversed.add((owner, table_name))
    for x in list(connected_tables):
        if x not in traversed:
            traverse(x[0], x[1])



traverse("USD_NAMN_KORTARKIV_OSLO", "STADNAMN")


primary_keys = {}
foreign_keys = {}
for c in constraints:
    if c["CONSTRAINT_TYPE"] == "P":
        primary_keys[(c["OWNER"], c["TABLE_NAME"])] = primary_keys.get((c["OWNER"], c["TABLE_NAME"]), []) + [c["COLUMN_NAME"]]
    if c["CONSTRAINT_TYPE"] == "R": # Referential integrity = foreign key
        other = constraint_dict[c["R_CONSTRAINT_NAME"]]
        foreign_keys[(c["OWNER"], c["TABLE_NAME"], c["COLUMN_NAME"])] = (other["OWNER"], other["TABLE_NAME"], other["COLUMN_NAME"])



classes = {}
props = {}

BASE = Namespace("https://data.spraksamlingane.no/stadnamn/archive/nbas/")


SKIP_TABLES = {"STATUS", "STATUS_HISTORIE", "INNSKRIVAR", "LYD", "HERRED", "FYLKER", "BILETETYPAR", "KART_ARKIV", "ARKIV", "KART_TYPE", "KARTREF_TYPAR"}
# STATUS, STATUS_HISTORIE and INNSKRIVAR skipped due to limited capacity in stardog essentials plan
# LYD skipped because it doesn't contain any exact references to the audio files, only "lydbandnr"
# Skip FYLKE because label is ID and no other fields present
# Skip Herred because label is ID and other fields are redundant and mostly null
# Skip FYLKER, BILETETYPAR, KART_ARKIV, ARKIV and KART_TYPE because they have only one column
# Skip KARTREF_TYPAR because the second column is empty
# Skip Kart because these metadata will be kept as IIIF - reduce duplication of data

for owner, table in connected_tables:
    if table in SKIP_TABLES:
        #print("SKIPPING", table)
        continue


    graph = Graph()
    graph.bind('nbas', BASE)

    for f in glob.glob(owner + "/" + table + "_[0-9]*.json"):
        #print(owner, f)
        

        with open(f) as infile:
            print(f)
            table, data = json.load(infile).popitem()
            class_uid = table.capitalize()
            graph.bind(table.lower(), BASE + class_uid + "#")

            table_id = (owner, table)
            #print(table_id)
            primary_key_cols = primary_keys.get(table_id)
            foreign_key_cols = {key[2:]: value for key, value in foreign_keys.items() if tuple(key[:2]) == table_id and value[1] not in SKIP_TABLES}

            if data is None:
                continue
            if len(data) == 0:
                continue
            if primary_key_cols is None:
                #print("No primary key:", table)
                continue

            assert len(data[0]) != 1, table
            

            # Binary ralations
            if len(data[0]) == len(primary_key_cols) == 2:
                
                # The column the largest number of unique values will be the domain
                domain_col, range_col = sorted(data[0].keys(), key=lambda x: len({y[x] for y in data}), reverse=True)

                subj_table, subj_col =  foreign_key_cols[(domain_col,)][1:]
                obj_table, obj_col =  foreign_key_cols[(range_col,)][1:]

                subj_uuid_base =  subj_table.capitalize() + "/"
                obj_uuid_base = subj_table.capitalize() + "/"
                pred = BASE[table.lower()]
                
                for row in data: #[:5]:
                    subj = BASE[subj_uuid_base + subj_col.lower() + "=" + urllib.parse.quote(str(row[domain_col]))]
                    obj = BASE[obj_uuid_base  + obj_col.lower() + "=" + urllib.parse.quote(str(row[range_col]))]
                    graph.add((subj, pred, obj))
                    #print("ROW", row)
                continue
            
            for row in data: #[:10]:
                primary_key_vals = [row.get(key) for key in primary_key_cols]
                uid = ";".join(f"{key.lower()}={urllib.parse.quote(str(val))}" for key, val in zip(primary_key_cols, primary_key_vals))
                uri = BASE[class_uid + "/" + uid]
                props = {}
                fk_classes = {}

                for fk, reference in foreign_key_cols.items():
                    fk_vals = [row.get(key) for key in fk]
                    if None in fk_vals:
                        continue

                    fk_classes[fk] = fk_classes.get(fk,  reference[1].capitalize())
                    props[fk] = props.get(fk, class_uid + "#ref-" + ";".join([x.lower() for x in fk]))
                    fk_uid = fk_classes[fk] + "#" + ";".join(f"{key.lower()}={urllib.parse.quote(str(val))}" for key, val in zip(reference[2:], fk_vals))

                    graph.add((BASE[props[fk]], RDF.type, OWL.ObjectProperty))
                    graph.add((BASE[props[fk]], RDFS.domain, BASE[class_uid]))
                    graph.add((BASE[props[fk]], RDFS.range, BASE[fk_classes[fk]]))
                    graph.add((BASE[fk_classes[fk]], RDF.type, OWL.Class))
                    graph.add((uri, BASE[props[fk]], BASE[fk_uid]))

                for key, value in row.items():
                    if not value:
                        continue

                    if (key,) not in primary_key_cols and (key,) not in foreign_key_cols:                        
                        graph.add((BASE[class_uid], RDF.type, OWL.Class))
                        props[key] = props.get(key, class_uid + "#" + key.lower())
                        graph.add((uri, BASE[props[key]], Literal(value) ))
                        graph.add((BASE[props[key]], RDF.type, OWL.DatatypeProperty))



    if graph:
        graph.serialize(format = "ttl", destination = f"lfs-data/rdf/nbas-{owner}-{table}.ttl")

    #with open(owner + "/" + table + ".json")

# TODO: legg til registreringsdato fra status