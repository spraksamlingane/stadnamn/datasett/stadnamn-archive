import csv
from stadnamn.vocab.sosi import *
from stadnamn.vocab.coordinate_types import get_coordinate_type
from stadnamn.elasticsearch import es_upload, save_elastic_json
from stadnamn.wikidata import ADM_MAPPING

import sys 

json_data = []
seen = set()

# Snid index is replaced by validation of dataset updates
#snid_index_file = open('lfs-data/snid/mu1950_snid.csv', 'w')
#snid_index = csv.writer(snid_index_file)

"""missing_coord = open("mu1950_fix_coordinates.csv", "w", encoding="utf8", newline="")
missing_coord_writer = csv.writer(missing_coord, delimiter=",", quotechar='"', quoting=csv.QUOTE_ALL)
missing_coord_writer.writerow(["uuid", "label", "knr", "gnidu", "midu", "x", "y"])"""



with open('lfs-data/processed/MU1950/mu1950_updated.csv', 'r', encoding="utf8") as f:
    reader = csv.reader(f, delimiter=';' , quotechar='"')
    header = next(reader)
    print(header)
    seen_gard = set()
    seen_bruk= set()

    for row in reader:
        cells = dict(zip(header, row))
        
        # We can safely skip dupliate gards because the only differing fields are related to bruk or coordinates
        if cells["BNR"] == "1" and cells["gard_uuid"] not in seen_gard:
            seen_gard.add(cells["gard_uuid"])
            gard_data = {
                "uuid": cells["gard_uuid"],
                "label": cells["Gardsnamn"],
                "snid": cells["Gardsnamn_SNID"],
                "adm1": cells["Fylke"],
                "adm2": cells["Kommune"],
                "wikiAdm": ADM_MAPPING[cells["KNRu"]]["id"],
                "sosi": SOSI_GARD,
                "cadastre": [{"gnr": int(cells["GNR"])}],
                "knr": cells["KNR"],
            }
            if cells.get("GNIDu"):
                gard_data["gnidu"] = cells["GNIDu"]

            if cells.get("X"):
                gard_data["coordinateType"] = get_coordinate_type(cells["Koordinattype"])
                gard_data["location"] = {
                                    "type": "Point",
                                    "coordinates": [float(cells["X"]), float(cells["Y"])]
                }
                
            json_data.append(gard_data)
            #snid_index.writerow([cells["gardsnamn_snid"], cells["gard_uuid"]])

        bruk_data = {
            "uuid": cells["bruk_uuid"],
            "label": cells["Bruksnamn"],
            "adm1": cells["Fylke"],
            "adm2": cells["Kommune"],
            "wikiAdm": ADM_MAPPING[cells["KNRu"]]["id"],
            "year": cells["År"],
            "sosi": SOSI_BRUK,
            "within": cells["gard_uuid"],
            "snid": cells["Bruksnamn_SNID"],
            "rawData": cells,
            "cadastre": [{"gnr": int(cells["GNR"]), "bnr": int(cells["BNR"])}]
        }

        if cells.get("midu"):
            bruk_data["midu"] = cells["MIDu"]

        if cells.get("gnidu"):
            bruk_data["gnidu"] = cells["gnidu"]

        """if (cells["x"] or cells["y"]) and float(cells["y"]) < 56:
            missing_coord_writer.writerow([bruk_data["uuid"], bruk_data["label"], cells["knr"], cells["gnidu"], cells["midu"], cells["x"], cells["y"]])"""
        
        if cells.get("X"):
            bruk_data["coordinateType"] = get_coordinate_type(cells["Koordinattype"])
            bruk_data["location"] = {
                "type": "Point",
                "coordinates": [float(cells["X"]), float(cells["Y"])]
            }
        
        assert cells["bruk_uuid"] not in seen_bruk, "Duplicate bruk_uuid: " + cells["bruk_uuid"]
        seen_bruk.add(cells["bruk_uuid"])
        json_data.append(bruk_data)
        #snid_index.writerow([cells["bruksnamn_snid"], cells["bruk_uuid"]])

#snid_index_file.close()

output_json = save_elastic_json("mu1950", json_data)
es_upload(output_json, "mu1950")


        


        


