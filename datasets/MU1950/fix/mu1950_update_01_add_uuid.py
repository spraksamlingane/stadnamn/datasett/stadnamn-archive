"""
Prevent duplicate IDs in Elasticsearch by adding UUIDs to each row in the CSV file.
These uuids should be preserved in later changes to the data.
In the conversion to JSON for upload to Elasticsearch, rows where bruksnr is 1 will generate both a bruk and a gard.
"""
import csv
import uuid
seen = {}
nr_duplicates = 0
SEED_NAMESPACE = "http://data.toponym.ub.uib.no/graph/MU1950"
seen = set()
def get_uuid(columns, values):
    # Cancatenate if key is a tuple
    if isinstance(columns, tuple):
        columns = "_".join(columns)
        values = "_".join(values)
    seed = f"{SEED_NAMESPACE}_{columns}_{values}"

    return str(uuid.uuid3(uuid.NAMESPACE_URL, seed))

with open("lfs-data/processed/MU1950/mu1950_complete.csv", "r", encoding="latin-1") as f:
    reader = csv.reader(f, delimiter='\t')
    headings = next(reader)
    # Add uuid to each row and save to new file
    with open("lfs-data/processed/MU1950/mu1950_norsampo_uuid.csv", "w", encoding="utf-8", newline="") as f2:
        
        writer = csv.writer(f2, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
        writer.writerow(headings + ["gard_uuid", "bruk_uuid"])
        for num, row in enumerate(reader):
            cells = dict(zip(headings, row))
            gard_uuid = get_uuid(("KNR", "GNR", "SNID", "Gardsnamn", "GNIDu"), (cells["KNR"], cells["GNR"], cells["Gardsnamn_SNID"], cells["Gardsnamn"], cells["GNIDu"]))
            bruk_uuid = get_uuid("row_mu1950_complete", str(num))
            assert bruk_uuid not in seen
            seen.add(bruk_uuid)
            
            writer.writerow(row + [gard_uuid, bruk_uuid])

