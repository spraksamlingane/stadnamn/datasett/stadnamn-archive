import csv
import json

# Check that the UUIDs in the update file are present in the original file
updated_rows = {}
original_rows = {}


with open("lfs-data/processed/MU1950/mu1950_norsampo_uuid.csv", "r", encoding="latin-1") as original_file:
    original_reader = csv.reader(original_file, delimiter=';', quotechar='"')
    original_headings = next(original_reader)
    for row in original_reader:
        cells = dict(zip(original_headings, row))
        original_rows[cells["bruk_uuid"]] = cells


with open("lfs-data/updates/MU1950/20280528_MU1950_coordinates_fixed.txt", "r", encoding="utf-8") as update_file:
        update_reader = csv.reader(update_file, delimiter=';', quotechar='"')
        update_headings = next(update_reader)

        seen_field = set()

        for row in update_reader:
            cells = dict(zip(update_headings, row))
            assert cells["uuid"] in original_rows, f"uuid {cells['uuid']} not found in original file"

            updated_rows[cells["uuid"]] = {
                 "X": cells["x"],
                "Y": cells["y"],
                "Koordinattype": cells["Koordinattype"]
            }


with open("lfs-data/updates/MU1950/20240528_mu1950_update.json", "w", encoding="utf-8") as update_file:
    json.dump(updated_rows, update_file, indent=4, ensure_ascii=False)

            