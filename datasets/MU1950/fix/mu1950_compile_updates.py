import os
import json
import csv

updates = {}
# Open all json files in lfs-data/updates/MU1950/ and add to updates
for f in sorted(os.listdir('lfs-data/updates/MU1950/')):
    if f.endswith('.json'):
        with open(f'lfs-data/updates/MU1950/{f}', 'r', encoding='utf8') as update_file:
            updates.update(json.load(update_file))



with open("lfs-data/processed/MU1950/mu1950_updated.csv", "w", encoding="utf-8", newline="") as updated_file:
    updated_file_writer = csv.writer(updated_file, delimiter=';', quotechar='"', quoting=csv.QUOTE_ALL)
    with open("lfs-data/processed/MU1950/mu1950_norsampo_uuid.csv", "r", encoding="utf-8") as original_file:
        
        original_reader = csv.reader(original_file, delimiter=';', quotechar='"')
        original_headings = next(original_reader)

        updated_file_writer.writerow(original_headings)

        for row in original_reader:
            cells = dict(zip(original_headings, row))
            if cells["bruk_uuid"] in updates:
                cells.update(updates[cells["bruk_uuid"]])
            updated_file_writer.writerow([cells[h] for h in original_headings])
            
            