import csv
import uuid
import re
import csv
import uuid
from stadnamn.elasticsearch import es_upload, save_elastic_json

json_data = []

with open("lfs-data/enriched/TOT/20240718_Stedsnavn_fra_Toten.txt" , "r", encoding="utf-8") as f:
    reader = csv.DictReader(f, delimiter=";")
    for row in reader:
        assert int(row["ID"])
        
        instance_data = {
            "uuid": str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/TOT/" + row["ID"])),
            "label": row["Oppslagsform"],
            "adm1": row["Fylke"],
            "adm2": row["Herad"],
            "adm3": row["Sokn"],

            "rawData": row,
            
        }
        if row["Herad"] == "Østre Toten": instance_data["wikiAdm"] = "Q308420"
        if row["Herad"] == "Vestre Toten": instance_data["wikiAdm"] = "Q48717"
        
        kjeldeform = row.get("Kjeldeform")
        if kjeldeform:
            kjelde = row.get("Kjelde")
            if kjelde:
                instance_data["attestations"] = []
                # Find all years in the string
                years = re.findall(r"\d{4}", kjelde)
                for year in years:
                    instance_data["attestations"].append({"year": year, "label": kjeldeform})
            elif kjeldeform != row["Oppslagsform"]:
                instance_data["altLabels"] = [kjeldeform]
            
        bostedets_navn = row.get("bostedets_navn")
        if bostedets_navn and bostedets_navn.lower() != row["Oppslagsform"].lower():
            instance_data["altLabels"] = instance_data.get("altLabels", []) + [bostedets_navn]
                    


        if row.get("GNR"):
            cadastre = []
            gnr = row.get("GNR")
            bnr = row.get("BNR")

            # Fix errors
            if bnr == "1+87,3":
                gnr = "85+87"
                bnr = "1,3"


            if bnr:
                if "-" in bnr:
                    bruka = bnr.split("-")
                    assert int(bruka[0]) < int(bruka[1])
                    cadastre.append({"gnr": int(row.get("GNR")), "bnr": [n for n in range(int(bruka[0]), int(bruka[1]) + 1)]})

                        
                else: 
                    bruka = re.split(r"\+|\sog\s|,", row["BNR"])
                    cadastre.append({"gnr": int(row.get("GNR")), "bnr": [int(n) for n in bruka] if len(bruka) > 1 else int(bruka[0])})
                        
            else:
                if "-" in gnr:
                    garder = gnr.split("-")
                    assert int(garder[0]) < int(garder[1])
                    cadastre.append({"gnr": [n for n in range(int(garder[0]), int(garder[1]) + 1)]})
                else:
                    garder = re.split(r"\+|\sog\s|,", row["GNR"])
                    cadastre.append({"gnr": [int(n) for n in garder] if len(garder) > 1 else int(garder[0])})

            instance_data["cadastre"] = cadastre


        if row.get("X"):
            instance_data["location"] = {
                "type": "Point",
                "coordinates": [float(row["X"]), float(row["Y"])]
            }

        assert row.get("Oppslagsform"), row

        json_data.append(instance_data)




output_json = save_elastic_json("tot", json_data)
es_upload(output_json, "tot", "local")
