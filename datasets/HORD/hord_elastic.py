import json
from stadnamn.elasticsearch import es_upload, save_elastic_json
import re
import csv
"""
Second round of refining HORD data
1. Extract label that isn't phonetic and doesn't contain special characters
2. Extract altLabels that aren't phonetic and doesn't contain special characters
3. Add enriched data - SNID, GNIDu etc

"""

with open(".tmp/hord_data.json", "r", encoding="utf-8") as f:
    json_data = json.load(f)

with open("lfs-data/processed/HORD/adm/adm2wikidata.json", "r", encoding='utf8') as f:
    adm2wikidata = json.load(f)

snid_mapping = {}
with open("lfs-data/enriched/HORD/20221118_Hordanamn_MASTER.txt", "r", encoding="windows-1252", newline="") as f:
    reader = csv.DictReader(f, delimiter="\t", quoting=csv.QUOTE_NONE)
    for row in reader:
        assert row["objektNr"], row
        assert (row["objektNr"], row["kommuneNr"]) not in snid_mapping or snid_mapping[(row["objektNr"], row["kommuneNr"])]["SNID"] == row["SNID"], f"FIRST: {snid_mapping[(row['objektNr'], row['kommuneNr'])]} \n\nSECOND: {row}\n\n\n\n"
        assert (row["objektNr"], row["kommuneNr"]) not in snid_mapping or snid_mapping[(row["objektNr"], row["kommuneNr"])]["MIDu"] == row["MIDu"], f"FIRST: {snid_mapping[(row['objektNr'], row['kommuneNr'])]} \n\nSECOND: {row}\n\n\n\n"
        snid_mapping[(row["objektNr"], row["kommuneNr"])] = row
    


antall = 0
nolabel = []
not_enriched = []
found = []

word = "[\"\w\-\s\.&]+"



def strict_find_label(candidates):
    for candidate in candidates:
        candidate = candidate.strip()
        candidate = candidate.replace(" UPL", "")
        if re.match("^[A-ZÆØÅ][a-zæøåA-ZÆØÅ\s\-]*$", candidate):
            return candidate

def find_label(candidates, alt):
    for candidate in candidates:
        candidate = candidate.strip()
        



        # Example: Nordre Toppe [Nordra ..]
        if " – " not in candidate and (candidate[-1] == "-" or candidate[-2:] == ".."):                  
            continue

        candidate = candidate.strip("?").strip()
        candidate = candidate.strip("!").strip()
        if not candidate: continue

        if re.match(f"^{word}$", candidate):
            return candidate

        # Starts with "Naustet til osv."
        if re.match(f"^{word}\s(til|sitt|sin)\s{word}$", candidate):
            return candidate
        
        # Extract two options if parenthesis inside word: 
        # Slett(e)stølen => [Slettestølen, Slettstølen]
        match = re.match(f"^{word}\w(\(\w+\))\w{word}$", candidate)
        if match:
            alt.add(candidate.replace("(", "").replace(")", ""))
            return candidate.replace(match.group(1), "")

        match = re.match("^(.*)\s*[\[\(](.*)[\]\)]$", candidate)
        if match:
            for m in match.groups():
                nested = find_label([m], alt)
                if nested:
                    return nested

        
        # Second name after, dash, slash or comma
        match = re.match("^(.*)\s*[–/,]\s*(.*)$", candidate)
        if match:
            for m in match.groups():
                nested = find_label([m], alt)
                if nested:
                    return nested
                
        # Nested parentheses
        match = re.match(f"^({word})\s[\[\(].*\(.*\).*[\]\)]$", candidate)
        if match:
            return match.group(1)
                
"""
Main loop

"""


for entry in json_data:
    # If label contains any characters other than letters or whitespace
    row_id = (entry["rawData"]["objektNr"], entry["rawData"]["kommuneNr"])
    if row_id not in snid_mapping:
        not_enriched.append(row_id)
    else:
        entry["snid"] = snid_mapping[row_id]["SNID"]
        entry["gnidu"] = snid_mapping[row_id]["GNIDu"]
        entry["midu"] = snid_mapping[row_id]["MIDu"]


    # Remove bnr if zero
    for cadastre in entry.get("cadastre", []):
        if cadastre.get("bnr") == 0:
            del cadastre["bnr"]
    # Remove cadastre if gnr is zero
    entry["cadastre"] = [x for x in entry["cadastre"] if x.get("gnr") != 0]
    if not entry["cadastre"]:
        del entry["cadastre"]

    entry["wikiAdm"] = adm2wikidata[entry["adm1"]][entry["adm2"]]

    
    potential_labels = []
    # Not including the field alternativtNamn because it's corrupted
    if entry["rawData"].get("normertForm"):
        potential_labels.append(entry["rawData"]["normertForm"])
    if entry["rawData"].get("namn"):
        potential_labels.append(entry["rawData"]["namn"])
    if entry["rawData"].get("uttale"):
        potential_labels.append(entry["rawData"]["uttale"])
    if entry["rawData"].get("alternativUttale"):
        potential_labels.append(entry["rawData"]["alternativUttale"])
    

    # Labes should contain at least one letter
    potential_labels = [x for x in potential_labels if re.search("\w", x)]

    # Remove preceding number and whitespace
    potential_labels = [re.sub("^\d+\s", "", x) for x in potential_labels]

    potential_labels = [x for x in potential_labels if x]
    
    if potential_labels:
        alt = set()
        found_label = find_label(potential_labels, alt)
        found_label = strict_find_label(potential_labels) or found_label

        if not found_label:
            antall += 1
            nolabel.append(" | ".join(potential_labels))
            found_label = potential_labels[0]
        

        alt |= {x.replace("\"", "") for x in potential_labels if re.match(f"^{word}$", x) and x[1:].lower() == x[1:]}
        alt = {x.capitalize() for x in alt if x.strip("\"").lower().replace("\"", "") != found_label.lower().replace("\"", "")}
        found.append(found_label + ": " + " | ".join(alt))
        entry["label"] = found_label
        entry["altLabels"] = list(alt)
    else:
        entry["label"] = "[Namn mangler]"

        

        

print("Problematic labels:", antall)
with open(".tmp/hord_label_found.txt", "w", encoding="utf-8") as f:
    f.write("\n".join(found))

with open(".tmp/hord_problematic_label.txt", "w", encoding="utf-8") as f:
    f.write("\n".join(nolabel))

with open(".tmp/not_enriched.txt", "w", encoding="utf-8") as f:
    f.write("\n".join([str(x) for x in not_enriched]))




output_json = save_elastic_json("hord", json_data)

es_upload(output_json, "hord")