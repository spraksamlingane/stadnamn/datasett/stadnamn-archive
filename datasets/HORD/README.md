# Hordanamn
Python environment: environments/requirements_1.txt 

## Data

### xml_export
Folder containing an export from hordanamn, received from Tone Merete Bruvik 13.12.2022


## Conversion

### Preparation
Properties extracted and the data validated. Hierarchy of data properties and object properties imported into protege, in an ontology based on bsn_ontology.ttl, save as bsn_ontology.ttl.

### xml2rdf.py
Based on the script in bustadnamn. Run it with the filename as argument in order to do a complete conversion, otherwise only a limited number of rows will be converted.





