import os
import xml.etree.ElementTree as ET
from rdflib import Namespace, RDF, RDFS, Graph, Literal, XSD, BNode, DCTERMS, URIRef, DCAT, SKOS, DCMITYPE, PROV
import uuid
import xml.etree.ElementTree as ET
import sys
import re
import utm
import requests
import json

TEST = len(sys.argv) == 1

RAW = "lfs-data/raw/"
xmlfiles = [RAW + "/HORD/xml_export/"+fil for fil in os.listdir(RAW+"HORD/xml_export/") if fil[-3:] == "xml"]


SPRAK = Namespace("https://data.spraksamlingane.no/id/")
IIIF_MANIFEST = Namespace("https://iiif.spraksamlingane.no/iiif/manifest/")
IIIF_PREZI = Namespace("http://iiif.io/api/presentation/3#")

# Uuid functions
def create_collection_uuid(identifier):
    seed = "http://data.toponym.ub.uib.no/hordanamn/collections/" + identifier
    uid = str(uuid.uuid3(uuid.NAMESPACE_URL, seed))
    return uid, seed

local_uri = lambda *args, fragment = "": SPRAK[str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/HORD/" +  "_".join(list(args))))+ fragment]

# Graphs

ontology = Graph()
ontology.parse("datasets/HORD/hord_ontology.ttl")
graph = Graph()

NAMESPACES = {x[0]: x[1] for x in ontology.namespaces()}
HORD = Namespace(NAMESPACES[""])

WGS = Namespace("http://www.w3.org/2003/01/geo/wgs84_pos#")
CC = Namespace("https://creativecommons.org/")
WDW = Namespace("http://dewey.info/class/")

graph.bind("dct", DCTERMS)
graph.bind("dcat", DCAT)
graph.bind("skos", SKOS)
graph.bind("spr", SPRAK)
graph.bind("geo", WGS)
graph.bind("prov", PROV)
graph.bind("iiifm", IIIF_MANIFEST)
graph.bind("iifp", IIIF_PREZI)
graph.bind("", HORD)


CLASS = {str(result[0][len(HORD):]): result[0] for result in ontology.query("SELECT ?class WHERE { ?class a owl:Class }")}
OBJPROP = {str(result[0][len(HORD):]): result[0] for result in ontology.query("SELECT ?prop WHERE { ?prop rdfs:subPropertyOf :rawObjectProperty }")}
DATAPROP = {str(result[0][len(HORD):]): result[0] for result in ontology.query("SELECT ?prop WHERE { ?prop rdfs:subPropertyOf+ :rawDataProperty }")} 
ADDED_PROPS = {str(result[0][len(HORD):]): result[0] for result in ontology.query("SELECT ?prop WHERE { ?prop rdfs:subPropertyOf ?propertyClass . FILTER NOT EXISTS { ?prop rdfs:subPropertyOf :rawDataProperty } FILTER NOT EXISTS { ?prop rdfs:subPropertyOf :rawObjectProperty }}")}

manifest_basis = {}

def assert_one_to_one(row, node, tag):
    assert len(node.findall(tag)) == 1, print(len(row.findall(tag)), row.attrib["idnr"])

has_data = {}
def has_content(node):
    for x in node:
        if x.text and x.text.strip() != "":
            has_data[node.tag] = True
            has_data[x.tag] = True
            return True
        if has_content(x):
            has_data[node.tag] = True
            return True
    return False


def parse(uri, node):
    for child in node:
        if child.tag in DATAPROP and child.text and child.text.strip() != "":
            graph.add((uri, DATAPROP[child.tag], Literal(child.text)))
        elif has_content(child):
            child_uri = BNode()
            graph.add((uri, OBJPROP[child.tag], child_uri))
            parse(child_uri, child)


with open("lfs-data/processed/HORD/20230126_audio_metadata.json") as inn:
    audio_metadata = json.load(inn)


with open("lfs-data/processed/HORD/20230403_hord_audio_duration.json") as inn:
    audio_duration = json.load(inn)



# IIIF collections
HORDANAMN_COLLECTION, HORDANAMN_COLLECTION_SEED = create_collection_uuid("TOP_COLLECTION")
LLE_COLLECTION, LLE_COLLECTION_SEED = create_collection_uuid("LLE_COLLECTION")

prepared_iiif = {"manifests": {}, "collections": {
    HORDANAMN_COLLECTION: {
        "seed": HORDANAMN_COLLECTION_SEED,
        "label": {None: "Hordanamn"}
    },
    LLE_COLLECTION: {
        "seed": LLE_COLLECTION_SEED,
        "label": {None: "LLE arkiv"},
        "parent": HORDANAMN_COLLECTION
    },
}}


# Read xml

for f in xmlfiles:
    original_file = re.search(r"xml_export/(.+)", f).group(1)
    print(f)
    tree = ET.parse(f)
    root = tree.getroot()
    kommuner = {}
    teller = 0
    for row in root:
        if not TEST or teller < 100:
            teller += 1
            namn_nr = row.find("namnNr").text
            knr = row.find("kommuneNr").text
            objekt_nr = row.find("objektNr").text
            uri = local_uri("objektNr", "kommuneNr", objekt_nr, knr)

            raw_uri = BNode() #local_uri("objektNr", "kommuneNr", objekt_nr, knr, fragment = "#data")
            graph.add((raw_uri, RDF.type, CLASS["Row"]))
            parse(raw_uri, row)
            if row.attrib["id"] != "":
                graph.add((raw_uri, DATAPROP["id"], Literal(row.attrib["id"])))
            graph.add((uri, DCTERMS.hasPart, raw_uri))

            # Add coordinates
            lng = row.find("xKoordinat").text
            lat = row.find("yKoordinat").text
            if lat and lat.strip() != "" and lng and lng.strip() != "":

                try:
                    # Fix typo in coordinate out of range
                    if (namn_nr, objekt_nr) == ("19228", "H4244"):
                        lat = "2" + lat
                    
                    vlat, vlng = utm.to_latlon(float(lat), float(lng), 32, 'V')
                    location = BNode()
                    graph.add((uri, DCTERMS.hasPart, location))
                    graph.add((location, RDF.type, DCTERMS.Location))
                    graph.add((location, WGS.lat, Literal(str(vlat))))
                    graph.add((location, WGS.long, Literal(str(vlng))))
                except Exception as e:
                    print(lng, lat)
                    print(namn_nr, objekt_nr)
                    print(e)
            
            namn = row.find("namn").text or row.find("normertForm").text
            if objekt_nr in audio_metadata:
                komm = row.find("kommuneNamnLC").text
                audio_file_uuid = str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/HORD/audio/" +   "_".join((objekt_nr, komm, knr, namn))))

                
                audio_data = audio_metadata[objekt_nr].get(audio_file_uuid)
                if audio_data:
                    audio_uri = SPRAK[audio_file_uuid]
                    graph.add((audio_uri, DCTERMS.type, DCMITYPE.Sound))
                    graph.add((uri, DCTERMS.hasPart, audio_uri))
                    graph.add((audio_uri, RDF.type, CLASS["VoiceRecording"]))
                    if audio_duration[audio_file_uuid] == 0:
                        graph.add((audio_uri, DCTERMS.description, Literal("Empty audio file", lang="en")))
                        graph.add((audio_uri, DCTERMS.description, Literal("Tom lydfil", lang="no")))
                    else:
                        audio_manifest_seed = audio_file_uuid + "#manifest"
                        audio_manifest_uuid = str(uuid.uuid3(uuid.NAMESPACE_URL, audio_manifest_seed))
                        graph.add((audio_uri, DCTERMS.identifier, Literal(audio_file_uuid)))
                        manifest_uri = IIIF_MANIFEST[audio_manifest_uuid]
                        graph.add((audio_uri, DCTERMS.isReferencedBy, manifest_uri))
                        graph.add((manifest_uri, DCTERMS.identifier, Literal(audio_manifest_uuid)))
                        graph.add((manifest_uri, RDF.type, IIIF_PREZI.Manifest))

                        collection_data = {}
                        
                        if "url" in audio_data: # Files present in the previous web application
                            audio_path = audio_data["url"][33:]
                            audio_filename = audio_data["url"][audio_data["url"].rindex("/")+1:]
                            if "/" in audio_path:
                                audio_path = audio_path[:audio_path.rindex("/")]
                            collection_uuid, collection_seed = create_collection_uuid(audio_path)

                            if "/" in audio_path:
                                parent_name = audio_path[:audio_path.rindex("/")]
                                parent_uuid, parent_seed = create_collection_uuid(parent_name)

                                prepared_iiif["collections"][parent_uuid] = {
                                "seed": parent_seed,
                                "label": parent_name,
                                "parent": HORDANAMN_COLLECTION
                                }
                                parent = parent_uuid
                                

                                

                            else: # Files missing in the previous web application
                                parent = HORDANAMN_COLLECTION

                            prepared_iiif["collections"][collection_uuid] = {
                                "seed": collection_seed,
                                "label": audio_path,
                                "parent": parent
                            }
                        else:
                            audio_filename = audio_data["hdd_files"][0][audio_data["hdd_files"][0].rindex("/")+1:]
                            audio_path = audio_data["hdd_files"][0][15:]
                            audio_path = audio_path[:audio_path.rindex("/")]
                            
                            collection_uuid, collection_seed = create_collection_uuid(audio_path)
                            prepared_iiif["collections"][collection_uuid] = {
                                "seed": collection_seed,
                                "label": audio_path,
                                "parent": LLE_COLLECTION
                            }
                        
                        # Add manifest types and labels to graph
                        graph.add((IIIF_MANIFEST[collection_uuid], RDF.type, IIIF_PREZI.Collection))
                        graph.add((IIIF_MANIFEST[collection_uuid], RDFS.label, Literal(audio_path)))
                        graph.add((IIIF_MANIFEST[collection_uuid], DCTERMS.identifier, Literal(collection_uuid)))

                        graph.add((manifest_uri, DCTERMS.isPartOf, IIIF_MANIFEST[collection_uuid]))
                        graph.add((manifest_uri, RDFS.label, Literal(audio_filename)))
                        


                        prepared_iiif["manifests"][audio_manifest_uuid] = {
                            "seed": audio_manifest_seed,
                            "collection": collection_uuid,
                            "audio": audio_file_uuid,
                            "label": audio_data["name"],
                            "filename": audio_filename,
                            "original_path": audio_path,
                            "municipality": audio_data["municipality"].capitalize(),
                            "municipality_number": audio_data["municipality_number"],
                            "county": row.find("fylkesNamn").text or "Hordaland",
                            "duration": audio_duration[audio_file_uuid]
                        }

                        for item in ([audio_data.get("url")] if audio_data.get("url") else []) + audio_data.get("hdd_files", []):
                            graph.add((audio_uri, ADDED_PROPS["formerFileLocation"], Literal(item)))
                            prepared_iiif["manifests"][audio_manifest_uuid]["file_locations"] = prepared_iiif["manifests"][audio_manifest_uuid].get("file_locations", []) + [item]
                    

                    


                    
with open("datasets/HORD/manifest_basis_sample.json" if TEST else "lfs-data/processed/HORD/hord_manifest_basis.json", "w") as out:
    json.dump(prepared_iiif, out, indent = "\t")

del prepared_iiif

print("serializing")
graph.serialize("datasets/HORD/hord_sample.ttl" if TEST else "lfs-data/rdf/HORD/" + sys.argv[1]) #TODO: handle output format


    


