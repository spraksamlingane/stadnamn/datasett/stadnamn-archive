import os
import xml.etree.ElementTree as ET


RAW = "lfs-data/raw/"
xmlfiles = [RAW + "/HORD/xml_export/"+fil for fil in os.listdir(RAW+"HORD/xml_export/") if fil[-3:] == "xml"]


nodes = {}
attributes = {}
parents = {}
lc_not_matching = {} # Specific to hordanamn: only keep lowercease fields that dont match their original fiels

# Validate data and extract properties
def explore(node, path, fil):
    for attrib in node.attrib:
        if node.attrib[attrib] is not None and node.attrib[attrib] != "":
            attpath = path + "/" + attrib
            assert attributes.get(attrib, attpath) == attpath
            attributes[attrib] = path + "/" + attrib
    if node.text:
        nodepath = path + "/" +  node.tag
        assert nodes.get(node.tag, nodepath) == nodepath
        nodes[node.tag] = nodepath


    for child in node:
        if child.text and child.tag[-2:] == "LC":
                original = node.find(child.tag[:-2])
                if not original.text or original.text.lower() != child.text:
                    lc_not_matching[(child.tag, fil)] = (child.text, original.text)
        if child.text:
            parentpath = path+"/"+node.tag
            assert parents.get(parentpath, parentpath) == parentpath
            parents[node.tag] = parentpath
        explore(child, path + node.tag, fil)


for fil in xmlfiles:
    print(fil)
    tree = ET.parse(fil)
    root = tree.getroot()
    for row in root:
        explore(row, "", fil)


    
# Check for collisions with enrichment properties and classes
for key in attributes.keys() | nodes.keys():
    assert key[0] == key[0].lower()
    assert key not in {"attributeProperty", "rawDataProperty", "rawObjectProperty", "subdivisionOf", "sourceMunicipality", "Row"}


# Write properties to file
with open("datasets/HORD/script-output/properties.txt", "w") as outfile:
    outfile.write("# Data properties\n")
    outfile.write("attributeProperty\n")
    for key in attributes:
        outfile.write("\t"+key+"\n")
    outfile.write("rawDataProperty\n")

    for key in attributes: # Subclasses of both attributeProperty and rawDataProperty
        outfile.write("\t"+key+"\n")

    for key in nodes:
        if key not in parents and key[-2:] != "LC" or key == "kommuneNamnLC":
            outfile.write("\t"+key+"\n")

    outfile.write("# Object properties\n")
    outfile.write("rawObjectProperty\n")
    for key in parents:
        outfile.write("\t"+key+"\n")