import os
import xml.etree.ElementTree as ET
import rdflib
import pandas as pd

files = ["large-file-storage/raw-data/HORD/XML-filer lasted ned fra eXist/" + fn for fn in os.listdir("large-file-storage/raw-data/HORD/XML-filer lasted ned fra eXist/")]
tags = set()
notnull = set()
rows = []

for f in files:   
    with open(f, encoding="utf-8") as xmlfile:
        print(f)
        
        tree = ET.parse(xmlfile)
        root = tree.getroot()

        for child in root:
            
            for att in child: 
                tags.add(att.tag)
                if att.text and att.text.strip() != "":
                    notnull.add(att.tag)
            #rows.append({att.tag: att.text.strip() for att in child if att.text})

print("NOTNULL", notnull)
print()
print("NULL", [x for x in tags if x not in notnull])

