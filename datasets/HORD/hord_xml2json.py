import os
import xml.etree.ElementTree as ET
import uuid
import xml.etree.ElementTree as ET
import sys
import re
import utm
import json
from stadnamn.elasticsearch import es_upload, save_elastic_json
from collections import defaultdict

TEST = len(sys.argv) == 1


RAW = "lfs-data/raw/"
xmlfiles = [RAW + "/HORD/xml_export/"+fil for fil in os.listdir(RAW+"HORD/xml_export/") if fil[-3:] == "xml"]


# Uuid functions
def create_collection_uuid(identifier):
    seed = "http://data.toponym.ub.uib.no/hordanamn/collections/" + identifier
    uid = str(uuid.uuid3(uuid.NAMESPACE_URL, seed))
    return uid, seed

hordanamn_uuid = lambda *args, fragment = "": str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/HORD/" +  "_".join(list(args))))+ fragment

manifest_basis = {}
json_data = []


has_data = {}
def has_content(node):
    for x in node:
        if x.text and x.text.strip() != "":
            has_data[node.tag] = True
            has_data[x.tag] = True
            return True
        if has_content(x):
            has_data[node.tag] = True
            return True
    return False



with open("lfs-data/processed/HORD/20230126_audio_metadata.json") as inn:
    audio_metadata = json.load(inn)


with open("lfs-data/processed/HORD/20230403_hord_audio_duration.json") as inn:
    audio_duration = json.load(inn)



# IIIF collections
HORDANAMN_COLLECTION, HORDANAMN_COLLECTION_SEED = create_collection_uuid("TOP_COLLECTION")
LLE_COLLECTION, LLE_COLLECTION_SEED = create_collection_uuid("LLE_COLLECTION")

prepared_iiif = {"manifests": {}, "collections": {
    HORDANAMN_COLLECTION: {
        "seed": HORDANAMN_COLLECTION_SEED,
        "label": {None: "Hordanamn"}
    },
    LLE_COLLECTION: {
        "seed": LLE_COLLECTION_SEED,
        "label": {None: "LLE arkiv"},
        "parent": HORDANAMN_COLLECTION
    },
}}




def element_to_dict(t):
    d = {t.tag: {} if t.attrib else None}
    children = list(t)
    if children:
        dd = defaultdict(list)
        for dc in map(element_to_dict, children):
            for k, v in dc.items():
                if v:  # Skip keys with None or empty string values
                    dd[k].append(v)
        d = {t.tag: {k:v[0] if len(v)==1 else v for k, v in dd.items()}}
                    
    if t.attrib:
        d[t.tag].update(('@' + k, v) for k, v in t.attrib.items() if v)  # Skip attributes with None or empty string values
    if t.text:
        text = t.text.strip()
        if children or t.attrib:
            if text:
              d[t.tag]['#text'] = text
        else:
            if text:  # Only assign text if it's not None or empty
                d[t.tag] = text
    return {k: v for k, v in d.items() if v}  # Skip keys with None or empty string values at the top level

    

# Read xml

def preprocess_xml(xml_string):
    """ Preprocessing that must be done before parsing the XML string"""
    xml_string = xml_string.replace('<br/>', '\n')
    return xml_string



for f in xmlfiles:

    nocord = 0
    original_file = re.search(r"xml_export/(.+)", f).group(1)
    print(f)
     # Read the file as a string
    with open(f, 'r', encoding="utf-8") as file:
        xml_string = file.read()

    # Preprocess the XML string
    xml_string = preprocess_xml(xml_string)

    # Parse the XML string
    root = ET.fromstring(xml_string)
    kommuner = {}
    teller = 0
    for row in root:
        NAMN_NR = namn_nr = row.find("namnNr").text # all caps variables for uuid, must not be changed
        KNR = knr = row.find("kommuneNr").text
        OBJEKT_NR = objekt_nr = row.find("objektNr").text
        row_uuid = hordanamn_uuid("objektNr", "kommuneNr", OBJEKT_NR, KNR)
        normertForm = row.find("normertForm").text
        LABEL =  row.find("namn").text or row.find("normertForm").text

        label = row.find("normertForm").text or row.find("namn").text
        label = label.strip("'\"")

        row_json = {
            "uuid": row_uuid,
            "label": label,
            "archive": {
                "institution": "Statens kartverk" if row.find("arkivTilvising").text == "SSR" else "LLE, UiB"
                }
        }

        if row.find("bruka"):
            bruka = row.find("bruka").findall("bruk")
            if bruka and bruka[0].find("gardsNr") is not None:
                row_json["cadastre"] = []
                for bruk in bruka:
                    gardsnr_elements = bruk.findall("gardsNr")
                    bruksnr_elements = bruk.findall("bruksNr")

                    
                    if bruksnr_elements and len(gardsnr_elements) != 1:
                        continue

                    for gardsnr in gardsnr_elements:
                        if not gardsnr.text:
                            continue
                        # Find all numbers in the string
                        gnr_values = re.findall(r'\d+', gardsnr.text)
                        if bruksnr_elements and len(gnr_values) != 1:
                            continue
                        for bruksnr in bruksnr_elements:
                            if not bruksnr.text:
                                continue
                            bnr_values = re.findall(r'\d+', bruksnr.text)
                            row_json["cadastre"].append({"gnr": int(gnr_values[0]), "bnr": int(bnr_values[0]) if len(bnr_values) == 1 else [int(bnr) for bnr in bnr_values]})
                        
                        if not bruksnr_elements:
                            row_json["cadastre"].append({"gnr": int(gnr_values[0]) if len(gnr_values) == 1 else [int(gnr) for gnr in gnr_values]})
                                

        lng = row.find("xKoordinat").text
        lat = row.find("yKoordinat").text

        if lat and lat.strip() != "" and lng and lng.strip() != "":
            try:
                """ Data cleaning """
                # Fix typo in coordinate out of range
                if (namn_nr, objekt_nr) == ("19228", "H4244"):
                    lat = "2" + lat
                # Fix coordinate that contains newlines
                if "\n" in lat:
                    lat = lat.split("\n")[0]

                vlat, vlng = utm.to_latlon(float(lat), float(lng), 32, 'V')
            except Exception as e:
                print("INVALID COORDINATES", lng, lat, namn_nr, objekt_nr, e)
            row_json["location"] = {
                "type": "Point",
                "coordinates": [vlng, vlat]
            }
        row_json["rawData"] = element_to_dict(row)["row"]
        row_json["rawData"] = {k: v for k, v in row_json["rawData"].items() if k[-2:] != "LC"} # Exlude lowecase versions of the fields
        json_data.append(row_json)


        """
        Handle administrative divisions
        """
        row_json["adm1"] = row.find("fylkesNamn").text
        if not row_json["adm1"] or row_json["adm1"] == "Samnanger": # Samnanger is not a county
            row_json["adm1"] = "Hordaland"

        adm_kommune = row.find("kommuneNamn").text
        if " - " in adm_kommune:
            adm_kommune, row_json["adm3"] = adm_kommune.split(" - ")
        row_json["adm2"] = adm_kommune

        


        if objekt_nr in audio_metadata:
            komm = KOMM = row.find("kommuneNamnLC").text
            audio_file_uuid = str(uuid.uuid3(uuid.NAMESPACE_URL, "http://data.toponym.ub.uib.no/graph/HORD/audio/" +   "_".join((OBJEKT_NR, KOMM, KNR, LABEL))))
            audio_data = audio_metadata[objekt_nr].get(audio_file_uuid)
            if audio_data:
                if audio_duration[audio_file_uuid] != 0:
                    AUDIO_MANIFEST_SEED = audio_file_uuid + "#manifest"
                    audio_manifest_uuid = str(uuid.uuid3(uuid.NAMESPACE_URL, AUDIO_MANIFEST_SEED))
                    row_json["audio"] = {"manifest": audio_manifest_uuid, "file": audio_file_uuid + ".wav"}
                    collection_data = {}
                    
                    if "url" in audio_data: # Files present in the previous web application
                        audio_path = audio_data["url"][33:]
                        audio_filename = audio_data["url"][audio_data["url"].rindex("/")+1:]
                        if "/" in audio_path:
                            audio_path = audio_path[:audio_path.rindex("/")]
                        collection_uuid, collection_seed = create_collection_uuid(audio_path)

                        if "/" in audio_path:
                            parent_name = audio_path[:audio_path.rindex("/")]
                            parent_uuid, parent_seed = create_collection_uuid(parent_name)

                            prepared_iiif["collections"][parent_uuid] = {
                            "seed": parent_seed,
                            "label": parent_name,
                            "parent": HORDANAMN_COLLECTION
                            }
                            parent = parent_uuid
                            
                        else: # Files missing in the previous web application
                            parent = HORDANAMN_COLLECTION

                        prepared_iiif["collections"][collection_uuid] = {
                            "seed": collection_seed,
                            "label": audio_path,
                            "parent": parent
                        }
                    else:
                        audio_filename = audio_data["hdd_files"][0][audio_data["hdd_files"][0].rindex("/")+1:]
                        audio_path = audio_data["hdd_files"][0][15:]
                        audio_path = audio_path[:audio_path.rindex("/")]
                        
                        collection_uuid, collection_seed = create_collection_uuid(audio_path)
                        prepared_iiif["collections"][collection_uuid] = {
                            "seed": collection_seed,
                            "label": audio_path,
                            "parent": LLE_COLLECTION
                        }                       


                    prepared_iiif["manifests"][audio_manifest_uuid] = {
                        "seed": AUDIO_MANIFEST_SEED,
                        "collection": collection_uuid,
                        "audio": audio_file_uuid,
                        "label": audio_data["name"],
                        "filename": audio_filename,
                        "original_path": audio_path,
                        "municipality": audio_data["municipality"].capitalize(),
                        "municipality_number": audio_data["municipality_number"],
                        "county": row.find("fylkesNamn").text or "Hordaland",
                        "duration": audio_duration[audio_file_uuid]
                    }

                    for item in ([audio_data.get("url")] if audio_data.get("url") else []) + audio_data.get("hdd_files", []):
                        prepared_iiif["manifests"][audio_manifest_uuid]["file_locations"] = prepared_iiif["manifests"][audio_manifest_uuid].get("file_locations", []) + [item]
    

with open("datasets/HORD/manifest_basis_sample.json" if TEST else "lfs-data/processed/HORD/hord_manifest_basis.json", "w") as out:
    json.dump(prepared_iiif, out, indent = "\t")

del prepared_iiif

print("Save json")
with open(".tmp/hord_data.json", "w") as out:
    json.dump(json_data, out, indent = "\t")
